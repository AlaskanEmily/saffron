#ifndef SAFFRON_GL_FUNC_VAR
#define SAFFRON_GL_FUNC_VAR(X) SaffronGL_ ## X ## _PROC X;
#endif
#ifndef SAFFRON_GL_FUNC_LOAD
#define SAFFRON_GL_FUNC_LOAD(X, EXT) \
    SaffronGL_LoadFunction(gl_ctx, (char*)("gl" #X EXT), &maybe_func); \
    if(!Saffron_GetCtxFunctionOK(maybe_func, &func)) break; \
    (ctx->X) = (SaffronGL_ ## X ## _PROC)func; 

#define SAFFRON_GL_LOAD_FUNC_CORE(X) SAFFRON_GL_FUNC_LOAD(X, "")
#endif

#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_GenVertexArrays_PROC)(
    GLsizei,
    GLuint*);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_DeleteVertexArrays_PROC)(
    GLsizei,
    const GLuint*);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_BindVertexArray_PROC)(
    GLuint);
#endif
#ifndef SAFFRON_GL_IMPL
#define SAFFRON_GL_VAO_FUNCS(X) \
    X(GenVertexArrays) \
    X(DeleteVertexArrays) \
    X(BindVertexArray) \
/* */
struct SaffronGL_VAOCtx{
    SAFFRON_GL_VAO_FUNCS(SAFFRON_GL_FUNC_VAR)
};
#endif
#ifndef SAFFRON_GL_IMPL
#ifdef SAFFRON_GL_EXPECT_VAO
#define SAFFRON_GL_VAO_FUNC(CTX, X) (gl ## X)
#define SAFFRON_GL_VAO_CTX MR_Word
#define SAFFRON_GL_VAO_CTX_FIELD(NAME)
#define SAFFRON_GL_ALLOC_VAO_CTX() 0
#define SAFFRON_GL_LOAD_VAO_CTX(GLCTX, CTX) 0
#else
struct SaffronGL_VAOCtx;
int SaffronGL_LoadVAOCtx(MR_Word gl_ctx, struct SaffronGL_VAOCtx *ctx);
#define SAFFRON_GL_VAO_FUNC(CTX, X) ((CTX)->X)
#define SAFFRON_GL_VAO_CTX struct SaffronGL_VAOCtx*
#define SAFFRON_GL_VAO_CTX_FIELD(NAME) struct SaffronGL_VAOCtx NAME;
#define SAFFRON_GL_ALLOC_VAO_CTX() \
    MR_GC_malloc_atomic(sizeof(struct SaffronGL_VAOCtx))
#define SAFFRON_GL_LOAD_VAO_CTX(GLCTX, CTX) SaffronGL_LoadVAOCtx((GLCTX), (CTX))
#endif
#else
#ifndef SAFFRON_GL_EXPECT_VAO
int SaffronGL_LoadVAOCtx(MR_Word gl_ctx, struct SaffronGL_VAOCtx *ctx){
    MR_Word maybe_func, func;
    MR_Bool have_ext;
    MR_String err = NULL;
    MR_Integer major, minor;
    SaffronGL_Version(gl_ctx, &major, &minor);
    if(major > 3 || (major == 3 && minor >= 0)) do{
        SAFFRON_GL_VAO_FUNCS(SAFFRON_GL_LOAD_FUNC_CORE)
        return 0;
    }while(0);
    SaffronGL_SupportsExtension(gl_ctx, (MR_String)"GL_ARB_vertex_array_object", &have_ext);
    if(have_ext){
        do{
            SAFFRON_GL_VAO_FUNCS(SAFFRON_GL_LOAD_FUNC_CORE)
            return 0;
        }while(0);
    }
#if (defined __APPLE__)
#define SAFFRON_GL_LOAD_FUNC_VAO_APPLE(X) SAFFRON_GL_FUNC_LOAD(X, "APPLE")
    SaffronGL_SupportsExtension(gl_ctx, (MR_String)"GL_APPLE_vertex_array_object", &have_ext);
    if(have_ext){
        do{
            SAFFRON_GL_VAO_FUNCS(SAFFRON_GL_LOAD_FUNC_VAO_APPLE)
            return 0;
        }while(0);
    }
#endif /* (defined __APPLE__) */
    return 1;
}
#endif
#endif
#ifndef GL_VERTEX_ARRAY_BINDING
#define GL_VERTEX_ARRAY_BINDING 0x85B5
#endif
