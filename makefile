# Unix makefile to build Saffron.
# Needs GNU Make or BSD Make.

MMC?=mmc
SO_FOUND!=case uname in Darwin) echo dylib ;; *_NT|*CYG*|*[Mm][Ss][Yy][Ss]*) echo dll ;; *) echo so ;; esac
SO?=$(SO_FOUND)
ROOT!=pwd
MMC_EXTRA_FLAGS1!=uname | egrep -v 'BSD$$' >/dev/null || echo ' --c-include-dir /usr/X11R6/include -L /usr/X11R6/lib'
MMC_EXTRA_FLAGS2!=uname | fgrep Linux >/dev/null || echo ' --c-include-dir /usr/local/include -L /usr/local/lib'
MMC_OPTS?=--use-grade-subdirs --intermodule-optimization -O 7 --output-compile-error-lines 1024 $(MMC_EXTRA_FLAGS1) $(MMC_EXTRA_FLAGS2) --cflag -g --ld-flag -g
GRADE_OPT!=test -z '$(GRADE)' || echo ' --grade=$(GRADE)'
PARALLEL_OPT!=test -z '$(PARALLEL)' || echo ' -j $(PARALLEL)'
SAFFRON_MMC_OPTS=$(MMC_OPTS) $(GRADE_OPT) $(PARALLEL_OPT)
SHARED_MMC_FLAGS=--linkage shared --lib-linkage shared
STATIC_MMC_FLAGS=--linkage static --lib-linkage static
AWK?=awk
CC!=$(MMC) $(SAFFRON_MMC_OPTS) --output-cc
CFLAGS!=$(MMC) $(SAFFRON_MMC_OPTS) --output-cflags

ALL=build_saffron
all: $(ALL)

UTILS=build_saffron_window build_saffron_delegate build_saffron_glow build_saffron_tga
utils: $(UTILS)

DEMOS=build_saffron_cube_demo build_saffron_shader_demo build_saffron_glfw_demo
demos: $(DEMOS)

MMATH_LINK_FLAGS=--search-lib-files-dir '$(ROOT)/mmath' --init-file '$(ROOT)/mmath/mmath.init'
# Used for utils, tests, and demos. That's why it requires the ROOT variable to be set in shell.
SAFFRON_LINK_FLAGS=--search-lib-files-dir '$(ROOT)' --init-file '$(ROOT)/saffron.init'
GLOW_INCLUDE_FLAGS=--c-include-dir '$(ROOT)/glow'
# Transunit, used for testing.
TRANSUNIT_LINK_FLAGS=--search-lib-files-dir '$(ROOT)/test/transunit' --init-file '$(ROOT)/test/transunit/transunit.init' --link-object '$(ROOT)/test/transunit/libtransunit.a'
# TGA, used for some demos and has its own tests.
SAFFRON_TGA_LINK_FLAGS=--search-lib-files-dir '$(ROOT)/util/tga' --init-file '$(ROOT)/util/tga/saffron_tga.init'
SAFFRON_GLOW_LINK_FLAGS=--search-lib-files-dir '$(ROOT)/util/glow' --init-file '$(ROOT)/util/glow/saffron_glow.init' $(GLOW_INCLUDE_FLAGS)
SAFFRON_WINDOW_LINK_FLAGS=--search-lib-files-dir '$(ROOT)/util/window' --init-file '$(ROOT)/util/window/saffron_window.init'
SAFFRON_DELEGATE_LINK_FLAGS=$(SAFFRON_WINDOW_LINK_FLAGS) --search-lib-files-dir '$(ROOT)/util/delegate' --init-file '$(ROOT)/util/delegate/saffron_delegate.init'
SAFFRON_GLFW_LINK_FLAGS=$(SAFFRON_DELEGATE_LINK_FLAGS) --search-lib-files-dir '$(ROOT)/util/glfw' --init-file '$(ROOT)/util/glfw/saffron_glfw.init'

# Generated source.
saffron.gl.buffer.inc: saffron.gl_gen.awk saffron.gl.buffer.csv
	$(AWK) -F, -f saffron.gl_gen.awk saffron.gl.buffer.csv > '$@'
	@touch saffron.gl.buffer.m # Force the Mercury compiler to rebuild this...

saffron.gl.shader.inc: saffron.gl_gen.awk saffron.gl.shader.csv
	$(AWK) -F, -f saffron.gl_gen.awk saffron.gl.shader.csv > '$@'
	@touch saffron.gl.shader.m # Force the Mercury compiler to rebuild this...

saffron.gl.vao.inc: saffron.gl_gen.awk saffron.gl.vao.csv
	$(AWK) -F, -f saffron.gl_gen.awk saffron.gl.vao.csv > '$@'
	@touch saffron.gl.vao.m # Force the Mercury compiler to rebuild this...

# Primary libraries.
# Each is preceded by the shared object they create (if any, some like the TGA reader is only built static).
# This is used to copy the shared objects to target directories.
libsaffron.$(SO): build_saffron
build_saffron: build_mmath saffron.gl.buffer.inc saffron.gl.shader.inc saffron.gl.vao.inc
	$(MMC) $(SAFFRON_MMC_OPTS) $(MMATH_LINK_FLAGS) --make libsaffron $(STATIC_MMC_FLAGS)
	$(MMC) $(SAFFRON_MMC_OPTS) $(MMATH_LINK_FLAGS) --make libsaffron $(SHARED_MMC_FLAGS) -L '$(ROOT)/mmath' -lmmath -lGL -lX11
	install libsaffron.$(SO) demo/
	install libsaffron.$(SO) test/

mmath/libmmath.$(SO): build_mmath
build_mmath:
	cd mmath && $(MMC) $(SAFFRON_MMC_OPTS) --make libmmath $(STATIC_MMC_FLAGS)
	cd mmath && $(MMC) $(SAFFRON_MMC_OPTS) --make libmmath $(SHARED_MMC_FLAGS)
	install mmath/libmmath.$(SO) demo/
	install mmath/libmmath.$(SO) test/

CLEAN_LIB=for X in "$(ROOT)/$$DIR"/*.err "$(ROOT)/$$DIR"/*.mh "$(ROOT)/$$DIR"/*.init "$(ROOT)/$$DIR"/*.a "$(ROOT)/$$DIR"/*.so "$(ROOT)/$$DIR"/*.dylib "$(ROOT)/$$DIR"/*.lib "$(ROOT)/$$DIR"/*.dll ; do if test -f "$$X" || test -h "$$X" ; then rm "$$X" ; fi ; done
CLEAN_MERCURY=cd "$(ROOT)/$$DIR" && $(MMC) $(SAFFRON_MMC_OPTS) $(SAFFRON_LINK_FLAGS) --make "$$TARGET".clean ; $(CLEAN_LIB)

#Utils
glow/libglow.$(SO): build_glow
build_glow:
	$(MAKE) -C glow libglow.a libglow.$(SO) CC='$(CC)' CFLAGS='$(CFLAGS)'
	install glow/libglow.$(SO) demo/
	install glow/libglow.$(SO) test/

util/glow/libsaffron_glow.$(SO): build_saffron_glow
build_saffron_glow: build_glow build_saffron_window build_saffron
	cd util/glow && $(MMC) $(SAFFRON_MMC_OPTS) $(MMATH_LINK_FLAGS) $(SAFFRON_LINK_FLAGS) $(SAFFRON_WINDOW_LINK_FLAGS) $(GLOW_INCLUDE_FLAGS) --make libsaffron_glow $(STATIC_MMC_FLAGS) 
	cd util/glow && $(MMC) $(SAFFRON_MMC_OPTS) $(MMATH_LINK_FLAGS) $(SAFFRON_LINK_FLAGS) $(SAFFRON_WINDOW_LINK_FLAGS) $(GLOW_INCLUDE_FLAGS) --make libsaffron_glow $(SHARED_MMC_FLAGS) -L '$(ROOT)/util/window' -lsaffron_window -L '$(ROOT)' -lsaffron -L '$(ROOT)/glow' -lglow -l GL -l X11
	install util/glow/libsaffron_glow.$(SO) demo/
	install util/glow/libsaffron_glow.$(SO) test/

util/window/libsaffron_window.$(SO): build_saffron_window
build_saffron_window:
	cd util/window && $(MMC) $(SAFFRON_MMC_OPTS) $(MMATH_LINK_FLAGS) $(SAFFRON_LINK_FLAGS) --make libsaffron_window $(STATIC_MMC_FLAGS)
	cd util/window && $(MMC) $(SAFFRON_MMC_OPTS) $(MMATH_LINK_FLAGS) $(SAFFRON_LINK_FLAGS) --make libsaffron_window $(SHARED_MMC_FLAGS)
	install util/window/libsaffron_window.$(SO) demo/
	install util/window/libsaffron_window.$(SO) test/

util/window/libsaffron_delegate.$(SO): build_saffron_delegate
build_saffron_delegate: build_saffron_window
	cd util/delegate && $(MMC) $(SAFFRON_MMC_OPTS) $(MMATH_LINK_FLAGS) $(SAFFRON_LINK_FLAGS) $(SAFFRON_WINDOW_LINK_FLAGS) --make libsaffron_delegate $(STATIC_MMC_FLAGS)
	cd util/delegate && $(MMC) $(SAFFRON_MMC_OPTS) $(MMATH_LINK_FLAGS) $(SAFFRON_LINK_FLAGS) $(SAFFRON_WINDOW_LINK_FLAGS) --make libsaffron_delegate $(SHARED_MMC_FLAGS) -L '$(ROOT)/util/window' -lsaffron_window
	install util/delegate/libsaffron_delegate.$(SO) demo/
	install util/delegate/libsaffron_delegate.$(SO) test/

util/glow/libsaffron_glfw.$(SO): build_saffron_glfw
build_saffron_glfw: build_saffron_delegate build_saffron_window build_saffron
	cd util/glfw && $(MMC) $(SAFFRON_MMC_OPTS) $(MMATH_LINK_FLAGS) $(SAFFRON_LINK_FLAGS) $(SAFFRON_DELEGATE_LINK_FLAGS) --make libsaffron_glfw $(STATIC_MMC_FLAGS) 
	cd util/glfw && $(MMC) $(SAFFRON_MMC_OPTS) $(MMATH_LINK_FLAGS) $(SAFFRON_LINK_FLAGS) $(SAFFRON_DELEGATE_LINK_FLAGS) --make libsaffron_glfw $(SHARED_MMC_FLAGS) -L '$(ROOT)/util/window' -lsaffron_window -L '$(ROOT)/util/delegate' -lsaffron_delegate -L '$(ROOT)' -lsaffron -lglfw -lGL
	install util/glfw/libsaffron_glfw.$(SO) demo/
	install util/glfw/libsaffron_glfw.$(SO) test/

build_saffron_tga:
	cd util/tga && $(MMC) $(SAFFRON_MMC_OPTS) --make libsaffron_tga $(STATIC_MMC_FLAGS)

# Demos

# Shared resources between demos and test.
# All of these are copied from the test directory to the demo directory.
demo/res/crate.tga: test/res/tga/crate.tga
	cp test/res/tga/crate.tga '$@'

demo/res/ctc24.tga: test/res/tga/ctc24.tga
	cp test/res/tga/ctc24.tga '$@'

# Dummy build to avoid the demo rules from building timer.m's files at once.
build_demo_utils:
	cd demo && $(MMC) $(SAFFRON_MMC_OPTS) --make libtimer

build_saffron_cube_demo: build_demo_utils demo/res/crate.tga build_saffron build_saffron_tga build_saffron_glow
	cd demo && $(MMC) $(SAFFRON_MMC_OPTS) -E --make saffron_cube_demo $(MMATH_LINK_FLAGS) $(SAFFRON_LINK_FLAGS) $(SAFFRON_GLOW_LINK_FLAGS) $(SAFFRON_WINDOW_LINK_FLAGS) $(SAFFRON_TGA_LINK_FLAGS) -L '$(ROOT)/glow' -lglow -lX11 -lmmath -lsaffron -lsaffron_window -lsaffron_glow --link-object '$(ROOT)/util/tga/libsaffron_tga.a' -lGL

build_saffron_shader_demo: build_demo_utils build_saffron build_saffron_glow
	cd demo && $(MMC) $(SAFFRON_MMC_OPTS) -E --make saffron_shader_demo $(MMATH_LINK_FLAGS) $(SAFFRON_LINK_FLAGS) $(SAFFRON_GLOW_LINK_FLAGS) $(SAFFRON_WINDOW_LINK_FLAGS) -L '$(ROOT)/glow' -lglow -lX11 -lmmath -lsaffron -lsaffron_window -lsaffron_glow -lGL

build_saffron_glfw_demo: build_demo_utils demo/res/crate.tga build_saffron build_saffron_glfw build_saffron_tga
	cd demo && $(MMC) $(SAFFRON_MMC_OPTS) -E --make saffron_glfw_demo $(MMATH_LINK_FLAGS) $(SAFFRON_LINK_FLAGS) $(SAFFRON_GLFW_LINK_FLAGS)  $(SAFFRON_TGA_LINK_FLAGS) -lglfw -lmmath -lsaffron -lsaffron_window -lsaffron_delegate -lsaffron_glfw --link-object '$(ROOT)/util/tga/libsaffron_tga.a' -lGL

# Testing
build_transunit:
	cd test/transunit && $(MMC) $(SAFFRON_MMC_OPTS) --make libtransunit $(STATIC_MMC_FLAGS)

test_saffron_tga: build_transunit build_saffron_tga
	cd test && $(MMC) $(SAFFRON_MMC_OPTS) --make saffron_tga_test $(TRANSUNIT_LINK_FLAGS) $(SAFFRON_TGA_LINK_FLAGS)
	cd test/res/tga && '$(ROOT)/test/saffron_tga_test'

# Clean
clean_mmath:
	DIR=mmath ; TARGET=mmath ; $(CLEAN_MERCURY)

clean_saffron:
	DIR='$(ROOT)' ; TARGET=saffron ; $(CLEAN_MERCURY)

clean_glow:
	DIR=glow ; $(CLEAN_LIB)
	rm -f glow/*.o glow/*.os

clean_saffron_glow:
	DIR=util/glow ; TARGET=saffron_glow ; $(CLEAN_MERCURY)

clean_saffron_tga:
	DIR=util/tga ; TARGET=saffron_tga ; $(CLEAN_MERCURY)

clean_saffron_window:
	DIR=util/window ; TARGET=saffron_window ; $(CLEAN_MERCURY)

clean_saffron_delegate:
	DIR=util/delegate ; TARGET=saffron_delegate ; $(CLEAN_MERCURY)

clean_saffron_glfw:
	DIR=util/glfw ; TARGET=saffron_glfw ; $(CLEAN_MERCURY)

clean_test_saffron_tga:
	DIR=test ; TARGET=test_saffron_tga ; $(CLEAN_MERCURY)

clean_saffron_cube_demo:
	DIR=demo ; TARGET=saffron_cube_demo ; $(CLEAN_MERCURY)

clean_saffron_shader_demo:
	DIR=demo ; TARGET=saffron_shader_demo ; $(CLEAN_MERCURY)

clean_saffron_glfw_demo:
	DIR=demo ; TARGET=saffron_glfw_demo ; $(CLEAN_MERCURY)

CLEAN=clean_mmath clean_saffron clean_glow clean_saffron_glow clean_saffron_window clean_saffron_delegate clean_saffron_glfw clean_saffron_tga clean_saffron_delegate clean_glow clean_test_saffron_tga clean_saffron_cube_demo clean_saffron_cube_demo clean_saffron_glfw_demo
clean: $(CLEAN)
	rm -f demo/res/crate.tga demo/res/ctc24.tga demo/lib*.$(SO) # Remove the demo libraries we copied and libtimer.

.IGNORE: $(CLEAN)
.PHONY: all demos test utils $(ALL) build_mmath $(UTILS) build_saffron_tga build_saffron_window build_glow build_saffron_glow build_saffron_delegate build_saffron_cube_demo build_test_saffron_tga build_demo_utils $(DEMOS) $(CLEAN)

