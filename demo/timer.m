% Copyright (C) 2018-2022 AlaskanEmily, Transnat Games
%
% this software is provided 'as-is', without any express or implied
% warranty.  in no event will the authors be held liable for any damages
% arising from the use of this software.
%
% permission is granted to anyone to use this software for any purpose,
% including commercial applications, and to alter it and redistribute it
% freely, subject to the following restrictions:
%
% 1. the origin of this software must not be misrepresented; you must not
%    claim that you wrote the original software. if you use this software
%    in a product, an acknowledgment in the product documentation would be
%    appreciated but is not required.
% 2. altered source versions must be plainly marked as such, and must not be
%    misrepresented as being the original software.
% 3. this notice may not be removed or altered from any source distribution.


:- module timer.

%==============================================================================%
% Time functions which aren't provided by the Mercury standard library.
:- interface.
%==============================================================================%

:- use_module io.

:- pred ms(int::uo, io.io::di, io.io::uo) is det.

:- pred sleep(int::in, io.io::di, io.io::uo) is det.

%==============================================================================%
:- implementation.
%==============================================================================%

%------------------------------------------------------------------------------%
% We need this for TimeSpan and DateTime.
:- pragma foreign_decl("C#"," using System; ").

%------------------------------------------------------------------------------%
% Declate the calling convention (gets us TCO on smarter compilers)
% We don't use a define Z2_Milliseconds right to GetTickCount because we want
% to avoid include Windows.h in the mh file.
:- pragma foreign_decl("C",
"
#if defined _WIN32 && ( defined _MSC_VER || defined __WATCOMC__ )
#define Z2_MS_CALL __stdcall
#elif defined __CYGWIN__
#define Z2_MS_CALL __attribute__((stdcall))
#else
#define Z2_MS_CALL
#endif

long Z2_MS_CALL Z2_Milliseconds(void);
void Z2_MS_CALL Z2_Sleep(long);
").

%------------------------------------------------------------------------------%
% Implementation of C get ticks
:- pragma foreign_code("C",
    "
#if defined _WIN32 || defined __CYGWIN__

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN 1
#endif
#include <Windows.h>

long Z2_MS_CALL Z2_Milliseconds() { return GetTickCount(); }
void Z2_MS_CALL Z2_Sleep(long MS) { Sleep(MS); }

#elif defined __linux__

#define DEFAULT_SOURCE_
#include <unistd.h>
#include <time.h>

#if defined CLOCK_MONOTONIC_COARSE
#define Z2_CLOCK_MONOTONIC CLOCK_MONOTONIC_COARSE
#elif defined CLOCK_MONOTONIC_RAW
#define Z2_CLOCK_MONOTONIC CLOCK_MONOTONIC_RAW
#else
#define Z2_CLOCK_MONOTONIC CLOCK_MONOTONIC
#endif

long Z2_MS_CALL Z2_Milliseconds(void){
    struct timespec t;
    clock_gettime(Z2_CLOCK_MONOTONIC, &t);
    return (t.tv_sec * 1000) + (t.tv_nsec / 1000000);
}

/* TODO: This is apparently deprecated on Linux. */
void Z2_MS_CALL Z2_Sleep(long ms){
    usleep(ms * 1000);
}

#else

 /* OS X and BSD */
#define _BSD_SOURCE
#include <unistd.h>
#include <sys/time.h>

long Z2_MS_CALL Z2_Milliseconds(void){
    struct timeval t;
    gettimeofday(&t, NULL);
    return (t.tv_sec * 1000) + (t.tv_usec / 1000);
}

void Z2_MS_CALL Z2_Sleep(long ms){
    usleep(ms * 1000);
}

#endif
    ").

%------------------------------------------------------------------------------%

:- pragma foreign_proc("C", ms(Time::uo, IO0::di, IO1::uo),
    [promise_pure, thread_safe, will_not_call_mercury, may_duplicate,
    will_not_throw_exception, does_not_affect_liveness],
    " Time = Z2_Milliseconds(); IO1 = IO0; ").

%------------------------------------------------------------------------------%

:- pragma foreign_proc("Java", ms(Time::uo, IO0::di, IO1::uo),
    [promise_pure, thread_safe, will_not_call_mercury, may_duplicate,
    will_not_throw_exception, does_not_affect_liveness],
    " Time = (int)System.currentTimeMillis(); IO1 = IO0; ").

%------------------------------------------------------------------------------%

:- pragma foreign_proc("C#", ms(Time::uo, IO0::di, IO1::uo),
    [promise_pure, thread_safe, will_not_call_mercury, may_duplicate,
    will_not_throw_exception, does_not_affect_liveness],
    " Time = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond; IO1 = IO0; ").

%------------------------------------------------------------------------------%
% Default implementation does nothing. This is kind of meh.
sleep(_, !IO).

:- pragma foreign_proc("C",
    sleep(Time::in, IOi::di, IOo::uo),
    [will_not_call_mercury, will_not_throw_exception, may_duplicate,
     thread_safe, promise_pure, does_not_affect_liveness, tabled_for_io],
    " Z2_Sleep(Time); IOo = IOi; ").
