% Released as public domain by Transnat Games for demonstration purposes.
%
% Any copyright is dedicated to the Public Domain.
% https://creativecommons.org/publicdomain/zero/1.0/

:- module saffron_glfw_demo.

%=============================================================================%
:- interface.
%=============================================================================%

:- use_module io.

%-----------------------------------------------------------------------------%

:- pred main(io.io::di, io.io::uo) is det.

%=============================================================================%
:- implementation.
%=============================================================================%

:- use_module bool.
:- use_module exception.
:- import_module float.
:- import_module int.
:- import_module list.
:- use_module math.
:- use_module maybe.
:- use_module thread.
:- use_module thread.mvar.

:- use_module mmath.
:- use_module mmath.matrix.
:- use_module mmath.vector.
:- import_module mmath.vector.vector3.
:- import_module mmath.vector.vector4.

:- use_module saffron.
:- use_module saffron.draw.
:- import_module saffron.geometry.
:- use_module saffron.gl.
:- use_module saffron.gl2.
:- use_module saffron.soft.
:- use_module saffron_window.
:- use_module saffron_delegate.
:- use_module saffron_glfw.
:- use_module saffron_tga.

:- use_module timer.

%-----------------------------------------------------------------------------%

:- type state(Ctx, Group) ---> state(thread.mvar.mvar({Ctx, Group})).

%-----------------------------------------------------------------------------%

:- pred redraw(state(Ctx, Group), io.io, io.io)
    <= (saffron.gl.context(Ctx),
        saffron.draw.basic_transform(Ctx),
        saffron.draw.transform_stack(Ctx),
        saffron.draw.draw(Ctx, Group)).
:- mode redraw(in, di, uo) is det.

redraw(state(MVar), !IO) :-
    thread.mvar.read(MVar, {Ctx, Group}, !IO),
    % Get the current time in order to animate the camera.
    timer.ms(Ticks, !IO),
    Seconds = float(int.unchecked_rem(Ticks, 6283185)) / 1000.0,
    
    % Alternatively...
    % time.clock(Ticks, !IO),
    % (time.clocks_per_sec =< 0 -> Rate = 100 ; Rate = time.clocks_per_sec),
    % Seconds = float(Ticks) / float(Rate),
    
    % Build the rotation matrix.
    % To make this more visually interesting, we also move the rotation
    % vector. If this seems confusing, just change RotateVector to be
    % `vector(U1, U1, 1.0)` or some other constant to see the rotation
    % normal in action.
    SpinT = Seconds, % / 0.23,
    Angle = Seconds * 0.87,
    RotateVector =
        mmath.vector.normalize(vector(math.sin(SpinT), 1.0, math.cos(SpinT))),
    % In general, you should always normalize vectors being used to build
    % transformation matrices.
    Matrix = mmath.matrix.rotate(Angle, RotateVector),
    
    % Here we will push the ortho matrix set in main/2 and then transform
    % it by our rotation. Then, after drawing, we pop the transformed
    % matrix to restore the ortho matrix.
    saffron.draw.push_matrix(Ctx, !IO),
    
    % In most actual programs, this would be tracked separately for the
    % model and camera.
    saffron.draw.transform(Ctx, Matrix, !IO),
    
    % Draw the model. Most real programs would iterate a set of Groups.
    saffron.draw.draw(Ctx, Group, !IO),
    
    saffron.draw.pop_matrix(Ctx, !IO).

%-----------------------------------------------------------------------------%

:- instance saffron_delegate.delegate(state(Ctx, Group))
    <= (saffron.gl.context(Ctx),
        saffron.draw.basic_transform(Ctx),
        saffron.draw.transform_stack(Ctx),
        saffron.draw.draw(Ctx, Group)) where [
    pred(saffron_delegate.key_press/4) is saffron_delegate.key_nop,
    pred(saffron_delegate.key_release/4) is saffron_delegate.key_nop,
    pred(saffron_delegate.button_press/6) is saffron_delegate.button_nop,
    pred(saffron_delegate.button_release/6) is saffron_delegate.button_nop,
    pred(saffron_delegate.mouse_motion/5) is saffron_delegate.motion_nop,
    pred(saffron_delegate.resize/5) is saffron_delegate.resize_nop,
    pred(saffron_delegate.redraw/3) is saffron_glfw_demo.redraw,
    pred(saffron_delegate.quit/3) is saffron_delegate.quit_nop
].

%-----------------------------------------------------------------------------%

:- pred run(Win, thread.mvar.mvar({T1, T2}), io.io, io.io)
    <= saffron_delegate.window(Win).
:- mode run(in, in, di, uo) is det.

run(Win, MVar, !IO) :-
    % Although this isn't really needed here, it is good hygiene to ensure that
    % you measure your game and update calls, and then sleep based on the
    % difference of the time used and expected frame time.
    timer.ms(NewTicks, !IO),
    
    saffron_delegate.update(Win, ShouldQuit, !IO),
    (
        ShouldQuit = bool.yes % Nothing else to do.
    ;
        ShouldQuit = bool.no,
        % Delay, then continue looping.
        % The delay is from now (EndMS) until the anticipated end of frame/tick
        % (NewTicks + 16). This should give us an optimistic 60 FPS.
        timer.ms(EndMS, !IO),
        SleepMS = NewTicks + 16 - EndMS,
        % Delay only if we didn't blow the frame budget.
        ( SleepMS >= 0 -> timer.sleep(SleepMS, !IO) ; true ),
        run(Win, MVar, !IO)
    ).

%-----------------------------------------------------------------------------%
% Geometry for the cube model.
%
% Normally, you will want to load a model file, or perhaps just have a few
% primitives defined in a similar to this.
% 
% 0 ----- 1
%  \       \
% | \     | \
% |  2 ----- 3
% |  |    |  |
% |  |    |  |
% 4 -|--- 5  |
%  \ |     \ |
%   \       \
%    6 ----- 7
% 
%    y
% z  |
%  \ |
%   \|
%    o----x
% 
% Faces (in triangle strip order):
% 0: 0, 1, 2, 3
% 1: 0, 2, 4, 6
% 2: 3, 1, 7, 5
% 3: 1, 0, 5, 4
% 4: 2, 3, 6, 7
% 5: 6, 7, 4, 5
:- func cube_geometry = list.list(list.list(saffron.geometry.vertex3d)).
cube_geometry = [
        [
            vertex(Vec0, U0, U0) |[
            vertex(Vec1, U1, U0) |[
            vertex(Vec2, U0, U1) |[
            vertex(Vec3, U1, U1) |[
        ]]]]] |[
        [
            vertex(Vec0, U0, U0) |[
            vertex(Vec2, U1, U0) |[
            vertex(Vec4, U0, U1) |[
            vertex(Vec6, U1, U1) |[
        ]]]]] |[
        [
            vertex(Vec3, U0, U0) |[
            vertex(Vec1, U1, U0) |[
            vertex(Vec7, U0, U1) |[
            vertex(Vec5, U1, U1) |[
        ]]]]] |[
        [
            vertex(Vec1, U0, U0) |[
            vertex(Vec0, U1, U0) |[
            vertex(Vec5, U0, U1) |[
            vertex(Vec4, U1, U1) |[
        ]]]]] |[
        [
            vertex(Vec2, U0, U0) |[
            vertex(Vec3, U1, U0) |[
            vertex(Vec6, U0, U1) |[
            vertex(Vec7, U1, U1) |[
        ]]]]] |[
        [
            vertex(Vec6, U0, U0) |[
            vertex(Vec7, U1, U0) |[
            vertex(Vec4, U0, U1) |[
            vertex(Vec5, U1, U1) |[
        ]]]]] |[
    ]]]]]]] :-
     
    Vec0 = vector(-1.0,  1.0,  1.0),
    Vec1 = vector( 1.0,  1.0,  1.0),
    Vec2 = vector(-1.0,  1.0, -1.0),
    Vec3 = vector( 1.0,  1.0, -1.0),
    Vec4 = vector(-1.0, -1.0,  1.0),
    Vec5 = vector( 1.0, -1.0,  1.0),
    Vec6 = vector(-1.0, -1.0, -1.0),
    Vec7 = vector( 1.0, -1.0, -1.0),
    U0 = 0.0,
    U1 = 1.0 .

%-----------------------------------------------------------------------------%

main(!IO) :-
    % Load the demo image
    saffron_tga.load_path("res/crate.tga", ImageResult, !IO),
    
    % The Mercury compiler is smart enough to see that the error branch will
    % throw, and will unify the result outside that disjunction.
    (
        ImageResult = io.error(Err),
        exception.throw(exception.software_error(io.error_message(Err)))
    ;
        ImageResult = io.ok({BMP, TexW, TexH})
    ),
    W = 640,
    H = 480,
    Title = "Saffron GLFW Demo",
    
    thread.mvar.init(MVar, !IO),
    saffron_glfw.create_window(state(MVar), W, H, Title, 2, 0, WinRes, !IO),
    (
        WinRes = io.error(Err),
        exception.throw(exception.software_error(io.error_message(Err)))
    ;
        WinRes = io.ok(Win)
    ),
    
    % Using the backend-specific type is what determines what backend Glow is
    % using.
    saffron.gl2.init(Win, Ctx, !IO),
    
    saffron.gl.opengl_version_string(Ctx, GLVersion, !IO),
    io.write_string("OpenGL Version: ", !IO),
    io.write_string(GLVersion, !IO),
    io.nl(!IO),
    
    saffron.gl.shader_model_version_string(Ctx, GLSLVersion, !IO),
    io.write_string("GLSL Version: ", !IO),
    io.write_string(GLSLVersion, !IO),
    io.nl(!IO),
    
    % Set the ortho mode
    AR = float(W) / float(H),
    
    Matrix = mmath.matrix.ortho(-AR, AR, 1.0, -1.0, -1000.0, 1000.0),
    saffron.draw.set_matrix(Ctx, Matrix, !IO),
    
    saffron.draw.transform(Ctx, mmath.matrix.scale(0.4, 0.4, 0.4), !IO),
    
    % Upload the image to create a texture.
    saffron.gl.create_texture_from_bitmap(Ctx, BMP, TexW, TexH, Texture, !IO),
    Faces = cube_geometry,
    
    % Upload geometry to buffers.
    % Most programs would probably want to use index buffers for a shape like
    % this with mostly shared geometry.
    list.map_foldl(
        saffron.geometry.create_buffer_list(Ctx),
        Faces, FaceBuffers,
        !IO),
    
    % Construct the faces.
    % This *could* be done as one large, wound shape, but the majority of
    % programs will be loading meshes that will contain individual hulls/faces
    % that will need to be constructed in this way.
    list.map_foldl(
        saffron.geometry.create_shape2(Ctx, saffron.geometry.triangle_strip, Texture),
        FaceBuffers,
        Shapes,
        !IO),
    
    % Construct the shape.
    saffron.draw.create_group_list(Ctx, Shapes, Group, !IO),
    
    thread.mvar.put(MVar, {Ctx, Group}, !IO),
    % Run the engine.
    run(Win, MVar, !IO),
    
    % Cleanup
    saffron.destroy(Ctx, Group, !IO),
    list.foldl(saffron.destroy(Ctx), Shapes, !IO),
    list.foldl(saffron.destroy(Ctx), FaceBuffers, !IO),
    saffron.destroy(Ctx, Texture, !IO).

