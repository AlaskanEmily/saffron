# Runs a demo, setting the appropriate library path.
PWD=`pwd`
if test -z "$WINDIR" ; then
    SEP=':'
    PTE='/'
    case $JAVA,$2 in
        ,--java)
            PATH="$PATH:$JAVA_HOME/bin"
            JAVA=`which java 2>/dev/null || ls "$JAVA_HOME"/bin/java 2>/dev/null || echo java`
            ;;
    esac
    EXTRA_PATH="$PWD"
    case `uname` in
        *BSD)
            EXTRA_PATH="$EXTRA_PATH:/usr/local/lib:/usr/X11R6/lib:/usr/local/share/lwjgl3"
            for JAR in /usr/local/share/lwjgl3/*.jar ; do
                case "$JAR" in
                    *opengles*) : ;;
                    *lwjgl.jar|*lwjgl-natives*.jar|*glfw*.jar|*opengl*.jar) LWJGL_PATH="$LWJGL_PATH:$JAR" ;;
                esac
            done
            ;;
        Linux*)
            EXTRA_PATH="$EXTRA_PATH:/usr/local/share/lwjgl3"
            for JAR in /usr/local/share/lwjgl3/*.jar ; do
                case "$JAR" in
                    *opengles*) : ;;
                    *lwjgl.jar|*glfw*.jar|*opengl*.jar|*jemalloc*|*sse*) LWJGL_PATH="$LWJGL_PATH:$JAR" ;;
                esac
            done
            ;;
    esac
    export LD_LIBRARY_PATH="$EXTRA_PATH:$LD_LIBRARY_PATH"
    if test ! -z "$SAFFRON_DEBUG_LIBS" ; then
        echo " $PWD"
        echo " $LD_LIBRARY_PATH"
        ldd " $1"
    fi
else
    SEP=';'
    PTE='\'
    case $JAVA,$2 in
        ,--java)
            PATH="$PATH:$JAVA_HOME/bin"
            for J in java javaw ; do
                JAVA=`which $J 2>/dev/null || dir "$JAVA_HOME"/bin/$J.exe 2>/dev/null`
                test -z "$JAVA" || break
            done
            ;;
    esac
fi
export PATH="$PWD"$SEP"$PATH"
case "$2" in
    --java)
        TARGET="$1"
        shift
        shift
        if test -z "$LWJGL_PATH" ; then
            for JAR in lwjgl*.jar ; do
                LWJGL_PATH="$LWJGL_PATH"$SEP"$PWD"$PTE"$JAR"
            done
        fi
        CLASSPATH="$CLASSPATH"$SEP"$LWJGL_PATH"
        test -z "$JAVA" && JAVA=java
        for JAR in *.jar ; do
            case "$JAR" in
                *_demo.jar|lwjgl*) : ;;
                *) CLASSPATH="$CLASSPATH"$SEP"$PWD"$PTE"$JAR" ;;
            esac
        done
        export CLASSPATH="$PWD"$PTE"$TARGET.jar:$CLASSPATH"
        echo " $CLASSPATH"
        echo " $LD_LIBRARY_PATH"
        exec "$JAVA" "jmercury.$TARGET" $@
esac
exec $@
