% Released as public domain by Transnat Games for demonstration purposes.
%
% Any copyright is dedicated to the Public Domain.
% https://creativecommons.org/publicdomain/zero/1.0/

:- module saffron_shader_demo.

%=============================================================================%
% A demo of using shaders, animating a texture based on shader inputs.
%
% This only uses the Glow with OpenGL2 backend, as those are expected to work
% on most computers.
%
% This does not load any textures, and just allocates a blank bitmap at runtime
% which is then modified by the shader.
%
% TODO: This should really be a generic module that just uses the saffron
% typeclasses to handle each backend, which would be a better demonstration
% of how to use this type of library.
:- interface.
%=============================================================================%

:- use_module io.

%-----------------------------------------------------------------------------%

:- pred main(io.io::di, io.io::uo) is det.

%=============================================================================%
:- implementation.
%=============================================================================%

:- use_module exception.
:- import_module float.
:- import_module int.
:- import_module list.
:- use_module maybe.
:- use_module string.
:- use_module time.

:- use_module mmath.
:- use_module mmath.matrix.
:- use_module mmath.vector.
:- import_module mmath.vector.vector3.
:- import_module mmath.vector.vector4.

:- use_module timer.

:- use_module saffron.
:- use_module saffron.draw.
:- import_module saffron.geometry.
:- use_module saffron.gl.
:- use_module saffron.gl.shader.
:- use_module saffron.gl2.
:- use_module saffron.soft.
:- use_module saffron.texture.
:- use_module saffron_window.
:- use_module saffron_glow.

%-----------------------------------------------------------------------------%

:- pred run(pred(float, io.io, io.io), Ctx, Win, Group, io.io, io.io)
    <= (saffron_window.window(Win),
        saffron.gl.context(Ctx),
        saffron.draw.basic_transform(Ctx),
        saffron.draw.transform_stack(Ctx),
        saffron.draw.draw(Ctx, Group)).
:- mode run(pred(in, di, uo) is det, in, in, in, di, uo) is det.

run(UpdateUniform, Ctx, Win, Group, !IO) :-
    saffron_window.get_window_event(Win, MaybeEvent, !IO),
    (
        MaybeEvent = maybe.yes(Event),
        % Handle the event. Most programs will want to do more than just
        % quitting or continuing.
        (
            Event = saffron_window.quit
            % Just quit here, do not recurse.
        ;
            % Key press
            % Most programs will want to track the list of pressed keys and add
            % the key to a set or list.
            Event = saffron_window.press(_),
            run(UpdateUniform, Ctx, Win, Group, !IO)
        ;
            % Key release
            Event = saffron_window.release(_),
            run(UpdateUniform, Ctx, Win, Group, !IO)
        ;
            % Mouse button press
            % Some programs will want to track what mouse buttons are held.
            Event = saffron_window.press(_, _, _),
            run(UpdateUniform, Ctx, Win, Group, !IO)
        ;
            % Mouse button release
            Event = saffron_window.release(_, _, _),
            run(UpdateUniform, Ctx, Win, Group, !IO)
        ;
            % Mouse motion.
            % Some programs will want to track where the mouse is.
            Event = saffron_window.motion(_, _),
            run(UpdateUniform, Ctx, Win, Group, !IO)
        ;
            % Window resize. This currently can't be returned from the Glow
            % backend.
            Event = saffron_window.resize(_, _),
            run(UpdateUniform, Ctx, Win, Group, !IO)
        )
    ;
        MaybeEvent = maybe.no,
        
        % Get the current time in order to animate the camera.
        timer.ms(Ticks, !IO),
        Seconds = float(int.unchecked_rem(Ticks, 6283185)) / 1000.0,
        UpdateUniform(Seconds, !IO),
        
        % Alternatively...
        % time.clock(Ticks, !IO),
        % (time.clocks_per_sec =< 0 -> Rate = 100 ; Rate = time.clocks_per_sec),
        % Seconds = float(Ticks) / float(Rate),
        
        % Draw the model. Most real programs would iterate a set of Groups.
        saffron.draw.draw(Ctx, Group, !IO),
        
        % Performing the flip_screen ends the current frame and begins the next
        % frame.
        % You may want to do a sleep here for some programs, or enable vsync.
        % Otherwise, on some platforms your program will use 100% of a CPU.
        saffron_window.flip_screen(Win, !IO),
        
        run(UpdateUniform, Ctx, Win, Group, !IO)
    ).


%-----------------------------------------------------------------------------%

:- func billboard_geometry = list.list(saffron.geometry.vertex3d).
billboard_geometry = [
        vertex(vector(-0.5, -0.5, 0.0), 0.0, 0.0) |[
        vertex(vector( 0.5, -0.5, 0.0), 1.0, 0.0) |[
        vertex(vector(-0.5,  0.5, 0.0), 0.0, 1.0) |[
        vertex(vector( 0.5,  0.5, 0.0), 1.0, 1.0) |[
    ]]]]].

%-----------------------------------------------------------------------------%
% See saffron_gl.txt and saffron.gl.m for documentation on standard Saffron
% shader variables in GLSL.
:- func vertex_shader_source = string.
vertex_shader_source = "#version 110
void main(void){
    gl_Position = gl_Vertex;
}
".

:- func fragment_shader_source = string.
fragment_shader_source = "#version 110
uniform float saffron_demo_time;
void main(void){
    float val = gl_FragCoord.x + gl_FragCoord.y + saffron_demo_time;
    float mod = sin(val / 64.0) + 1.0;
    gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0) * mod * 0.5;
}
".

%-----------------------------------------------------------------------------%

main(!IO) :-
    W = 640,
    H = 480,
    Title = "Saffron Shader Demo",
    
    % Create the window and OpenGL context
    saffron_glow.viewport_size(W, H, WinW, WinH),
    saffron_glow.create_window(WinW, WinH, Title, WinRes, !IO),
    
    % The Mercury compiler is smart enough to see that the error branch will
    % throw, and will unify the result outside that disjunction.
    (
        WinRes = io.error(Err),
        exception.throw(exception.software_error(io.error_message(Err)))
    ;
        WinRes = io.ok(Win)
    ),
    saffron_glow.create_context(Win, 1, 3, GlowCtxRes, !IO),
    (
        GlowCtxRes = io.error(Err),
        exception.throw(exception.software_error(io.error_message(Err)))
    ;
        GlowCtxRes = io.ok(GlowCtx)
    ),
    
    % Show the window.
    % Depending on the Glow backend, this *might* need to happen after creating
    % the OpenGL context, but not necessarily.
    saffron_glow.show_window(Win, !IO),
    
    % Using the backend-specific type is what determines what backend Glow is
    % using.
    saffron.gl2.init(GlowCtx, Ctx, !IO),
    
    saffron.gl.opengl_version_string(Ctx, GLVersion, !IO),
    io.write_string("OpenGL Version: ", !IO),
    io.write_string(GLVersion, !IO),
    io.nl(!IO),
    
    saffron.gl.shader_model_version_string(Ctx, GLSLVersion, !IO),
    io.write_string("GLSL Version: ", !IO),
    io.write_string(GLSLVersion, !IO),
    io.nl(!IO),
    
    MaybeShaderCtx = Ctx ^ saffron.gl2.shader_ctx,
    (
        MaybeShaderCtx = maybe.no,
        exception.throw(exception.software_error(
            "Shaders not supported by this OpenGL configuration"))
    ;
        MaybeShaderCtx = maybe.yes(ShaderCtx)
    ),
    
    % Setup the shader.
    % In most games and graphical programs, these will most commonly be loaded
    % from a file. Many programs will also want to use some kind of template
    % system to be able to generate different GLSL version files from a single
    % set of inputs, but in this case we're only using version 1(00).
    %
    % The interface to create shaders is implemented in Saffron in a graphics-
    % independent way. The format and language of the shader depends on which
    % backend is used. To support a graphics backend, the programmer will need
    % need to consider different shader formats.
    % This must be handled on a per-graphics-backend basis.
    saffron.gl.shader.create_vertex_shader(
        ShaderCtx,
        vertex_shader_source,
        MaybeVertexShader,
        !IO),
    % Most programs will want to either use some kind of fallback, report
    % a failure to the user rather than just failing, or log the error and then
    % not load this asset.
    % For simplicity, we are just going to throw an exception.
    (
        MaybeVertexShader = maybe.ok(VertexShader)
    ;
        MaybeVertexShader = maybe.error(VertexShaderError),
        exception.throw(exception.software_error(
            string.append("Vertex shader compile error: ", VertexShaderError)))
    ),
    
    saffron.gl.shader.create_fragment_shader(
        ShaderCtx,
        fragment_shader_source,
        MaybeFragmentShader,
        !IO),
    (
        MaybeFragmentShader = maybe.ok(FragmentShader)
    ;
        MaybeFragmentShader = maybe.error(FragmentShaderError),
        exception.throw(exception.software_error(
            string.append("Fragment shader compile error: ", FragmentShaderError)))
    ),
    
    saffron.gl.shader.create_program(
        ShaderCtx,
        FragmentShader,
        VertexShader,
        MaybeShaderProgram,
        !IO),
    (
        MaybeShaderProgram = maybe.ok(ShaderProgram)
    ;
        MaybeShaderProgram = maybe.error(ShaderProgramError),
        exception.throw(exception.software_error(
            string.append("Shader linking error: ", ShaderProgramError)))
    ),
    
    % Build the shape and group
    saffron.texture.create_empty(Ctx, 16, 16, Tex, !IO),
    saffron.geometry.create_buffer_list(Ctx, billboard_geometry, Buffer, !IO),
    saffron.geometry.create_shape(Ctx, triangle_strip, Buffer, Tex, Shape, !IO),
    saffron.draw.create_group_list(Ctx, ShaderProgram, [Shape|[]], Group, !IO),
    
    % Set the ortho mode
    AR = float(W) / float(H),
    
    Matrix = mmath.matrix.ortho(-AR, AR, 1.0, -1.0, -1000.0, 1000.0),
    saffron.draw.set_matrix(Ctx, Matrix, !IO),
    
    saffron.gl.shader.get_uniform_location(ShaderCtx, ShaderProgram, "saffron_demo_time", MaybeUniform, !IO),
    UpdateUniform = (pred(Seconds::in, !.XIO::di, !:XIO::uo) is det :-
        (
            MaybeUniform = maybe.no
        ;
            MaybeUniform = maybe.yes(Uniform),
            saffron.gl.shader.uniform(ShaderCtx, Uniform, Seconds * 100.0, !XIO)
        ) ),
    
    run(UpdateUniform, Ctx, Win, Group, !IO),
    
    saffron.destroy(Ctx, Group, !IO),
    saffron.destroy(Ctx, Shape, !IO),
    saffron.destroy(Ctx, Buffer, !IO),
    saffron.destroy(Ctx, Tex, !IO),
    saffron.destroy(Ctx, ShaderProgram, !IO),
    saffron.destroy(Ctx, VertexShader, !IO),
    saffron.destroy(Ctx, FragmentShader, !IO).

