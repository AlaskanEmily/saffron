% Copyright (c) 2021 Transnat Games, AlaskanEmily
%
% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.

:- module saffron.gl4.
%=============================================================================%
% OpenGL 4.1 renderer.
%
% Uses OpenGL buffers and VAOs, has support for both generic shaders and
% GLSL shaders.
:- interface.
%=============================================================================%

:- use_module vector.

:- use_module saffron.draw.
:- use_module saffron.geometry.
:- use_module saffron.texture.
:- use_module saffron.gl.

%-----------------------------------------------------------------------------%

:- type context(Ctx).

%-----------------------------------------------------------------------------%

:- instance saffron.gl.supports_extensions(context(Ctx))
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- instance saffron.gl.context(context(Ctx))
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- instance saffron.draw.basic_transform(context(Ctx))
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- instance saffron.draw.transform_stack(context(Ctx))
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- instance saffron.draw.transform(context(Ctx))
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- instance saffron.context(context(Ctx))
    <= saffron.context(Ctx).

%-----------------------------------------------------------------------------%

:- type texture == saffron.gl.texture.

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(context(Ctx), texture)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- instance saffron.texture.texture(context(Ctx), texture)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- type vertex(Vec) == saffron.geometry.vertex(Vec).
:- type vertex2d == vertex(vector.vector2).
:- type vertex3d == vertex(vector.vector3).

%-----------------------------------------------------------------------------%

:- type buffer2d.
:- type buffer3d.

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(context(Ctx), buffer2d)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(context(Ctx), buffer3d)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- instance saffron.geometry.buffer(context(Ctx), buffer2d, vector.vector2)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- instance saffron.geometry.buffer(context(Ctx), buffer3d, vector.vector3)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- type color_buffer.

%-----------------------------------------------------------------------------%

:- instance saffron.draw.color_buffer(context(Ctx), color_buffer)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(context(Ctx), color_buffer)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- type shape2d.
:- type shape3d.

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(context(Ctx), shape2d)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(context(Ctx), shape3d)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- instance saffron.geometry.shape(context(Ctx), shape2d, buffer2d, texture)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- instance saffron.geometry.shape(context(Ctx), shape3d, buffer3d, texture)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- type group2d.
:- type group3d.

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(context(Ctx), group2d)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(context(Ctx), group3d)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- instance saffron.draw.group(context(Ctx), group2d, shape2d)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- instance saffron.draw.group(context(Ctx), group3d, shape3d)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- instance saffron.draw.draw(context(Ctx), group2d)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- instance saffron.draw.draw(context(Ctx), group3d)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- pred init(context(Ctx), io.io, io.io)
    <= saffron.gl.context(Ctx).
:- mode init(in, di, uo) is det.

%-----------------------------------------------------------------------------%

:- type shader.

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(context(Ctx), shader)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- func default_shader3d(context(Ctx)) = shader.
:- func default_shader2d(context(Ctx)) = shader.

%=============================================================================%
:- implementation.
%=============================================================================%

:- use_module array.
:- use_module bitmap.
:- use_module exception.
:- import_module float.
:- import_module int.
:- import_module list.

%-----------------------------------------------------------------------------%

% Internal types.
:- type vertex_array.
:- type buffer.
:- type extensions.

%-----------------------------------------------------------------------------%

:- type context(Ctx) ---> context(
    ext::extensions,
    saffron.draw.transform_context(Ctx)).

%-----------------------------------------------------------------------------%

:- pragma foreign_decl("C",
    "
#ifndef GL_ARRAY_BUFFER 
# define GL_ARRAY_BUFFER 0x8892
#endif
#ifndef GL_STREAM_DRAW
# define GL_STREAM_DRAW 0x88E0
#endif
#ifndef GL_STREAM_READ
# define GL_STREAM_READ 0x88E1
#endif
#ifndef GL_STREAM_COPY
# define GL_STREAM_COPY 0x88E2
#endif
#ifndef GL_STATIC_DRAW
# define GL_STATIC_DRAW 0x88E4
#endif
#ifndef GL_STATIC_READ
# define GL_STATIC_READ 0x88E5
#endif
#ifndef GL_STATIC_COPY
# define GL_STATIC_COPY 0x88E6
#endif
#ifndef GL_DYNAMIC_DRAW
# define GL_DYNAMIC_DRAW 0x88E8
#endif
#ifndef GL_DYNAMIC_READ
# define GL_DYNAMIC_READ 0x88E9
#endif
#ifndef GL_DYNAMIC_COPY
# define GL_DYNAMIC_COPY 0x88EA
#endif

#define SAFFIRE_GL4_FOR_EACH_EXT(X) \\
    X(GenBuffers) \\
    X(DeleteBuffers) \\
    X(BindBuffer) \\
    X(BufferData) \\
    X(MapBuffer) \\
    X(UnmapBuffer) \\
    X(GenVertexArrays) \\
    X(DeleteVertexArrays) \\
    X(BindVertexArray) \\
    X(VertexAttribPointer) \\
    X(EnableVertexAttribArray) \\
    X(DisableVertexAttribArray) \\

/* These can use MR_Word to get pointer-sized integer. */
typedef void (APIENTRY*Saffron_GL4_GenBuffers_t)(GLsizei n, GLuint *buffers);
typedef void (APIENTRY*Saffron_GL4_DeleteBuffers_t)(GLsizei n, const GLuint *buffers);
typedef void (APIENTRY*Saffron_GL4_BindBuffer_t)(GLenum e, GLuint buffers);
typedef void (APIENTRY*Saffron_GL4_BufferData_t)(GLenum e, MR_Word s, const void *data, GLenum data);
typedef void *(APIENTRY*Saffron_GL4_MapBuffer_t)(GLenum target, GLenum access);
typedef GLboolean (APIENTRY*Saffron_GL4_UnmapBuffer_t)(GLenum target);
typedef void (APIENTRY*Saffron_GL4_GenVertexArrays_t)(GLsizei n, GLuint *arrays);
typedef void (APIENTRY*Saffron_GL4_DeleteVertexArrays_t)(GLsizei n, const GLuint *buffers);
typedef void (APIENTRY*Saffron_GL4_BindVertexArray_t)(GLuint a);

#if (defined _WIN32) || (defined WIN32)
/* We strongly expect other platforms to have these functions.
 * PORTING: If they do not, add add them to the conditional above.
 *
 * TODO: We should add proc loading to the window module and use those.
 */
#define SAFFIRE_GL4_EXT_FIELD(NAME) \\
    Saffron_GL4_ ## NAME ## _t Saffron_GL4_ ## NAME;
typedef struct{
    SAFFIRE_GL4_FOR_EACH_EXT(SAFFIRE_GL4_EXT_FIELD)
} *Saffron_GL4_Ext;
# define SAFFRON_GL4_EXT(E, NAME) ((E)->Saffron_GL4_ ## NAME)
# define SAFFRON_GL4_CREATE_EXT() Saffron_GL4_InitExt()

Saffron_GL4_Ext Saffron_GL4_InitExt(void);

#else
typedef MR_Word Saffron_GL4_Ext;
# define SAFFRON_GL4_EXT(_E, NAME) (gl ## NAME)
# define SAFFRON_GL4_CREATE_EXT() (1)
#endif
    ").

:- pragma foreign_code("C",
    "
/* PORTING: This will need something better for finding if an extension is
 * present when not on Windows.
 */

#if (defined _WIN32) || (defined WIN32)
Saffron_GL4_Ext Saffron_GL4_InitExt(void){
    Saffron_GL4_Ext ext;
    ext = MR_GC_malloc_atomic(sizeof(*ext));
    /* PORTING: This uses WGL directly right now. */
    const char *exts = glGetString(GL_EXTENSIONS);
    
# define SAFFIRE_GL4_LOAD_EXT(NAME) \\
    if(!(ext->Saffron_GL4_ ## NAME = \\
        (Saffron_GL4_ ## NAME ## _t)wglGetProcAddress(""gl"" #NAME))){ \\
        goto fail; \\
    }
    
    SAFFIRE_GL4_FOR_EACH_EXT(SAFFIRE_GL4_LOAD_EXT)
    
    return ext;
fail:
    MR_GC_free(ext); \\
    return NULL; \\
}
#endif
    ").

%-----------------------------------------------------------------------------%

:- pragma foreign_type("C", buffer, "GLuint").
:- pragma foreign_type("C", vertex_array, "GLuint").
:- pragma foreign_type("C", shader, "GLuint").
:- pragma foreign_type("C", extensions, "Saffron_GL4_Ext").

:- type buffer2d ---> buffer2d(buffer).
:- type buffer3d ---> buffer3d(buffer).
:- type color_buffer ---> color_buffer(buffer).

%-----------------------------------------------------------------------------%

:- pred create_buffer(extensions::in, buffer::uo, io.io::di, io.io::uo) is det.
:- pragma foreign_proc("C", create_buffer(Ext::in, Buf::uo, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    tabled_for_io, may_duplicate, does_not_affect_liveness],
    "
    IOo = IOi;
    SAFFRON_GL4_EXT(Ext, GenBuffers)(&Buf, 1);
    ").

%-----------------------------------------------------------------------------%

:- pred destroy_buffer(extensions::in, buffer::in, io.io::di, io.io::uo) is det.
:- pragma foreign_proc("C", destroy_buffer(Ext::in, Buf::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    tabled_for_io, may_duplicate, does_not_affect_liveness],
    "
    IOo = IOi;
    SAFFRON_GL4_EXT(Ext, DeleteBuffers)(&Buf, 1);
    ").

%-----------------------------------------------------------------------------%

:- pred bind_buffer(extensions::in, buffer::in, io.io::di, io.io::uo) is det.
:- pragma foreign_proc("C", bind_buffer(Ext::in, Buf::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    tabled_for_io, may_duplicate, does_not_affect_liveness],
    "
    IOo = IOi;
    SAFFRON_GL4_EXT(Ext, BindBuffer)(GL_ARRAY_BUFFER, Buf);
    ").

%-----------------------------------------------------------------------------%

:- type shape2d ---> shape2d(
    primitive_type_2d::saffron.gl.primitive_type,
    buffer_2d::buffer2d,
    vertex_array_2d::vertex_array,
    buffer_len_2d::int,
    texture_2d::texture).

%-----------------------------------------------------------------------------%

:- type shape3d ---> shape3d(
    primitive_type_3d::saffron.gl.primitive_type,
    buffer_3d::buffer3d,
    vertex_array_3d::vertex_array,
    buffer_len_3d::int,
    texture_3d::texture).

%-----------------------------------------------------------------------------%

:- type group3d ---> group2d(array.array(shape3d)).

%-----------------------------------------------------------------------------%

:- type group2d ---> group2d(array.array(shape2d)).

%-----------------------------------------------------------------------------%

:- instance saffron.gl.supports_extensions(context(Ctx))
    <= saffron.gl.context(Ctx) where [
    (saffron.gl.supports(_, _) :- false)
].

%-----------------------------------------------------------------------------%

:- instance saffron.gl.context(context(Ctx))
    <= saffron.gl.context(Ctx) where [].

%-----------------------------------------------------------------------------%

:- instance saffron.draw.basic_transform(context(Ctx))
    <= saffron.gl.context(Ctx) where [
    
    (saffron.draw.set_matrix(context(_, Ctx), Matrix, !IO) :-
        saffron.draw.set_matrix(Ctx, Matrix, !IO))
].

%-----------------------------------------------------------------------------%

:- instance saffron.draw.transform_stack(context(Ctx))
    <= saffron.gl.context(Ctx) where [
    
    (saffron.draw.transform(context(_, Ctx), Matrix, !IO) :-
        saffron.draw.transform(Ctx, Matrix, !IO)),
    (saffron.draw.identity(context(_, Ctx), !IO) :-
        saffron.draw.identity(Ctx, !IO)),
    (saffron.draw.push_matrix(context(_, Ctx), !IO) :-
        saffron.draw.push_matrix(Ctx, !IO)),
    (saffron.draw.pop_matrix(context(_, Ctx), !IO) :-
        saffron.draw.pop_matrix(Ctx, !IO))
        
].

%-----------------------------------------------------------------------------%

:- instance saffron.draw.transform(context(Ctx))
    <= saffron.gl.context(Ctx) where [].

%-----------------------------------------------------------------------------%

:- instance saffron.context(context(Ctx))
    <= saffron.context(Ctx) where [].

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(context(Ctx), texture)
    <= saffron.gl.context(Ctx) where [
    
    pred(saffron.destroy/4) is saffron.gl.destroy_texture
].

%-----------------------------------------------------------------------------%

:- instance saffron.texture.texture(context(Ctx), texture)
    <= saffron.gl.context(Ctx) where [
    
    pred(saffron.texture.create_empty/6) is saffron.gl.create_empty_texture,
    pred(saffron.texture.create_from_bitmap/7) is saffron.gl.create_texture_from_bitmap,
    pred(saffron.texture.upload_at/9) is saffron.gl.texture_upload_at
].

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(context(Ctx), group2d)
    <= saffron.gl.context(Ctx) where [
    saffron.destroy(_, _, !IO)
].

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(context(Ctx), group3d)
    <= saffron.gl.context(Ctx) where [
    saffron.destroy(_, _, !IO)
].

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(context(Ctx), buffer2d)
    <= saffron.gl.context(Ctx) where [
    (saffron.destroy(context(Ext, _), buffer2d(B), !IO) :-
        destroy_buffer(Ext, B, !IO))
].

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(context(Ctx), buffer3d)
    <= saffron.gl.context(Ctx) where [
    (saffron.destroy(context(Ext, _), buffer3d(B), !IO) :-
        destroy_buffer(Ext, B, !IO))
].

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(context(Ctx), color_buffer)
    <= saffron.gl.context(Ctx) where [
    (saffron.destroy(context(Ext, _), color_buffer(B), !IO) :-
        destroy_buffer(Ext, B, !IO))
].

%-----------------------------------------------------------------------------%



