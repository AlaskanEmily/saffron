% Copyright (c) 2021-2023 Transnat Games, AlaskanEmily
%
% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.

:- module saffron.gl2.
%=============================================================================%
% Legacy OpenGL 2.0 renderer, requiring no extensions.
%
% Due to limitations in how typeclasses work, we must wrap both buffers and
% shapes. We can't just do something like `instance(buffer(Vec), Vec)` since
% without strict functional dependencies typeclasses can't have bare type
% variables as arguments.
:- interface.
%=============================================================================%

:- use_module rbtree.

:- use_module mmath.
:- use_module mmath.vector.

:- use_module saffron.draw.
:- use_module saffron.geometry.
:- use_module saffron.texture.
:- use_module saffron.gl.
:- use_module saffron.gl.buffer.
:- use_module saffron.gl.shader.
:- use_module saffron.soft.

%-----------------------------------------------------------------------------%

:- type context(Ctx) ---> context(
    ctx::Ctx,
    buffer_ctx::maybe.maybe(saffron.gl.buffer.buffer_ctx),
    shader_ctx::maybe.maybe(saffron.gl.shader.shader_ctx)).

%-----------------------------------------------------------------------------%

:- instance saffron.draw.basic_transform(context(Ctx))
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- instance saffron.gl.context(context(Ctx))
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- instance saffron.draw.transform_stack(context(Ctx))
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- instance saffron.draw.transform(context(Ctx))
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- instance saffron.context(context(Ctx))
    <= saffron.context(Ctx).

%-----------------------------------------------------------------------------%

:- type texture == saffron.gl.texture.

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(context(Ctx), texture)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- instance saffron.texture.texture(context(Ctx), texture)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- type vertex(Vec) == saffron.geometry.vertex(Vec).
:- type vertex2d == vertex(mmath.vector.vector2).
:- type vertex3d == vertex(mmath.vector.vector3).

%-----------------------------------------------------------------------------%

:- type buffer(Vec) == saffron.soft.buffer(vertex(Vec)).
:- type buffer2d ---> buffer(buffer(mmath.vector.vector2)).
:- type buffer3d ---> buffer(buffer(mmath.vector.vector3)).

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(context(Ctx), buffer2d)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(context(Ctx), buffer3d)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- instance saffron.geometry.buffer(context(Ctx), buffer2d, mmath.vector.vector2)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- instance saffron.geometry.buffer(context(Ctx), buffer3d, mmath.vector.vector3)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- type color_buffer == saffron.soft.color_buffer.

%-----------------------------------------------------------------------------%

:- instance saffron.draw.color_buffer(context(Ctx), color_buffer)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(context(Ctx), color_buffer)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- type shape(Vec) == saffron.soft.shape(Vec, texture).
:- type shape2d ---> shape(shape(mmath.vector.vector2)).
:- type shape3d ---> shape(shape(mmath.vector.vector3)).

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(context(Ctx), shape2d)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(context(Ctx), shape3d)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- instance saffron.geometry.shape(context(Ctx), shape2d, buffer2d, texture)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- instance saffron.geometry.shape(context(Ctx), shape3d, buffer3d, texture)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- type group(Vec) == saffron.soft.group(Vec, texture).
% TODO: We should be smarter than just an rbtree, since there are 4+ common
% values and in general we know the list will be small in size.
:- type group2d --->
    group(group(mmath.vector.vector2)) ;
    group(
        saffron.gl.shader.shader_program,
        rbtree.rbtree(string, saffron.gl.shader.vertex_attrib),
        group(mmath.vector.vector2)).

:- type group3d --->
    group(group(mmath.vector.vector3)) ;
    group(
        saffron.gl.shader.shader_program,
        rbtree.rbtree(string, saffron.gl.shader.vertex_attrib),
        group(mmath.vector.vector3)).

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(context(Ctx), group2d)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(context(Ctx), group3d)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- instance saffron.draw.group(context(Ctx), group2d, shape2d)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- instance saffron.draw.group(context(Ctx), group3d, shape3d)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- type shader == saffron.gl.shader.shader_program.

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(context(Ctx), shader)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(context(Ctx), saffron.gl.shader.shader)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- instance saffron.draw.group(context(Ctx), group2d, shape2d, shader)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- instance saffron.draw.group(context(Ctx), group3d, shape3d, shader)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- instance saffron.draw.draw(context(Ctx), group2d)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- instance saffron.draw.draw(context(Ctx), group3d)
    <= saffron.gl.context(Ctx).

%-----------------------------------------------------------------------------%

:- pred init(Ctx, context(Ctx), io.io, io.io)
    <= saffron.gl.context(Ctx).
:- mode init(in, out, di, uo) is det.

%=============================================================================%
:- implementation.
%=============================================================================%

:- use_module array.
:- use_module exception.
:- use_module list.

:- use_module mmath.matrix.
:- import_module mmath.vector.vector2.
:- import_module mmath.vector.vector3.

%-----------------------------------------------------------------------------%

:- pragma foreign_decl("Java", "
import org.lwjgl.opengl.GL11; // The name saffron.gl2 a lie, it's really 1.1
    ").

%-----------------------------------------------------------------------------%

:- pragma foreign_decl("C", "
// #define SAFFRON_GL2_DEBUG
#ifdef SAFFRON_GL2_DEBUG
const char *Saffron_GL2_DebugError(void);
#define SAFFRON_GL2_DEBUG_PRINT(WHAT) do{ \\
    if(WHAT) \\
      fputs((WHAT), stderr); \\
    else \\
      fputs(#WHAT "" == <NIL>"", stderr); \\
}while(0)

#else
#define Saffron_GL2_DebugError() 0
#define SAFFRON_GL2_DEBUG_PRINT(WHAT) ((void)(sizeof(WHAT)))
#endif
#define SAFFRON_GL2_DEBUG_ERROR(WHAT) do{ \\
    SAFFRON_GL2_DEBUG_PRINT(WHAT); \\
    SAFFRON_GL2_DEBUG_PRINT("": ""); \\
    SAFFRON_GL2_DEBUG_PRINT(Saffron_GL2_DebugError()); \\
    SAFFRON_GL2_DEBUG_PRINT(""\\n""); \\
}while(0)

").

%-----------------------------------------------------------------------------%

:- pragma foreign_code("C", "
#ifdef SAFFRON_GL2_DEBUG
const char *Saffron_GL2_DebugError(void){
#define SAFFRON_GL2_ERROR_CASE(WHAT) \\
        case WHAT: assert(WHAT == GL_NO_ERROR); return #WHAT
    switch(glGetError()){
        SAFFRON_GL2_ERROR_CASE(GL_NO_ERROR);
        SAFFRON_GL2_ERROR_CASE(GL_INVALID_ENUM);
        SAFFRON_GL2_ERROR_CASE(GL_INVALID_VALUE);
        SAFFRON_GL2_ERROR_CASE(GL_INVALID_OPERATION);
        SAFFRON_GL2_ERROR_CASE(GL_STACK_OVERFLOW);
        SAFFRON_GL2_ERROR_CASE(GL_STACK_UNDERFLOW);
        SAFFRON_GL2_ERROR_CASE(GL_OUT_OF_MEMORY);
    }
    MR_assert(0 && ""UNKNOWN ERROR!"");
    return ""UNKNOWN ERROR"";
#undef SAFFRON_GL2_ERROR_CASE
}
#endif
").

%-----------------------------------------------------------------------------%

:- pragma foreign_export("C",
    saffron.draw.raise_matrix_stack_error,
    "Saffron_GL2_RaiseMatrixStackError").

:- pragma foreign_export("Java",
    saffron.draw.raise_matrix_stack_error,
    "RaiseMatrixStackError").

%-----------------------------------------------------------------------------%

:- pragma foreign_decl("C",
    "
#define SAFFRON_GL2_MATRIX_N(NAME, PREFIX, N) (void)((NAME[(N)]) = PREFIX ## N)
    ").

%-----------------------------------------------------------------------------%

:- type matrix_pred == (pred(
    float, float, float, float,
    float, float, float, float,
    float, float, float, float,
    float, float, float, float,
    io.io, io.io)).

:- inst matrix_pred == (pred(
    in, in, in, in,
    in, in, in, in,
    in, in, in, in,
    in, in, in, in,
    di, uo) is det).

%-----------------------------------------------------------------------------%

:- instance saffron.context(context(Ctx))
    <= saffron.context(Ctx) where [
    
    (saffron.make_current(context(Ctx, _, _), !IO) :-
        saffron.make_current(Ctx, !IO) ),
    (saffron.load_function(context(Ctx, _, _), Name, Func, !IO) :-
        saffron.load_function(Ctx, Name, Func, !IO))
].

%-----------------------------------------------------------------------------%

:- instance saffron.gl.context(context(Ctx))
    <= saffron.gl.context(Ctx) where [].

%-----------------------------------------------------------------------------%

:- pred set_matrix `with_type` matrix_pred `with_inst` matrix_pred.

:- pragma foreign_proc("C",
    set_matrix(
        V0::in,  V1::in,  V2::in,  V3::in,
        V4::in,  V5::in,  V6::in,  V7::in,
        V8::in,  V9::in,  V10::in, V11::in,
        V12::in, V13::in, V14::in, V15::in,
        IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    may_duplicate, does_not_affect_liveness],
    "
    MR_Float matrix[16];
    matrix[0] = V0;
    matrix[1] = V4;
    matrix[2] = V8;
    matrix[3] = V12;
    matrix[4] = V1;
    matrix[5] = V5;
    matrix[6] = V9;
    matrix[7] = V13;
    matrix[8] = V2;
    matrix[9] = V6;
    matrix[10] = V10;
    matrix[11] = V14;
    matrix[12] = V3;
    matrix[13] = V7;
    matrix[14] = V11;
    matrix[15] = V15;
    SAFFRON_GL_FLOAT_FUNC(glLoadMatrix)(matrix);
    IOo = IOi;
    ").

:- pragma foreign_proc("Java",
    set_matrix(
        V0::in,  V1::in,  V2::in,  V3::in,
        V4::in,  V5::in,  V6::in,  V7::in,
        V8::in,  V9::in,  V10::in, V11::in,
        V12::in, V13::in, V14::in, V15::in,
        IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    may_duplicate],
    "
    double[] matrix = {
        V0, V4, V8,V12,
        V1, V5, V9,V13,
        V2, V6,V10,V14,
        V3, V7,V11,V15
    };
    org.lwjgl.opengl.GL11.glLoadMatrixd(matrix);
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- pred transform `with_type` matrix_pred `with_inst` matrix_pred.

:- pragma foreign_proc("C",
    transform(
        V0::in,  V1::in,  V2::in,  V3::in,
        V4::in,  V5::in,  V6::in,  V7::in,
        V8::in,  V9::in,  V10::in, V11::in,
        V12::in, V13::in, V14::in, V15::in,
        IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    tabled_for_io, may_duplicate, does_not_affect_liveness],
    "
    MR_Float matrix[16];
    matrix[0] = V0;
    matrix[1] = V4;
    matrix[2] = V8;
    matrix[3] = V12;
    matrix[4] = V1;
    matrix[5] = V5;
    matrix[6] = V9;
    matrix[7] = V13;
    matrix[8] = V2;
    matrix[9] = V6;
    matrix[10] = V10;
    matrix[11] = V14;
    matrix[12] = V3;
    matrix[13] = V7;
    matrix[14] = V11;
    matrix[15] = V15;
    SAFFRON_GL_FLOAT_FUNC(glMultMatrix)(matrix);
    IOo = IOi;
    ").

:- pragma foreign_proc("Java",
    transform(
        V0::in,  V1::in,  V2::in,  V3::in,
        V4::in,  V5::in,  V6::in,  V7::in,
        V8::in,  V9::in,  V10::in, V11::in,
        V12::in, V13::in, V14::in, V15::in,
        IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    may_duplicate],
    "
    double[] matrix = {
        V0, V4, V8,V12,
        V1, V5, V9,V13,
        V2, V6,V10,V14,
        V3, V7,V11,V15
    };
    org.lwjgl.opengl.GL11.glMultMatrixd(matrix);
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- pred transform(mmath.matrix.matrix::in, io.io::di, io.io::uo) is det.

transform(Matrix, !IO) :-
    mmath.matrix.destructure_matrix(Matrix,
        V0,  V1,  V2,  V3,
        V4,  V5,  V6,  V7,
        V8,  V9,  V10, V11,
        V12, V13, V14, V15),
    transform(
        V0,  V1,  V2,  V3,
        V4,  V5,  V6,  V7,
        V8,  V9,  V10, V11,
        V12, V13, V14, V15,
        !IO).

%-----------------------------------------------------------------------------%

:- pred identity(io.io::di, io.io::uo) is det.

:- pragma foreign_proc("C", identity(IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    may_duplicate, does_not_affect_liveness],
    "
    glLoadIdentity();
    IOo = IOi;
    ").

:- pragma foreign_proc("Java", identity(IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception],
    "
    org.lwjgl.opengl.GL11.glLoadIdentity();
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- pred push_matrix(io.io::di, io.io::uo) is det.

:- pragma foreign_proc("C", push_matrix(IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    tabled_for_io, may_duplicate, does_not_affect_liveness],
    "
    glPushMatrix();
    IOo = IOi;
    ").

:- pragma foreign_proc("Java", push_matrix(IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception],
    "
    org.lwjgl.opengl.GL11.glPushMatrix();
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- pred pop_matrix(io.io::di, io.io::uo) is det.

:- pragma foreign_proc("C", pop_matrix(IOi::di, IOo::uo),
    [may_call_mercury, promise_pure, thread_safe, tabled_for_io, may_duplicate],
    "
    glPopMatrix();
    if(glGetError() == GL_STACK_UNDERFLOW)
        Saffron_GL2_RaiseMatrixStackError();
    IOo = IOi;
    ").

:- pragma foreign_proc("Java", pop_matrix(IOi::di, IOo::uo),
    [may_call_mercury, promise_pure, thread_safe, may_duplicate],
    "
    org.lwjgl.opengl.GL11.glPopMatrix();
    if(GL11.glGetError() == org.lwjgl.opengl.GL11.GL_STACK_UNDERFLOW)
        RaiseMatrixStackError();
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- instance saffron.draw.basic_transform(context(Ctx))
    <= saffron.gl.context(Ctx) where [
    
    (saffron.draw.set_matrix(_, Matrix, !IO) :-
        mmath.matrix.destructure_matrix(Matrix,
            V0,  V1,  V2,  V3,
            V4,  V5,  V6,  V7,
            V8,  V9,  V10, V11,
            V12, V13, V14, V15),
        set_matrix(
            V0,  V1,  V2,  V3,
            V4,  V5,  V6,  V7,
            V8,  V9,  V10, V11,
            V12, V13, V14, V15,
            !IO) )
].

%-----------------------------------------------------------------------------%

:- instance saffron.draw.transform_stack(context(Ctx))
    <= saffron.gl.context(Ctx) where [

    (saffron.draw.transform(_, Matrix, !IO) :- transform(Matrix, !IO) ),
    (saffron.draw.identity(_, !IO) :- identity(!IO) ),
    (saffron.draw.push_matrix(_, !IO) :- push_matrix(!IO) ),
    (saffron.draw.pop_matrix(_, !IO) :- pop_matrix(!IO) )
].

%-----------------------------------------------------------------------------%

:- instance saffron.draw.transform(context(Ctx))
    <= saffron.gl.context(Ctx) where [].

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(context(Ctx), texture)
    <= saffron.gl.context(Ctx) where [
    
    pred(saffron.destroy/4) is saffron.gl.destroy_texture
].

%-----------------------------------------------------------------------------%

:- instance saffron.texture.texture(context(Ctx), texture)
    <= saffron.gl.context(Ctx) where [
    
    pred(saffron.texture.create_empty/6) is saffron.gl.create_empty_texture,
    pred(saffron.texture.create_from_bitmap/7) is saffron.gl.create_texture_from_bitmap,
    pred(saffron.texture.upload_at/9) is saffron.gl.texture_upload_at
].

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(context(Ctx), buffer2d)
    <= saffron.gl.context(Ctx) where [
    saffron.destroy(_, _, !IO)
].

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(context(Ctx), buffer3d)
    <= saffron.gl.context(Ctx) where [
    saffron.destroy(_, _, !IO)
].

%-----------------------------------------------------------------------------%

:- instance saffron.geometry.buffer(context(Ctx), buffer2d, mmath.vector.vector2)
    <= saffron.gl.context(Ctx) where [
    
    create_buffer_list(_, Vertices, buffer(array.from_list(Vertices)), !IO),
    create_buffer_array(_, Vertices, buffer(Vertices), !IO)
].

%-----------------------------------------------------------------------------%

:- instance saffron.geometry.buffer(context(Ctx), buffer3d, mmath.vector.vector3)
    <= saffron.gl.context(Ctx) where [
    
    create_buffer_list(_, Vertices, buffer(array.from_list(Vertices)), !IO),
    create_buffer_array(_, Vertices, buffer(Vertices), !IO)
].

%-----------------------------------------------------------------------------%

:- instance saffron.draw.color_buffer(context(Ctx), color_buffer)
    <= saffron.gl.context(Ctx) where [
    
    create_color_buffer_list(_, Colors, saffron.soft.color_buffer(array.from_list(Colors)), !IO),
    create_color_buffer_array(_, Colors, saffron.soft.color_buffer(Colors), !IO)
].

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(context(Ctx), color_buffer)
    <= saffron.gl.context(Ctx) where [
    saffron.destroy(_, _, !IO)
].

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(context(Ctx), shape2d)
    <= saffron.gl.context(Ctx) where [
    saffron.destroy(_, _, !IO)
].

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(context(Ctx), shape3d)
    <= saffron.gl.context(Ctx) where [
    saffron.destroy(_, _, !IO)
].

%-----------------------------------------------------------------------------%

:- instance saffron.geometry.shape(context(Ctx), shape2d, buffer2d, texture)
    <= saffron.gl.context(Ctx) where [
    
    (saffron.geometry.create_shape(_, Type, buffer(Buffer), Tex, shape(Shape), !IO) :-
        Shape = saffron.soft.create_shape(Type, Buffer, Tex)),
    (saffron.geometry.create_shape_index_list(_, Type, buffer(Buffer), Indices, Tex, shape(Shape), !IO) :-
        Shape = saffron.soft.create_shape_index_list(
            Type,
            Buffer,
            Indices,
            Tex)),
    (saffron.geometry.create_shape_index_array(_, Type, buffer(Buffer), Indices, Tex, shape(Shape), !IO) :-
        Shape = saffron.soft.create_shape_index_array(Type, Buffer, Indices, Tex))
].

%-----------------------------------------------------------------------------%

:- instance saffron.geometry.shape(context(Ctx), shape3d, buffer3d, texture)
    <= saffron.gl.context(Ctx) where [
    
    (saffron.geometry.create_shape(_, Type, buffer(Buffer), Tex, shape(Shape), !IO) :-
        Shape = saffron.soft.create_shape(Type, Buffer, Tex) ),
    
    (saffron.geometry.create_shape_index_list(_, Type, buffer(Buffer), Indices, Tex, shape(Shape), !IO) :-
        Shape = saffron.soft.create_shape_index_list(Type, Buffer, Indices, Tex) ),
    
    (saffron.geometry.create_shape_index_array(_, Type, buffer(Buffer), Indices, Tex, shape(Shape), !IO) :-
        Shape = saffron.soft.create_shape_index_array(Type, Buffer, Indices, Tex) )
].

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(context(Ctx), group2d)
    <= saffron.gl.context(Ctx) where [
    saffron.destroy(_, _, !IO)
].

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(context(Ctx), group3d)
    <= saffron.gl.context(Ctx) where [
    saffron.destroy(_, _, !IO)
].

%-----------------------------------------------------------------------------%

:- func get_shape2d(shape2d) = shape(mmath.vector.vector2).
get_shape2d(shape(Shape)) = Shape.

%-----------------------------------------------------------------------------%

:- func get_shape3d(shape3d) = shape(mmath.vector.vector3).
get_shape3d(shape(Shape)) = Shape.

%-----------------------------------------------------------------------------%

:- instance saffron.draw.group(context(Ctx), group2d, shape2d)
    <= saffron.gl.context(Ctx) where [
   
    (create_group_list(_, Shapes, group(Group), !IO) :-
        Group = saffron.soft.create_group_list(list.map(get_shape2d, Shapes)) ),
    (create_group_array(_, Shapes, group(Group), !IO) :-
        Group = saffron.soft.create_group_array(array.map(get_shape2d, Shapes)) )
].

%-----------------------------------------------------------------------------%

:- instance saffron.draw.group(context(Ctx), group3d, shape3d)
    <= saffron.gl.context(Ctx) where [
   
    (create_group_list(_, Shapes, group(Group), !IO) :-
        Group = saffron.soft.create_group_list(list.map(get_shape3d, Shapes)) ),
    (create_group_array(_, Shapes, group(Group), !IO) :-
        Group = saffron.soft.create_group_array(array.map(get_shape3d, Shapes)) )
].

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(context(Ctx), shader)
    <= saffron.gl.context(Ctx) where [
    
    ( saffron.destroy(context(_, _, maybe.no), _, !IO) :-
        exception.throw(exception.software_error(
            "Destroying shader without a context")) ),
    ( saffron.destroy(context(_, _, maybe.yes(ShaderCtx)), Shader, !IO) :-
        saffron.gl.shader.destroy_shader_program(ShaderCtx, Shader, !IO) )
].

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(context(Ctx), saffron.gl.shader.shader)
    <= saffron.gl.context(Ctx) where [
    
    ( saffron.destroy(context(_, _, maybe.no), _, !IO) :-
        exception.throw(exception.software_error(
            "Destroying shader without a context")) ),
    ( saffron.destroy(context(_, _, maybe.yes(ShaderCtx)), Shader, !IO) :-
        saffron.gl.shader.destroy_shader(ShaderCtx, Shader, !IO) )
].

%-----------------------------------------------------------------------------%

:- pred generate_attrib_tree(
    saffron.gl.shader.shader_ctx,
    saffron.gl.shader.shader_program,
    int,
    string,
    rbtree.rbtree(string, saffron.gl.shader.vertex_attrib),
    rbtree.rbtree(string, saffron.gl.shader.vertex_attrib),
    io.io, io.io).
:- mode generate_attrib_tree(in, in, in, in, in, out, di, uo) is det.

generate_attrib_tree(Ctx, Shader, _Loc, Name, !Tree, !IO) :-
    saffron.gl.shader.get_attrib_location(Ctx, Shader, Name, MaybeAttrib, !IO),
    (
        MaybeAttrib = maybe.no
    ;
        MaybeAttrib = maybe.yes(Attrib),
        rbtree.set(Name, Attrib, !Tree)
    ).

%-----------------------------------------------------------------------------%

:- pred generate_attrib_tree(
    saffron.gl.shader.shader_ctx,
    saffron.gl.shader.shader_program,
    rbtree.rbtree(string, saffron.gl.shader.vertex_attrib),
    io.io, io.io).
:- mode generate_attrib_tree(in, in, out, di, uo) is det.

generate_attrib_tree(Ctx, Shader, !:Tree, !IO) :-
    !:Tree = rbtree.init,
    generate_attrib_tree(Ctx, Shader, 1, "saffron_position", !Tree, !IO),
    generate_attrib_tree(Ctx, Shader, 2, "saffron_tex_coord", !Tree, !IO),
    generate_attrib_tree(Ctx, Shader, 3, "saffron_color", !Tree, !IO).

%-----------------------------------------------------------------------------%

:- instance saffron.draw.group(context(Ctx), group2d, shape2d, shader)
    <= saffron.gl.context(Ctx) where [
   
    (create_group_list(context(_, _, maybe.no), _, _, _, !IO) :-
        exception.throw(exception.software_error(
            "Cannot create group with a shader without a context")) ),
    (create_group_list(context(_, _, maybe.yes(ShaderCtx)), Shader, Shapes, group(Shader, Attribs, Group), !IO) :-
        generate_attrib_tree(ShaderCtx, Shader, Attribs, !IO),
        Group = saffron.soft.create_group_list(list.map(get_shape2d, Shapes)) ),
    
    (create_group_array(context(_, _, maybe.no), _, _, _, !IO) :-
        exception.throw(exception.software_error(
            "Cannot create group with a shader without a context")) ),
    (create_group_array(context(_, _, maybe.yes(ShaderCtx)), Shader, Shapes, group(Shader, Attribs, Group), !IO) :-
        generate_attrib_tree(ShaderCtx, Shader, Attribs, !IO),
        Group = saffron.soft.create_group_array(array.map(get_shape2d, Shapes)) )
].

%-----------------------------------------------------------------------------%

:- instance saffron.draw.group(context(Ctx), group3d, shape3d, shader)
    <= saffron.gl.context(Ctx) where [
   
    (create_group_list(context(_, _, maybe.no), _, _, _, !IO) :-
        exception.throw(exception.software_error(
            "Cannot create group with a shader without a context")) ),
    (create_group_list(context(_, _, maybe.yes(ShaderCtx)), Shader, Shapes, group(Shader, Attribs, Group), !IO) :-
        generate_attrib_tree(ShaderCtx, Shader, Attribs, !IO),
        Group = saffron.soft.create_group_list(list.map(get_shape3d, Shapes)) ),
    
    (create_group_array(context(_, _, maybe.no), _, _, _, !IO) :-
        exception.throw(exception.software_error(
            "Cannot create group with a shader without a context")) ),
    (create_group_array(context(_, _, maybe.yes(ShaderCtx)), Shader, Shapes, group(Shader, Attribs, Group), !IO) :-
        generate_attrib_tree(ShaderCtx, Shader, Attribs, !IO),
        Group = saffron.soft.create_group_array(array.map(get_shape3d, Shapes)) )
].

%-----------------------------------------------------------------------------%

:- pred vertex(float::in, float::in, float::in, io.io::di, io.io::uo) is det.

:- pragma foreign_proc("C", vertex(X::in, Y::in, Z::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    tabled_for_io, may_duplicate, does_not_affect_liveness],
    "
    SAFFRON_GL_FLOAT_FUNC(glVertex3)(X, Y, Z);
    SAFFRON_GL2_DEBUG_PRINT(""glVertex\\n"");
    IOo = IOi;
    ").

:- pragma foreign_proc("Java", vertex(X::in, Y::in, Z::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception],
    "
    org.lwjgl.opengl.GL11.glVertex3d(X, Y, Z);
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- pred tex_coord(float::in, float::in, io.io::di, io.io::uo) is det.

:- pragma foreign_proc("C", tex_coord(U::in, V::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    may_duplicate, does_not_affect_liveness],
    "
    SAFFRON_GL_FLOAT_FUNC(glTexCoord2)(U, V);
    SAFFRON_GL2_DEBUG_PRINT(""glTexCoord\\n"");
    IOo = IOi;
    ").

:- pragma foreign_proc("Java", tex_coord(U::in, V::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception],
    "
    org.lwjgl.opengl.GL11.glTexCoord2d(U, V);
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- pred tex_coord_i(int::in, int::in, io.io::di, io.io::uo) is det.

:- pragma foreign_proc("C", tex_coord_i(U::in, V::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    may_duplicate, does_not_affect_liveness],
    "
    glTexCoord2i(U, V);
    SAFFRON_GL2_DEBUG_PRINT(""glTexCoord\\n"");
    IOo = IOi;
    ").

:- pragma foreign_proc("Java", tex_coord_i(U::in, V::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception],
    "
    org.lwjgl.opengl.GL11.glTexCoord2i(U, V);
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- pred color(int::in, int::in, int::in, int::in, io.io::di, io.io::uo) is det.

:- pragma foreign_proc("C", color(R::in, G::in, B::in, A::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    may_duplicate, does_not_affect_liveness],
    "
    glColor4ub(R, G, B, A);
    SAFFRON_GL2_DEBUG_PRINT(""glColor\\n"");
    IOo = IOi;
    ").

:- pragma foreign_proc("Java",
    color(R::in, G::in, B::in, A::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception],
    "
    org.lwjgl.opengl.GL11.glColor4ub((byte)R, (byte)G, (byte)B, (byte)A);
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- pred color(color::in, io.io::di, io.io::uo) is det.
color(C, !IO) :-
    color(C ^ red, C ^ green, C ^ blue, C ^ alpha, !IO).

%-----------------------------------------------------------------------------%

:- pred draw_vertex2d(vertex2d::in, io.io::di, io.io::uo) is det.
draw_vertex2d(saffron.geometry.vertex(vector(X, Y), U, V), !IO) :-
    tex_coord(U, V, !IO),
    vertex(X, Y, 0.0, !IO).

%-----------------------------------------------------------------------------%

:- pred draw_vertex2d(vertex2d::in, color::in, io.io::di, io.io::uo) is det.
draw_vertex2d(saffron.geometry.vertex(vector(X, Y), U, V), C, !IO) :-
    color(C, !IO),
    tex_coord(U, V, !IO),
    vertex(X, Y, 0.0, !IO).

%-----------------------------------------------------------------------------%

:- pred draw_vertex3d(vertex3d::in, io.io::di, io.io::uo) is det.
draw_vertex3d(saffron.geometry.vertex(vector(X, Y, Z), U, V), !IO) :-
    tex_coord(U, V, !IO),
    vertex(X, Y, Z, !IO).

%-----------------------------------------------------------------------------%

:- pred draw_vertex3d(vertex3d::in, color::in, io.io::di, io.io::uo) is det.
draw_vertex3d(saffron.geometry.vertex(vector(X, Y, Z), U, V), C, !IO) :-
    color(C, !IO),
    tex_coord(U, V, !IO),
    vertex(X, Y, Z, !IO).

%-----------------------------------------------------------------------------%

:- pred begin(saffron.gl.primitive_type::in, io.io::di, io.io::uo) is det.

:- pragma foreign_proc("C", begin(Type::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    tabled_for_io, may_duplicate, does_not_affect_liveness],
    "
    SAFFRON_GL2_DEBUG_ERROR(""before glBegin"");
    glBegin(Type);
    IOo = IOi;
    ").

:- pragma foreign_proc("Java", begin(Type::in, IOi::di, IOo::uo),
    [promise_pure, thread_safe, will_not_throw_exception],
    "
    org.lwjgl.opengl.GL11.glBegin(jmercury.saffron__gl.PrimitiveTypeToGL(Type));
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- pred end(io.io::di, io.io::uo) is det.

:- pragma foreign_proc("C", end(IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    tabled_for_io, may_duplicate, does_not_affect_liveness],
    "
    glEnd();
    SAFFRON_GL2_DEBUG_ERROR(""after glEnd"");
    IOo = IOi;
    ").

:- pragma foreign_proc("Java", end(IOi::di, IOo::uo),
    [promise_pure, thread_safe, will_not_throw_exception],
    "
    org.lwjgl.opengl.GL11.glEnd();
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- pred draw_shape2d(shape(mmath.vector.vector2)::in, io.io::di, io.io::uo) is det.
draw_shape2d(Shape, !IO) :-
    saffron.gl.bind_texture(Shape ^ saffron.soft.texture, !IO),
    saffron.gl.primitive_type(Shape ^ saffron.soft.primitive_type, Type),
    begin(Type, !IO),
    saffron.soft.shape_foldl(draw_vertex2d, Shape, !IO),
    end(!IO).

%-----------------------------------------------------------------------------%

:- pred draw_shape3d(shape(mmath.vector.vector3)::in, io.io::di, io.io::uo) is det.
draw_shape3d(Shape, !IO) :-
    saffron.gl.bind_texture(Shape ^ saffron.soft.texture, !IO),
    saffron.gl.primitive_type(Shape ^ saffron.soft.primitive_type, Type),
    begin(Type, !IO),
    saffron.soft.shape_foldl(draw_vertex3d, Shape, !IO),
    end(!IO).

%-----------------------------------------------------------------------------%

:- instance saffron.draw.draw(context(Ctx), group2d)
    <= saffron.gl.context(Ctx) where [
    
    (saffron.draw.draw(context(_, _, maybe.no), group(Group), !IO) :-
        array.foldl(draw_shape2d, Group ^ saffron.soft.shapes, !IO) ),
    
    (saffron.draw.draw(context(_, _, maybe.yes(ShaderCtx)), group(Program, Attribs, Group), !IO) :-
        saffron.gl.shader.use_program(ShaderCtx, Program, !IO),
        array.foldl(draw_shape2d, Group ^ saffron.soft.shapes, !IO) ),
    
    (saffron.draw.draw(context(_, _, maybe.yes(ShaderCtx)), group(Group), !IO) :-
        saffron.gl.shader.use_program(ShaderCtx, saffron.gl.shader.no_shader_program, !IO),
        array.foldl(draw_shape2d, Group ^ saffron.soft.shapes, !IO) ),
    
    (saffron.draw.draw(context(_, _, maybe.no), group(_Program, _Attribs, _Group), !IO) :-
        exception.throw(exception.software_error(
            "Shader program exists without a shader context")))
].

%-----------------------------------------------------------------------------%

:- instance saffron.draw.draw(context(Ctx), group3d)
    <= saffron.gl.context(Ctx) where [
    
    ( saffron.draw.draw(context(_, _, maybe.no), group(Group), !IO) :-
        array.foldl(draw_shape3d, Group ^ saffron.soft.shapes, !IO) ),
    
    ( saffron.draw.draw(context(_, _, maybe.yes(ShaderCtx)), group(Program, Attribs, Group), !IO) :-
        saffron.gl.shader.use_program(ShaderCtx, Program, !IO),
        array.foldl(draw_shape3d, Group ^ saffron.soft.shapes, !IO) ),
    
    ( saffron.draw.draw(context(_, _, maybe.yes(ShaderCtx)), group(Group), !IO) :-
        saffron.gl.shader.use_program(ShaderCtx, saffron.gl.shader.no_shader_program, !IO),
        array.foldl(draw_shape3d, Group ^ saffron.soft.shapes, !IO) ),
    
    ( saffron.draw.draw(context(_, _, maybe.no), group(_Program, _Attribs, _Group), !IO) :-
        exception.throw(exception.software_error(
            "Shader program exists without a shader context")) )
].

%-----------------------------------------------------------------------------%

:- pred init(io.io::di, io.io::uo) is det.

:- pragma foreign_proc("C", init(IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    tabled_for_io, may_duplicate, does_not_affect_liveness],
    "
    IOo = IOi;
    SAFFRON_GL2_DEBUG_ERROR(""init"");
    glEnable(GL_BLEND);
    SAFFRON_GL2_DEBUG_ERROR(""blend"");
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    SAFFRON_GL2_DEBUG_ERROR(""blend_func"");
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    SAFFRON_GL2_DEBUG_ERROR(""clear_color"");
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
    SAFFRON_GL2_DEBUG_ERROR(""color"");
    glLineWidth(2.0f); /* This is legacy for the Z2 engine */
    SAFFRON_GL2_DEBUG_ERROR(""line_width"");
    glEnable(GL_TEXTURE_2D);
    SAFFRON_GL2_DEBUG_ERROR(""texture2d"");
    // glDisable(GL_CULL_FACE);
    SAFFRON_GL2_DEBUG_ERROR(""cull_face"");
    
    glEnable(GL_DEPTH_TEST);
    glLoadIdentity();
    glMatrixMode(GL_PROJECTION);
    SAFFRON_GL2_DEBUG_ERROR(""GL_PROJECTION"");
    glLoadIdentity();
    SAFFRON_GL2_DEBUG_ERROR(""identity"");
    glMatrixMode(GL_MODELVIEW);
    SAFFRON_GL2_DEBUG_ERROR(""MODELVIEW"");
    glLoadIdentity();
    SAFFRON_GL2_DEBUG_ERROR(""identity"");
    /*
    fputs(""Graphics vendor: "", stdout);
    puts((void*)glGetString(GL_VENDOR));
    fputs(""Graphics renderer: "", stdout);
    puts((void*)glGetString(GL_RENDERER));
    fputs(""OpenGL version: "", stdout);
    puts((void*)glGetString(GL_VERSION));
    fputs(""OpenGL extensions: "", stdout);
    {
        const char *str = (void*)glGetString(GL_EXTENSIONS);
        const char *next;
        while((next = strchr(str, ' ')) != NULL){
            fwrite(str, 1, next - str, stdout);
            putchar('\\n');
            str = next + 1;
        }
        puts(str);
    }
    */
    ").

:- pragma foreign_proc("Java", init(IOi::di, IOo::uo),
    [promise_pure, thread_safe, will_not_throw_exception, no_sharing],
    "
    IOo = IOi;
    GL11.glEnable(GL11.GL_BLEND);
    GL11.glBlendFunc(GL11.GL_SRC_ALPHA, org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA);
    GL11.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    GL11.glColor4d(1.0, 1.0, 1.0, 1.0);
    GL11.glLineWidth(2.0f); // This is legacy for the Z2 engine (editor for Java)
    GL11.glEnable(GL11.GL_TEXTURE_2D);
    GL11.glEnable(GL11.GL_DEPTH_TEST);
    GL11.glLoadIdentity();
    GL11.glMatrixMode(GL11.GL_PROJECTION);
    GL11.glLoadIdentity();
    GL11.glMatrixMode(GL11.GL_MODELVIEW);
    GL11.glLoadIdentity();
    ").

%-----------------------------------------------------------------------------%

:- func complete_load_ctx(string, maybe.maybe_error(T)) = maybe.maybe(T).
complete_load_ctx(_Name, maybe.ok(Ctx)) = maybe.yes(Ctx).
complete_load_ctx(Name, maybe.error(Err)) = maybe.no :-
    trace [io(!XIO)] (
        io.write_string("Error loading ", !XIO),
        io.write_string(Name, !XIO),
        io.write_string(" functions: ", !XIO),
        io.write_string(Err, !XIO),
        io.nl(!XIO)
    ).

%-----------------------------------------------------------------------------%

init(Ctx,
    context(Ctx, MaybeBufferCtx, MaybeShaderCtx),
    !IO) :-
    
    saffron.make_current(Ctx, !IO),
    saffron.gl.shader_model_version(Ctx, MaybeGLSLVersion, !IO),
    % TODO: Should this live inside create_shader_ctx?
    ( if
        MaybeGLSLVersion = maybe.ok(GLSLVersion),
        saffron.sem_ver_compare(GLSLCmp, GLSLVersion, saffron.sem_ver(1, 20)),
        GLSLCmp \= (<)
    then
        saffron.gl.shader.create_shader_ctx(Ctx, MaybeErrShaderCtx, !IO),
        MaybeShaderCtx = complete_load_ctx("shader", MaybeErrShaderCtx)
    else
        MaybeShaderCtx = maybe.no
    ),
    saffron.gl.buffer.create_buffer_ctx(Ctx, MaybeErrBufferCtx, !IO),
    MaybeBufferCtx = complete_load_ctx("buffer", MaybeErrBufferCtx),
    init(!IO).

