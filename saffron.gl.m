% Copyright (c) 2021-2024 Transnat Games, AlaskanEmily
%
% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.

:- module saffron.gl.
%=============================================================================%
% OpenGL bindings which are not specific to any particular backend.
% These primarily include texture and vertex buffer handling.
:- interface.
%=============================================================================%

:- use_module bitmap.
:- use_module bool.
:- use_module list.
:- use_module maybe.

:- use_module saffron.draw.
:- use_module saffron.geometry.
:- use_module saffron.texture.

:- include_module saffron.gl.buffer.
:- include_module saffron.gl.shader.
:- include_module saffron.gl.vao.

%-----------------------------------------------------------------------------%

:- type texture.

%-----------------------------------------------------------------------------%

:- type  data_type --->
    int ;
    float ;
    byte ;
    short.

%-----------------------------------------------------------------------------%

:- func float_bytes = int.

%-----------------------------------------------------------------------------%

:- type primitive_type --->
    line_strip ;
    line_loop ;
    triangles ;
    triangle_strip ;
    triangle_fan.

%-----------------------------------------------------------------------------%

:- pred primitive_type(saffron.geometry.primitive_type, saffron.gl.primitive_type).
:- mode primitive_type(in, out) is det.
:- mode primitive_type(out, in) is det.
:- mode primitive_type(in, in) is semidet. % Implied

%-----------------------------------------------------------------------------%

:- typeclass context(Ctx)
    <= saffron.context(Ctx) where [].

%-----------------------------------------------------------------------------%

:- pred supports_extension(Ctx, string, bool.bool, io.io, io.io)
    <= saffron.gl.context(Ctx).
:- mode supports_extension(in, in, uo, di, uo) is det.

%-----------------------------------------------------------------------------%

:- pred bind_texture(texture::in, io.io::di, io.io::uo) is det.

%-----------------------------------------------------------------------------%

:- pred destroy_texture(_, texture, io.io, io.io).
:- mode destroy_texture(in, in, di, uo) is det.

%-----------------------------------------------------------------------------%

:- pred create_empty_texture(_, int, int, texture, io.io, io.io).
:- mode create_empty_texture(in, in, in, out, di, uo) is det.

%-----------------------------------------------------------------------------%

:- pred create_texture_from_bitmap(_, bitmap.bitmap, int, int, texture, io.io, io.io).
:- mode create_texture_from_bitmap(in, in, in, in, out, di, uo) is det.

%-----------------------------------------------------------------------------%

:- pred texture_upload_at(_, texture, bitmap.bitmap, int, int, int, int, io.io, io.io).
:- mode texture_upload_at(in, in, in, in, in, in, in, di, uo) is det.

%-----------------------------------------------------------------------------%

:- pred opengl_version_string(Ctx, string, io.io, io.io)
    <= saffron.gl.context(Ctx).
:- mode opengl_version_string(in, out, di, uo) is det.

%-----------------------------------------------------------------------------%

:- pred opengl_version(Ctx, maybe.maybe_error(sem_ver), io.io, io.io)
    <= saffron.gl.context(Ctx).
:- mode opengl_version(in, out, di, uo) is det.

%-----------------------------------------------------------------------------%

:- pred shader_model_version_string(Ctx, string, io.io, io.io)
    <= saffron.gl.context(Ctx).
:- mode shader_model_version_string(in, out, di, uo) is det.

%-----------------------------------------------------------------------------%

:- pred shader_model_version(Ctx, maybe.maybe_error(sem_ver), io.io, io.io)
    <= saffron.gl.context(Ctx).
:- mode shader_model_version(in, out, di, uo) is det.

%=============================================================================%
:- implementation.
%=============================================================================%

% Include the OpenGL headers.
:- pragma foreign_decl("C", "

/* TODO: Can we just use MR_Word/MR_Integer here? */
#ifdef _WIN64
typedef signed long long SaffronGLsizeiptr;
#else
typedef signed long SaffronGLsizeiptr;
#endif

typedef char SaffronGLchar;

#if (defined WIN32) || (defined _WIN32) || (defined __CYGWIN__)

# include <Windows.h>
# include <GL/gl.h>

#elif (defined __HAIKU__) && (defined __amd64)

# define SAFFRON_GL_EXPECT_GL4
# define SAFFRON_GL_EXPECT_BUFFER
# define SAFFRON_GL_EXPECT_SHADER
# define GL_GLEXT_PROTOTYPES
# include <GL/gl.h>
# include <GL/glext.h>

#elif (defined __APPLE__)

/* We support as far back as OS X 10.3.4 (in theory), and that had at least
 * buffer support, if not shaders.
 *
 * See: http://support.apple.com/kb/TA22513
 */
# define SAFFRON_GL_EXPECT_BUFFER
# define GL_GLEXT_PROTOTYPES
# include <OpenGL/gl.h>
# include <OpenGL/glext.h>

# ifndef APIENTRY
#   define APIENTRY
# endif

#else

# if (defined __linux__) || (defined __OpenBSD__) || (defined __FreeBSD__)

/* Currently, we only expect OpenGL 4 on OpenBSD 6.2+, FreeBSD 9+, and Haiku,
 * as these systems all ship with Mesa.
 *
 * Technically FreeBSD can use NVidia proprietary drivers, but if the system
 * version is 9+ then we expect NVidia drivers that can also support this.
 */
#   if (defined __OpenBSD__)
#     include <sys/param.h>
#     if (OpenBSD >= 201710)
#       define SAFFRON_GL_EXPECT_GL4
#     endif
#   elif (defined __FreeBSD__) && (__FreeBSD__ >= 9)
#       define SAFFRON_GL_EXPECT_GL4
#   endif
#   define SAFFRON_GL_EXPECT_GL3
#   define SAFFRON_GL_EXPECT_BUFFER
#   define SAFFRON_GL_EXPECT_SHADER
#   define GL_GLEXT_PROTOTYPES
# endif

# include <GL/gl.h>
# include <GL/glext.h>

# ifndef APIENTRY
#   define APIENTRY
# endif

#endif

#ifndef GL_SHADING_LANGUAGE_VERSION
# define GL_SHADING_LANGUAGE_VERSION 0x8B8C
#endif

#ifdef MR_USE_SINGLE_PREC_FLOAT

# define SAFFRON_GL_FLOAT_FUNC(NAME) NAME ## f
# define SAFFRON_GL_FLOAT GL_FLOAT

# if (MR_BYTES_PER_WORD == 4) && !(defined MR_BOXED_FLOATS)
/* Indicates that we can just use MR_Word in place of floats, mostly useful
 * for arrays. */
#   define SAFFRON_GL_RAW_FLOATS
# endif

#else

# define SAFFRON_GL_FLOAT_FUNC(NAME) NAME ## d
# define SAFFRON_GL_FLOAT GL_DOUBLE

#endif

#if (MR_BYTES_PER_WORD == 4)
/* Indicates we can just use MR_Word/MR_Integer in place of GL_INT values */
# define SAFFRON_GL_RAW_INTS
#endif

").

% Imports for the Java grade.
:- pragma foreign_decl("Java", "
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11; // Base constants
import org.lwjgl.opengl.GL20;
import java.nio.ByteBuffer;
import jmercury.maybe.Maybe_1; // Used in MaybeInteger.
    ").

:- pragma foreign_code("Java", "
///////////////////////////////////////////////////////////////////////////////
/// \brief Shorthand to create a maybe(int).
public static Maybe_1<Integer> MaybeInteger(int i, boolean b){
    return b ? new Maybe_1.Yes_1(new Integer(i)) : new Maybe_1.No_0();
}

public static jmercury.maybe.Maybe_1<Integer> MaybeInteger(int i){
    return MaybeInteger(i, i >= 0);
}
public static ByteBuffer BufferFromBitmap(
    int w,
    int h,
    jmercury.runtime.MercuryBitmap bmp){
    if(w * h * 4 < bmp.elements.length)
        throw new IndexOutOfBoundsException(""Bitmap is too small"");
    final ByteBuffer buffer = ByteBuffer.allocateDirect(w * h * 4);
    buffer.put(bmp.elements, 0, w * h * 4);
    buffer.rewind();
    buffer.mark();
    return buffer;
}
    ").

%-----------------------------------------------------------------------------%

:- pragma foreign_enum("C", data_type/0, [
    int             - "GL_INT",
    float           - "SAFFRON_GL_FLOAT",
    byte            - "GL_UNSIGNED_BYTE",
    short           - "GL_UNSIGNED_SHORT"
]).

:- pragma foreign_export_enum(
    "Java",
    data_type/0,
    [prefix("DATA_TYPE_")|[uppercase|[]]]).

:- pragma foreign_code("Java", "
public static int DataTypeToGL(Data_type_0 t){
    if(t.equals(DATA_TYPE_INT))
        return org.lwjgl.opengl.GL11.GL_INT;
    else if(t.equals(DATA_TYPE_FLOAT))
        return org.lwjgl.opengl.GL11.GL_DOUBLE;
    else if(t.equals(DATA_TYPE_BYTE))
        return org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE;
    else if(t.equals(DATA_TYPE_SHORT))
        return org.lwjgl.opengl.GL11.GL_UNSIGNED_SHORT;
    else
        throw new IllegalStateException(""Unknown data type"");
}
    ").

%-----------------------------------------------------------------------------%

:- pragma foreign_enum("C", primitive_type/0, [
    line_strip      - "GL_LINE_STRIP",
    line_loop       - "GL_LINE_LOOP",
    triangles       - "GL_TRIANGLES",
    triangle_strip  - "GL_TRIANGLE_STRIP",
    triangle_fan    - "GL_TRIANGLE_FAN"
]).

:- pragma foreign_export_enum(
    "Java",
    primitive_type/0,
    [prefix("PRIMITIVE_TYPE_")|[uppercase|[]]]).

:- pragma foreign_code("Java", "
public static int PrimitiveTypeToGL(Primitive_type_0 t){
    if(t.equals(PRIMITIVE_TYPE_LINE_STRIP))
        return org.lwjgl.opengl.GL11.GL_LINE_STRIP;
    if(t.equals(PRIMITIVE_TYPE_LINE_LOOP))
        return org.lwjgl.opengl.GL11.GL_LINE_LOOP;
    if(t.equals(PRIMITIVE_TYPE_TRIANGLES))
        return org.lwjgl.opengl.GL11.GL_TRIANGLES;
    if(t.equals(PRIMITIVE_TYPE_TRIANGLE_STRIP))
        return org.lwjgl.opengl.GL11.GL_TRIANGLE_STRIP;
    if(t.equals(PRIMITIVE_TYPE_TRIANGLE_FAN))
        return org.lwjgl.opengl.GL11.GL_TRIANGLE_FAN;
    else
        throw new IllegalStateException(""Unknown primitive type"");
}
    ").

%-----------------------------------------------------------------------------%

:- pragma foreign_type("C", texture, "GLuint").
:- pragma foreign_type("Java", texture, "int").

%-----------------------------------------------------------------------------%

primitive_type(saffron.geometry.line_strip, saffron.gl.line_strip).
primitive_type(saffron.geometry.line_loop, saffron.gl.line_loop).
primitive_type(saffron.geometry.triangles, saffron.gl.triangles).
primitive_type(saffron.geometry.triangle_strip, saffron.gl.triangle_strip).
primitive_type(saffron.geometry.triangle_fan, saffron.gl.triangle_fan).

%-----------------------------------------------------------------------------%

:- pred supports_extension_inner(string, bool.bool, io.io, io.io).
:- mode supports_extension_inner(in, uo, di, uo) is det.

supports_extension_inner(_, bool.no, !IO).

:- pragma foreign_proc("C",
    supports_extension_inner(Ext::in, Supports::uo, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    may_duplicate, does_not_affect_liveness],
    "
    const MR_Integer len = strnlen(Ext, 512);
    const char *str = (void*)glGetString(GL_EXTENSIONS);
    Supports = MR_NO;
    IOo = IOi;
    if(str != NULL){
        do{
            if(strncmp(str, Ext, len) != 0 &&
                (str[len] == ' ' || str[len] == 0)){
                
                Supports = MR_YES;
                break;
            }
        }while((str = strchr(str, ' ')) != NULL && *(++str) != 0);
    }
    ").

%-----------------------------------------------------------------------------%

supports_extension(Ctx, Ext, Supports, !IO) :-
    saffron.make_current(Ctx, !IO),
    supports_extension_inner(Ext, Supports, !IO).

%-----------------------------------------------------------------------------%

float_bytes = 8.

:- pragma foreign_proc("C", float_bytes = (N::out),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    may_duplicate, does_not_affect_liveness],
    "
    N = sizeof(MR_Float);
    ").

%-----------------------------------------------------------------------------%

:- pred create_texture(texture::out, io.io::di, io.io::uo) is det.

:- pragma foreign_proc("C", create_texture(Tex::out, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    tabled_for_io, may_duplicate, does_not_affect_liveness],
    "
    IOo = IOi;
    glGenTextures(1, &Tex);
    ").

:- pragma foreign_proc("Java", create_texture(Tex::out, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception],
    "
    IOo = IOi;
    Tex = org.lwjgl.opengl.GL11.glGenTextures();
    ").

%-----------------------------------------------------------------------------%

:- pred tex_parameters(io.io::di, io.io::uo) is det.

:- pragma foreign_proc("C", tex_parameters(IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    may_duplicate, does_not_affect_liveness],
    "
    IOo = IOi;
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    ").

:- pragma foreign_proc("Java", tex_parameters(IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception],
    "
    IOo = IOi;
    org.lwjgl.opengl.GL11.glTexParameteri(org.lwjgl.opengl.GL11.GL_TEXTURE_2D, org.lwjgl.opengl.GL11.GL_TEXTURE_MIN_FILTER, org.lwjgl.opengl.GL11.GL_NEAREST);
    org.lwjgl.opengl.GL11.glTexParameteri(org.lwjgl.opengl.GL11.GL_TEXTURE_2D, org.lwjgl.opengl.GL11.GL_TEXTURE_MAG_FILTER, org.lwjgl.opengl.GL11.GL_NEAREST);
    ").

%-----------------------------------------------------------------------------%

:- pragma foreign_proc("C", bind_texture(Tex::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    tabled_for_io, may_duplicate, does_not_affect_liveness],
    "
    IOo = IOi;
    glBindTexture(GL_TEXTURE_2D, Tex);
    ").

:- pragma foreign_proc("Java", bind_texture(Tex::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception],
    "
    IOo = IOi;
    org.lwjgl.opengl.GL11.glBindTexture(org.lwjgl.opengl.GL11.GL_TEXTURE_2D, Tex);
    ").

%-----------------------------------------------------------------------------%

:- pred tex_image(int::in, int::in, io.io::di, io.io::uo) is det.

:- pragma foreign_proc("C", tex_image(W::in, H::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    may_duplicate, does_not_affect_liveness],
    "
    IOo = IOi;
    glTexImage2D(
        GL_TEXTURE_2D, 0, GL_RGBA, W, H, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    ").

:- pragma foreign_proc("Java", tex_image(W::in, H::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception],
    "
    IOo = IOi;
    org.lwjgl.opengl.GL11.nglTexImage2D(
        org.lwjgl.opengl.GL11.GL_TEXTURE_2D,
        0,
        org.lwjgl.opengl.GL11.GL_RGBA,
        W,
        H,
        0,
        org.lwjgl.opengl.GL11.GL_RGBA,
        org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE,
        0);
    ").

%-----------------------------------------------------------------------------%

:- pred tex_image(bitmap.bitmap, int, int, io.io, io.io).
:- mode tex_image(in, in, in, di, uo) is det.

:- pragma foreign_proc("C", tex_image(B::in, W::in, H::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    may_duplicate, does_not_affect_liveness],
    "
    IOo = IOi;
    MR_assert(MR_bitmap_length_in_bytes(B->num_bits) == (W * H) << 2);
    glTexImage2D(
        GL_TEXTURE_2D, 0, GL_RGBA, W, H, 0, GL_RGBA, GL_UNSIGNED_BYTE, B->elements);
    ").

:- pragma foreign_proc("Java", tex_image(B::in, W::in, H::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe],
    "
    final java.nio.ByteBuffer buffer = jmercury.saffron__gl.BufferFromBitmap(W, H, B);
    org.lwjgl.opengl.GL11.glTexImage2D(
        org.lwjgl.opengl.GL11.GL_TEXTURE_2D,
        0,
        org.lwjgl.opengl.GL11.GL_RGBA,
        W,
        H,
        0,
        org.lwjgl.opengl.GL11.GL_RGBA,
        org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE,
        buffer);
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- pred tex_sub_image(bitmap.bitmap, int, int, int, int, io.io, io.io).
:- mode tex_sub_image(in, in, in, in, in, di, uo) is det.

:- pragma foreign_proc("C",
    tex_sub_image(B::in, X::in, Y::in, W::in, H::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    may_duplicate, does_not_affect_liveness],
    "
    IOo = IOi;
    MR_assert(MR_bitmap_length_in_bytes(B->num_bits) == (W * H) << 2);
    glTexSubImage2D(
        GL_TEXTURE_2D, 0, X, Y, W, H, GL_RGBA, GL_UNSIGNED_BYTE, B->elements);
    ").

:- pragma foreign_proc("Java",
    tex_sub_image(B::in, X::in, Y::in, W::in, H::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe],
    "
    final java.nio.ByteBuffer buffer = jmercury.saffron__gl.BufferFromBitmap(W, H, B);
    org.lwjgl.opengl.GL11.glTexImage2D(
        org.lwjgl.opengl.GL11.GL_TEXTURE_2D,
        0,
        X,
        Y,
        W,
        H,
        org.lwjgl.opengl.GL11.GL_RGBA,
        org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE,
        buffer);
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- pred destroy_texture(texture::in, io.io::di, io.io::uo) is det.

:- pragma foreign_proc("C", destroy_texture(Tex::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    tabled_for_io, may_duplicate, does_not_affect_liveness],
    "
    IOo = IOi;
    glDeleteTextures(1, &Tex);
    ").

:- pragma foreign_proc("Java", destroy_texture(Tex::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception],
    "
    IOo = IOi;
    org.lwjgl.opengl.GL11.glDeleteTextures(Tex);
    ").

%-----------------------------------------------------------------------------%

destroy_texture(_Ctx, Tex, !IO) :-
    destroy_texture(Tex, !IO).

%-----------------------------------------------------------------------------%

create_empty_texture(_Ctx, W, H, Tex, !IO) :-
    create_texture(Tex, !IO),
    bind_texture(Tex, !IO),
    tex_image(W, H, !IO),
    tex_parameters(!IO).

%-----------------------------------------------------------------------------%

create_texture_from_bitmap(_Ctx, B, W, H, Tex, !IO) :-
    create_texture(Tex, !IO),
    bind_texture(Tex, !IO),
    tex_image(B, W, H, !IO),
    tex_parameters(!IO).

%-----------------------------------------------------------------------------%

texture_upload_at(_Ctx, Tex, B, X, Y, W, H, !IO) :-
    bind_texture(Tex, !IO),
    tex_sub_image(B, X, Y, W, H, !IO).

%-----------------------------------------------------------------------------%
% This is some ugly stuff.
%
% We want to be able to use saffron.load_function/5 from C, but it is a
% typeclass predicate, and saffron.gl.context is a higher order type.
%
% In order to be able to use a typeclass predicate on a GL context, we need an
% existentially type functor which we can pass to a Mercury predicate that will
% unpack it and invoke the predicate.
:- type gl_load_func_ctx --->
    some [Ctx] (gl_load_func_ctx(Ctx)
        => (saffron.context(Ctx),
            saffron.gl.context(Ctx))).

%-----------------------------------------------------------------------------%
% A non-higher-typed version of load_function that takes gl_load_func_ctx.
% This is only called from C code.
:- pred gl_load_function(
    gl_load_func_ctx,
    string,
    maybe.maybe_error(saffron.ctx_function),
    io.io, io.io).
:- mode gl_load_function(
    in,
    in,
    out,
    di, uo) is det.

gl_load_function(gl_load_func_ctx(Ctx), Name, MaybeFunc, !IO) :-
    saffron.load_function(Ctx, Name, MaybeFunc, !IO).

:- pragma foreign_export("C",
    gl_load_function(in, in, out, di, uo),
    "SaffronGL_LoadFunction").

:- pred gl_version(
    gl_load_func_ctx,
    int,
    int,
    io.io, io.io).
:- mode gl_version(
    in,
    out,
    out,
    di, uo) is det.

gl_version(gl_load_func_ctx(Ctx), Major, Minor, !IO) :-
    opengl_version(Ctx, MaybeSemVer, !IO),
    (
        MaybeSemVer = maybe.ok(SemVer),
        Major = major(SemVer),
        Minor = minor(SemVer)
    ;
        MaybeSemVer = maybe.error(Error),
        trace [io(!XIO)] (
            io.write_string("Error getting OpenGL version: ", !XIO),
            io.write_string(Error, !XIO),
            io.nl(!XIO)
        ),
        Major = 0,
        Minor = 0
    ).

:- pragma foreign_export("C",
    gl_version(in, out, out, di, uo),
    "SaffronGL_Version").

:- pred gl_supports_extension(
    gl_load_func_ctx,
    string,
    bool.bool,
    io.io, io.io).
:- mode gl_supports_extension(
    in,
    in,
    uo,
    di, uo) is det.

gl_supports_extension(gl_load_func_ctx(Ctx), Ext, Supports, !IO) :-
    supports_extension(Ctx, Ext, Supports, !IO).

:- pragma foreign_export("C",
    gl_supports_extension(in, in, uo, di, uo),
    "SaffronGL_SupportsExtension").

%-----------------------------------------------------------------------------%

:- type version_type ---> gl ; glsl.

:- pragma foreign_enum("C", version_type/0, [
    gl      - "GL_VERSION",
    glsl    - "GL_SHADING_LANGUAGE_VERSION"
]).

%-----------------------------------------------------------------------------%

:- pred version_string(version_type, string, io.io, io.io).
:- mode version_string(in, out, di, uo) is det.

version_string(_, "0.0", !IO).

:- pragma foreign_proc("C",
    version_string(Type::in, Str::out, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    does_not_affect_liveness, may_duplicate],
    "
    const char *ver = (char*)glGetString(Type);
    if(glGetError() != GL_NO_ERROR || ver == NULL || *ver == 0){
        Str = (MR_String)""0.0"";
    }
    else{
        while(*ver == ' ')
            ver++;
        MR_Integer len = strspn(ver, ""0123456789."");
        Str = MR_GC_malloc_atomic(len + 1);
        memcpy(Str, ver, len);
        Str[len] = '\\0';
    }
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- pred opengl_version_string(string::out, io.io::di, io.io::uo) is det.
opengl_version_string(Str, !IO) :-
    version_string(gl, Str, !IO).

:- pragma foreign_proc("Java",
    opengl_version_string(Str::out, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception],
    "
    IOo = IOi;
    Str = org.lwjgl.opengl.GL11.glGetString(org.lwjgl.opengl.GL11.GL_VERSION);
    ").

opengl_version_string(Ctx, Str, !IO) :-
    saffron.make_current(Ctx, !IO),
    opengl_version_string(Str, !IO).

%-----------------------------------------------------------------------------%

opengl_version(Ctx, saffron.parse_sem_ver(Version), !IO) :-
    opengl_version_string(Ctx, Version, !IO).

%-----------------------------------------------------------------------------%

:- pred shader_model_version_string(string::out, io.io::di, io.io::uo) is det.
shader_model_version_string(Str, !IO) :-
    version_string(glsl, Str, !IO).

:- pragma foreign_proc("Java",
    shader_model_version_string(Str::uo, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception],
    "
    IOo = IOi;
    Str = org.lwjgl.opengl.GL20.glGetString(
        org.lwjgl.opengl.GL20.GL_SHADING_LANGUAGE_VERSION);
    ").

shader_model_version_string(Ctx, Str, !IO) :-
    saffron.make_current(Ctx, !IO),
    shader_model_version_string(Str, !IO).

%-----------------------------------------------------------------------------%

shader_model_version(Ctx, saffron.parse_sem_ver(Version), !IO) :-
    shader_model_version_string(Ctx, Version, !IO).

