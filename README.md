Saffron
=======

Cross-platform Mercury graphics framework
-----------------------------------------

Saffron is multi-platform graphics framework for Mercury. It is not intended to
be a complete game engine on its own, but provides a set of graphics API and
windowing API agnostic features that let you write graphical programs in
Mercury.

The base library comes with bindings for OpenGL, and can create windows and
read input using Win32/WGL and X11/GLX. It is possible to extend the library,
without modification of the Saffron source, to add further windowing and input
backends (for OpenGL or otherwise), or to add other graphics APIs (DirectX,
Vulkan, Metal, etc).

Saffron is tested to build and run on:
 * Linux (amd64, x86)
 * OpenBSD (amd64, Sparc64)
 * Windows (Cygwin x86_64)

It should be possible to run Saffron on OS X and Haiku using the SDL2 backend
for the Glow library, although this has not been tested.

Currently Solaris will not work as on Unix we always build both static and
shared Mercury libraries, and this is not supported by Mercury on Solaris.

Support for Windows using MSVC is planned. The base library backends should all
compile on every supported platform, although for some (OpenGL 4.0, etc) it is
possible that they are not supported at runtime.

Notes on OpenGL with Mercury Grades
-----------------------------------

###Low-level Parallel Grades###

For obscure reasons, Mercury's low-level parallelism grades (any grade with
`.par` that is not `hlc` or `hl`) will not function with OpenGL. This problem
occurs regardless of your OpenGL driver and client library, and regardless of
your platform. If your compiler builds using one of these grades as a default
(you can test with `mmc --output-grade-string` to see the default grade), you
must set the grade to build with using the `GRADE` make variable, as so:

`make GRADE=asm_fast.gc.stseg`, or `make GRADE=hlc.gc.spf.par`, etc

###Single-precision Float Grades###

Graphics performance will usually be slightly faster if you use a `.spf` grade
for your programs. This allows Saffron to send less data (half as many bytes
per float value), and on 32-bit platforms (especially Windows) we can use
the unboxed Mercury float arrays for OpenGL buffers.

Saffron should still work with or without `.spf` grades, and care has been
taken to use the OpenGL functions that correspond to the native Mercury float
size, in an effort to reduce single/double conversions regardless of the grade.

Building
--------

###Preparing for a build###

Regardless of the platform, you will need:
 * OpenGL development libraries and headers, at least gl.h (glext.h required on some platforms)
 * Mercury, at least verison 20.1 (most ROTDs from 2019 onwards should work)

To prepare to build tests and demos (not necessary for the base library, but a
good idea), run:
`git submodule init && git submodule update`

Saffron also requires `make` to build, the exact type of make depends on the
platform. Usually GNU or BSD make will work, however the situation is slightly
more complicated on Windows (including Cygwin).

Note that you can only build using the Cygwin instructions if Mercury has been
configured to use a Cygwin C compiler, and you can only build using the
Windows instructions if Mercury has been configured to use the MSVC compiler.

###Unix (Linux, BSD, etc)###

On Unix, the requirements for building are:
 * GNU or BSD make
 * Mercury, at least verison 20.1 (most ROTDs from 2019 onwards should work)
 * OpenGL development libraries and at least gl.h and glext.h headers
 * X11 and GLX development libraries and headers

To build the base library, just use `make`. You can control parallelism, as so:
`make PARALLEL=8 -j2`, where the `PARALLEL` flag indicates the Mercury parallel
jobs and the `-j` flag indicates the Make parallel jobs. Since most of the
Mercury libraries can't be parallelized due to dependencies, it's recommended
to set PARALLEL equal to your number of cores and do `-j` of 2 or 4.

To build and run the base demo, do: `make demos && sh demo/saffron_cube_demo.sh`

###Cygwin###

For Cygwin, the requirements for building are:
 * GNU make
 * Mercury, at least verison 20.1 (most ROTDs from 2019 onwards should work), configured to use the Cygwin C compiler
 * OpenGL development libraries, and at least the gl.h header
 * Win32 API development libraries and headers

To build the base library, use `make -f makefile.cyg`. On Windows, the Mercury
parallelism flag (PARALLEL=<n>) will not do anything, although it should still
be safe to use the make -j<n> flag.

To build and run the base demo, do: `make -f makefile.cyg demos && sh demo/saffron_cube_demo.sh`

###Windows###

NOTE THAT THESE INSTRUCTIONS ARE STILL CONSIDERED EXPERIMENTAL!

For Windows (using MSVC), the requirements for building are:
 * Microsoft NMake (Sometimes included in the Visual Studio installation, available in the Windows SDK)
 * MSVC C++ 2015 or later (same requirement as needed to build the Mercury compiler)
 * Mercury, at least verison 20.1 (most ROTDs from 2019 onwards should work), configured to use the MSVC compiler and *not* compiled with --with-msvcrt

To build the base library, use `nmake /fnmakefile` from a Visual Studio build
command prompt.

If you really want build parallelism, Qt's JOM make program should also work,
however a relatively new version is required (older versions had broken
`MAKEDIR` variables).

To build and run the base demo, do: `nmake /fnmakefile demos && demo\saffron_cube_demo.exe`

