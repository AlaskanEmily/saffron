% Copyright (c) 2021-2023 Transnat Games, AlaskanEmily
%
% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.

:- module saffron.
%=============================================================================%
% Saffron is a graphics framework for Mercury programs.
:- interface.
%=============================================================================%

:- use_module io.
:- use_module maybe.

:- include_module saffron.draw.
:- include_module saffron.geometry.
:- include_module saffron.texture.
% TODO: This should probably be optional, but since we support OpenGL on all
% platforms it's kind of fine.
:- include_module saffron.gl.
:- include_module saffron.gl2.
%:- include_module saffron.gl4.
:- include_module saffron.soft.

%-----------------------------------------------------------------------------%
% Because Saffron data often includes references to GPU resources, it is vital
% that they are properly destroyed.
:- typeclass destroy(Ctx, Resource) where [
    pred destroy(Ctx::in, Resource::in, io.io::di, io.io::uo) is det
].

%-----------------------------------------------------------------------------%
% Opaque "function pointer" type. This is different depending on the grade.
:- type ctx_function.

%-----------------------------------------------------------------------------%

:- typeclass context(Ctx) where [
    % Makes this the current context.
    pred make_current(Ctx::in, io.io::di, io.io::uo) is det,
    % Loads a function for the context, given a name.
    % Note that it is *not* safe to assume that this function is usable, even
    % on success. You should always check the for the correct extension in
    % OpenGL when using this!
    pred load_function(Ctx, string, maybe.maybe_error(ctx_function), io.io, io.io),
    mode load_function(in, in, out, di, uo) is det
].

%-----------------------------------------------------------------------------%
% Default implementation of load_function, always fails.
:- pred default_load_function(_, string, maybe.maybe_error(ctx_function), io.io, io.io).
:- mode default_load_function(in, in, out, di, uo) is det.

%-----------------------------------------------------------------------------%
% Color could/should be a full type, but this is efficient.
% We should ideally support some 16-bit formats too, as they are still widely
% in use.
:- type color == int.

%-----------------------------------------------------------------------------%

:- func red_mask = int.
:- func green_mask = int.
:- func blue_mask = int.
:- func alpha_mask = int.

%-----------------------------------------------------------------------------%

:- func red_shift = int.
:- func green_shift = int.
:- func blue_shift = int.
:- func alpha_shift = int.

%-----------------------------------------------------------------------------%

:- func red_bits = int.
:- func green_bits = int.
:- func blue_bits = int.
:- func alpha_bits = int.

%-----------------------------------------------------------------------------%

:- func red(color) = int.
:- func green(color) = int.
:- func blue(color) = int.
:- func alpha(color) = int.

%-----------------------------------------------------------------------------%

:- func 'red :='(int, color) = color.
:- func 'green :='(int, color) = color.
:- func 'blue :='(int, color) = color.
:- func 'alpha :='(int, color) = color.

%-----------------------------------------------------------------------------%

:- func transparent = color.
:- func transparent_white = color.
:- func white = color.
:- func black = color.
:- func gray = color.
:- func red = color.
:- func blue = color.
:- func yellow = color.
:- func green = color.
:- func purple = color.
:- func orange = color.
:- func cyan = color.
:- func magenta = color.

%-----------------------------------------------------------------------------%
% Used by some backends to detect API versions.
:- type sem_ver --->
    sem_ver(int, int, int) ;
    sem_ver(int, int) ;
    sem_ver(int).

%-----------------------------------------------------------------------------%

:- func major(sem_ver) = int.
:- func minor(sem_ver) = int.
:- func patch(sem_ver) = int.

%-----------------------------------------------------------------------------%

:- pred sem_ver_compare(builtin.comparison_result, sem_ver, sem_ver).
:- mode sem_ver_compare(uo, in, in) is det.

%-----------------------------------------------------------------------------%

:- func parse_sem_ver(string) = maybe.maybe_error(sem_ver).

%=============================================================================%
:- implementation.
%=============================================================================%

:- import_module int.
:- import_module list.
:- use_module string.

%-----------------------------------------------------------------------------%
% Utilities used by Java code.
:- pragma foreign_decl("Java", "
import jmercury.list.List_1;
import jmercury.saffron__geometry.Vertex_1;
    ").

:- pragma foreign_code("Java", "
public static int ListLength(List_1<?> l){
    int i = 0;
    while(!jmercury.list.is_empty(l)){
        i++;
        l = jmercury.list.det_tail(l);
    }
    return i;
}
public static int WriteVector(double[] to, jmercury.mmath__vector__vector2.Vector_0 vector, int i){
    to[i++] = vector.x;
    to[i++] = vector.y;
    return i;
}
public static int WriteVector(double[] to, jmercury.mmath__vector__vector3.Vector_0 vector, int i){
    to[i++] = vector.x;
    to[i++] = vector.y;
    to[i++] = vector.z;
    return i;
}
public static int WriteVertexUV(double[] to, Vertex_1<?> vertex, int i){
    to[i++] = vertex.u;
    to[i++] = vertex.v;
    return i;
}
public static int WriteVertex2(double[] to, Vertex_1<jmercury.mmath__vector__vector2.Vector_0> vertex, int i){
    i = WriteVector(to, vertex.vec, i);
    i = WriteVertexUV(to, vertex, i);
    return i;
}
public static int WriteVertex3(double[] to, Vertex_1<jmercury.mmath__vector__vector3.Vector_0> vertex, int i){
    i = WriteVector(to, vertex.vec, i);
    i = WriteVertexUV(to, vertex, i);
    return i;
}
    ").

%-----------------------------------------------------------------------------%

default_load_function(_, _, maybe.error("load_function not supported"), !IO).

%-----------------------------------------------------------------------------%
% Nothing by default.
:- type ctx_function ---> ctx_function.

%-----------------------------------------------------------------------------%
% In C, Mercury guarantees that MR_Word can hold a function pointer.
:- pragma foreign_type("C", ctx_function, "MR_Word").

%-----------------------------------------------------------------------------%
% Used by C backends to create maybe.ok(ctx_function)
:- func create_ctx_function_ok(ctx_function) = maybe.maybe_error(ctx_function).
create_ctx_function_ok(Func) = maybe.ok(Func).
:- pragma foreign_export("C",
    create_ctx_function_ok(in) = (out),
    "Saffron_CreateCtxFunctionOK").

%-----------------------------------------------------------------------------%
% Used by C backends to create maybe.error(ctx_function)
:- func create_ctx_function_error(string) = maybe.maybe_error(ctx_function).
create_ctx_function_error(E) = maybe.error(E).
:- pragma foreign_export("C",
    create_ctx_function_error(in) = (out),
    "Saffron_CreateCtxFunctionError").

%-----------------------------------------------------------------------------%
% Used by C backends to get results from maybe.ok(ctx_function)
:- pred get_ctx_function_ok(maybe.maybe_error(ctx_function), ctx_function).
:- mode get_ctx_function_ok(in, out) is semidet.
get_ctx_function_ok(maybe.ok(Func), Func).
:- pragma foreign_export("C",
    get_ctx_function_ok(in, out),
    "Saffron_GetCtxFunctionOK").

%-----------------------------------------------------------------------------%
% Used by C backends to get results from maybe.error(ctx_function)
:- pred get_ctx_function_error(maybe.maybe_error(ctx_function), string).
:- mode get_ctx_function_error(in, out) is semidet.
get_ctx_function_error(maybe.error(E), E).
:- pragma foreign_export("C",
    get_ctx_function_error(in, out),
    "Saffron_GetCtxFunctionError").

%-----------------------------------------------------------------------------%
% These are kind of just to make loading from PNGs and TGAs more efficient.
red_mask = 0x000000FF.
green_mask = 0x0000FF00.
blue_mask = 0x00FF0000.
alpha_mask = 0xFF000000.

%-----------------------------------------------------------------------------%

red_shift = 0.
green_shift = 8.
blue_shift = 16.
alpha_shift = 24.

%-----------------------------------------------------------------------------%

red_bits = 8.
green_bits = 8.
blue_bits = 8.
alpha_bits = 8.

%-----------------------------------------------------------------------------%

red(C) = C /\ 0xFF.
green(C) = unchecked_right_shift(C, green_shift) /\ 0xFF. 
blue(C) = unchecked_right_shift(C, blue_shift) /\ 0xFF. 
alpha(C) = unchecked_right_shift(C, alpha_shift) /\ 0xFF. 

%-----------------------------------------------------------------------------%

'red :='(R, C) = (C /\ \red_mask) \/ (R /\ 0xFF).
'green :='(G, C) =
    (C /\ \red_mask) \/ unchecked_left_shift(G /\ 0xFF, green_shift).
'blue :='(B, C) =
    (C /\ \blue_mask) \/ unchecked_left_shift(B /\ 0xFF, blue_shift).
'alpha :='(A, C) =
    (C /\ \alpha_mask) \/ unchecked_left_shift(A /\ 0xFF, alpha_shift).

%-----------------------------------------------------------------------------%

transparent = 0.
transparent_white = 0x00FFFFFF.
white = 0xFFFFFFFF.
black = 0xFF000000.
gray = 0xFF808080.
red = 0xFF0000FF.
blue = 0xFFFF0000.
yellow = 0xFF00FFFF.
green = 0xFF00FF00.
purple = 0xFFF020A0. % X11 color value.
orange = 0xFF0080FF.
cyan = 0xFFFFFF00.
magenta = 0xFF00FF.

%-----------------------------------------------------------------------------%

major(sem_ver(Mj)) = Mj.
major(sem_ver(Mj, _)) = Mj.
major(sem_ver(Mj, _, _)) = Mj.

minor(sem_ver(_)) = (0).
minor(sem_ver(_, Mn)) = Mn.
minor(sem_ver(_, Mn, _)) = Mn.

patch(sem_ver(_)) = (0).
patch(sem_ver(_, _)) = 0.
patch(sem_ver(_, _, P)) = P.

%-----------------------------------------------------------------------------%

sem_ver_compare(Cmp, S1, S2) :-
    builtin.compare(MjCmp, major(S1), major(S2)),
    (
        MjCmp = (>),
        Cmp = (>)
    ;
        MjCmp = (<),
        Cmp = (<)
    ;
        MjCmp = (=),
        builtin.compare(MnCmp, minor(S1), minor(S2)),
        (
            MnCmp = (>),
            Cmp = (>)
        ;
            MnCmp = (<),
            Cmp = (<)
        ;
            MnCmp = (=),
            builtin.compare(Cmp, patch(S1), patch(S2))
        )
    ).

%-----------------------------------------------------------------------------%

parse_sem_ver(Str) = Result :-
    MajorList = string.split_at_char('.', Str),
    (
        MajorList = [],
        Result = maybe.error("Empty version string")
    ;
        MajorList = [MajorString|MinorList],
        ( if
            string.to_int(string.lstrip(MajorString), Major)
        then
            (
                MinorList = [],
                Result = maybe.ok(sem_ver(Major))
            ;
                MinorList = [MinorString|PatchList],
                ( if
                    string.to_int(MinorString, Minor)
                then
                    ( if
                        PatchList = [PatchString|_],
                        string.to_int(string.strip(PatchString), Patch)
                    then
                        SemVer = sem_ver(Major, Minor, Patch)
                    else
                        % We need to be tolerant of bad patch versions, unfortunately
                        SemVer = sem_ver(Major, Minor)
                    ),
                    Result = maybe.ok(SemVer)
                else
                    Result = maybe.error(string.append("Invalid minor version: ", MinorString))
                )
            )
        else
            Result = maybe.error(string.append("Invalid major version: ", MajorString))
        )
    ).

