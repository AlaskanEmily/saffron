% Copyright (c) 2014-2022 AlaskanEmily
% Copyright (c) 2018-2022 Transnat Games
% Copyright (c) 2014 Eric Armstrong
%
% This software is provided 'as-is', without any express or implied
% warranty.  In no event will the authors be held liable for any damages
% arising from the use of this software.
%
% Permission is granted to anyone to use this software for any purpose,
% including commercial applications, and to alter it and redistribute it
% freely, subject to the following restrictions:
%
% 1. The origin of this software must not be misrepresented; you must not
%    claim that you wrote the original software. If you use this software
%    in a product, an acknowledgment in the product documentation would be
%    appreciated but is not required.
% 2. Altered source versions must be plainly marked as such, and must not be
%    misrepresented as being the original software.
% 3. This notice may not be removed or altered from any source distribution.

:- module saffron_tga.
%=============================================================================%
% Simplified TGA image reader, taken from Athena. Used for the Saffron demos.
% This absolutely does NOT handle all TGA images, they must be specially
% encoded to be RGBA32 to work with this, although RLE is supported.
:- interface.
%=============================================================================%

:- use_module bitmap.
:- use_module io.
:- import_module list.

%-----------------------------------------------------------------------------%
% Simplest representation, since this is just for demos.
% The bitmap is laid out such that the bits are red, green, blue, alpha.
% This agrees with Saffron on little-endian platforms. We should really fix
% Saffron to have different shifts so that the memory layout of colors is not
% dependent on endianness.
:- type image == {bitmap.bitmap, int, int}.

%-----------------------------------------------------------------------------%

:- pred load_path(string, io.res(image), io.io, io.io).
:- mode load_path(in, out, di, uo) is det.

%-----------------------------------------------------------------------------%

:- pred load_stream(io.binary_input_stream, io.res(image), io.io, io.io).
:- mode load_stream(in, out, di, uo) is det.

%-----------------------------------------------------------------------------%

:- type options --->
    no_rle ; % Do not use run-length encoding.
    uncommon_format. % Allow the use of A4R4G4B4 or A8R5G6B5

%-----------------------------------------------------------------------------%

:- pred save_path(string, image, io.res, io.io, io.io).
:- mode save_path(in, in, out, di, uo) is det.

%-----------------------------------------------------------------------------%

:- pred save_path(string, image, list.list(options), io.res, io.io, io.io).
:- mode save_path(in, in, in, out, di, uo) is det.

%-----------------------------------------------------------------------------%

:- pred save_stream(io.binary_output_stream, image, io.io, io.io).
:- mode save_stream(in, in, di, uo) is det.

%-----------------------------------------------------------------------------%

:- pred save_stream(io.binary_output_stream, image, list.list(options), io.io, io.io).
:- mode save_stream(in, in, in, di, uo) is det.

%=============================================================================%
:- implementation.
%=============================================================================%

:- use_module char.
:- use_module exception.
:- use_module string.
:- use_module stream.
:- import_module int.

%-----------------------------------------------------------------------------%

:- inst res_error == bound(io.error(ground)).
:- mode res_error == free >> res_error.

%-----------------------------------------------------------------------------%

:- func error_truncated = io.error.
error_truncated = io.make_io_error("Image file is truncated").

%-----------------------------------------------------------------------------%

:- func res_truncated = (io.res(_)::res_error) is det.
res_truncated = io.error(error_truncated).

:- func res0_truncated = (io.res::res_error) is det.
res0_truncated = io.error(error_truncated).

%-----------------------------------------------------------------------------%

:- func error_oversize = io.error.
error_oversize = io.make_io_error("Pixel data is larger than dimensions.").

%-----------------------------------------------------------------------------%

:- func res_oversize = (io.res(_)::res_error) is det.
res_oversize = io.error(error_oversize).

:- func res0_oversize = (io.res::res_error) is det.
res0_oversize = io.error(error_oversize).

%-----------------------------------------------------------------------------%

load_path(Path, Result, !IO) :-
    io.open_binary_input(Path, StreamResult, !IO),
    (
        StreamResult = io.error(_),
        IOErr = io.make_io_error(string.append("Could not open file ", Path)),
        Result = io.error(IOErr)
    ;
        StreamResult = io.ok(Stream),
        load_stream(Stream, NoNameResult, !IO),
        ( if
            NoNameResult = io.error(Err)
        then
            IOErr = io.make_io_error(string.append(string.append(string.append(
                "Error in file ", Path),
                ": "),
                io.error_message(Err))),
            Result = io.error(IOErr)
        else
            Result = NoNameResult
        ),
        io.close_binary_input(Stream, !IO)
    ).

%-----------------------------------------------------------------------------%

load_stream(Stream, Result, !IO) :-
    % Read the header.
    some [!Buffer] (
        !:Buffer = bitmap.init(80), % 10 bytes, the largest individual field
        io.read_bitmap(Stream, 0, 3, !Buffer, NBytes, HeaderRes, !IO),
        ( if
            NBytes = 3
        then
            (
                HeaderRes = io.error(Err),
                Result = io.error(Err)
            ;
                HeaderRes = io.ok,
                IDLen = !.Buffer ^ bitmap.byte(0),
                ColorMapType = !.Buffer ^ bitmap.byte(1),
                ImageType = !.Buffer ^ bitmap.byte(2),
                % Skip the color map specification, we don't use that.
                stream.seek(Stream, stream.cur, 5, !IO),
                load_image_specification(Stream, !Buffer, SpecResult, !IO),
                % Skip the ID field, it's not useful.
                stream.seek(Stream, stream.cur, IDLen, !IO),
                (
                    SpecResult = io.ok(Spec),
                    ( if
                        ImageType = 0
                    then
                        % No Image.
                        Result = io.ok({bitmap.init(0), 0, 0})
                    else
                        ( if
                            ColorMapType = 0,
                            (
                                ImageType = 10, % RLE image
                                Pred = load_rle
                            ;
                                ImageType = 2, % Bitmapped image
                                Pred = load_uncompressed
                            )
                        then
                            Format = Spec ^ format,
                            Pred(Stream, 0, Format, gen_bmp(Spec), BMP, Res, !IO),
                            (
                                Res = io.error(Err),
                                Result = io.error(Err)
                            ;
                                Res = io.ok,
                                Result = io.ok({BMP, Spec ^ w, Spec ^ h})
                            )
                        else
                            IOErr = io.make_io_error("Unsupported TGA image type"),
                            trace [io(!XIO)] (
                                io.write_line({"ColorMapType: ", ColorMapType,
                                    "ImageType: ", ImageType},
                                    !XIO)
                            ),
                            Result = io.error(IOErr)
                        )
                    )
                ;
                    SpecResult = io.error(IOErr),
                    Result = io.error(IOErr)
                )
            )
        else
            Result = res_truncated
        ),
        % Silence state variable warning...
        !.Buffer = _
    ).

%-----------------------------------------------------------------------------%

:- pred write_pixel(int, int, bitmap.bitmap, bitmap.bitmap).
:- mode write_pixel(in, in, bitmap.bitmap_di, bitmap.bitmap_uo) is det.

write_pixel(I, Pixel, !BMP) :-
    O = int.unchecked_left_shift(I, 2),
    !BMP ^ bitmap.unsafe_byte(O) := Pixel /\ 0xFF,
    !BMP ^ bitmap.unsafe_byte(O + 1) :=
        int.unchecked_right_shift(Pixel, 8) /\ 0xFF,
    !BMP ^ bitmap.unsafe_byte(O + 2) :=
        int.unchecked_right_shift(Pixel, 16) /\ 0xFF,
    !BMP ^ bitmap.unsafe_byte(O + 3) :=
        int.unchecked_right_shift(Pixel, 24) /\ 0xFF.

%-----------------------------------------------------------------------------%

:- pred write_pixels(int, int, int, bitmap.bitmap, bitmap.bitmap).
:- mode write_pixels(in, in, in, bitmap.bitmap_di, bitmap.bitmap_uo) is det.

write_pixels(I, N, Pixel, !BMP) :-
    ( if
        N > 0
    then
        write_pixel(I, Pixel, !BMP),
        write_pixels(I + 1, N - 1, Pixel, !BMP)
    else
        true
    ).

%-----------------------------------------------------------------------------%

:- type format --->
    rgb16 ;
    argb16 ;
    rgb24 ;
    argb32 ;
    xrgb32.

%-----------------------------------------------------------------------------%

:- inst bpp == bound(16 ; 24 ; 32).
:- mode bpp_out == (free >> bpp).

%-----------------------------------------------------------------------------%
% format(BitsPerPixel, AlphaBits, BitDepth)
:- pred format(int, int, format).
:- mode format(in, in, in) is semidet. % Implied.
:- mode format(in, in, out) is semidet.
:- mode format(bpp_out, out, in) is det.
:- mode format(bpp_out, out, out) is multi. % Implied.
% :- mode format(in, out, in) is nondet.

format(16, 0, rgb16).
format(16, 1, argb16).
format(24, 0, rgb24).
format(32, 8, argb32).
format(32, 0, xrgb32).

%-----------------------------------------------------------------------------%
% extend_int(Input, Bits) = Out.
:- func extend_int(int, int) = int.
extend_int(In, Bits) =
    (int.unchecked_left_shift(In, 8 - Bits) \/ Ext) :-
    (In /\ 1 = 0 -> Ext = 0 ; Ext = int.unchecked_left_shift(1, 8 - Bits) - 1).

:- pragma inline(extend_int/2).

%-----------------------------------------------------------------------------%
% mask_int(In, Index, Bits) = Out.
:- func mask_int(int, int, int) = int.
mask_int(In, Index, Bits) =
    (int.unchecked_right_shift(In, Index) /\ Mask) :-
    Mask = int.unchecked_left_shift(1, Bits) - 1.

:- pragma inline(mask_int/3).

%-----------------------------------------------------------------------------%
% component(In, Index, Bits, Offset) = Out
:- func component(int, int, int, int) = int.
component(In, Index, Bits, Offset) =
    int.unchecked_left_shift(extend_int(mask_int(In, Index, Bits), Bits), Offset).

:- pragma inline(component/4).

%-----------------------------------------------------------------------------%
% component(In, Index, Bits, Offset) = Out
:- func component_invert(int, int, int, int) = int.
component_invert(In, Index, Bits, Offset) =
    int.unchecked_left_shift(int.xor(extend_int(mask_int(In, Index, Bits), Bits), 0xFF), Offset).

:- pragma inline(component/4).

%-----------------------------------------------------------------------------%
% Importantly, read_int will reverse the byte order, so this accepts bytes in
% reversed order too.
:- func decode_pixel(format, int) = int.
decode_pixel(rgb16, X) = 
    % Red
    R \/
    int.unchecked_left_shift(G, 8) \/
    % Blue
    int.unchecked_left_shift(B, 16) \/
    % Alpha, always full.
    0xFF000000 :-
    
    % Stored as RRRRRGGG, GGGBBBBB
    P = int.unchecked_left_shift(X, 8) \/ int.unchecked_right_shift(X, 8),
    
    % Mask from the lowest bit
    ( P /\ 0x0800 = 0 -> RMask = 0 ; RMask = 0x07 ),
    % Shift, then replace the low bits.
    R = int.unchecked_right_shift(P, 8) \/ RMask,
    
    % Mask from the lowest bit
    ( P /\ 0x0020 = 0 -> GMask = 0 ; GMask = 0x03 ),
    % Shift, then replace the low bits.
    G = (int.unchecked_right_shift(P, 2) /\ 0xFC) \/ GMask,
    
    % Mask from the lowest bit
    ( P /\ 0x0001 = 0 -> BMask = 0 ; BMask = 0x07 ),
    % Shift, then replace the low bits.
    B = (int.unchecked_left_shift(P, 3) /\ 0xFF) \/ BMask.

decode_pixel(argb16, X) =
    % Red
    R \/
    int.unchecked_left_shift(G, 8) \/
    % Blue
    int.unchecked_left_shift(B, 16) \/
    % Alpha
    Alpha :-
    
    % Stored as ARRRRRGG, GGGBBBBB
    P = int.unchecked_left_shift(X, 8) \/ int.unchecked_right_shift(X, 8),

    % Mask from the lowest bit
    ( P /\ 0x0400 = 0 -> RMask = 0 ; RMask = 0x07 ),
    % Shift, then replace the low bits.
    R = (int.unchecked_right_shift(P, 7) /\ 0xF8) \/ RMask,
    
    % Mask from the lowest bit
    ( P /\ 0x0020 = 0 -> GMask = 0 ; GMask = 0x07 ),
    % Shift, then replace the low bits.
    G = (int.unchecked_right_shift(P, 2) /\ 0xF8) \/ GMask,
    
    % Mask from the lowest bit
    ( P /\ 0x0001 = 0 -> BMask = 0 ; BMask = 0x07 ),
    % Shift, then replace the low bits.
    B = (int.unchecked_left_shift(P, 3) /\ 0xFF) \/ BMask,

    % Alpha is one bit, but it's inverted?
    ( P /\ 0x8000 = 0 -> Alpha = 0xFF000000 ; Alpha = 0 ).

decode_pixel(rgb24, P) = P \/ 0xFF000000.
decode_pixel(xrgb32, P) = P \/ 0xFF000000.
decode_pixel(argb32, P) =
    int.unchecked_right_shift(P, 8) \/
    int.unchecked_left_shift(P /\ 0xFF, 24).

%-----------------------------------------------------------------------------%

:- func int16le(int, bitmap.bitmap) = int.
int16le(N, BMP) = (BMP ^ bitmap.byte(N) \/
    int.unchecked_left_shift(BMP ^ bitmap.byte(N + 1), 8)).

:- pragma inline(int16le/2).

%-----------------------------------------------------------------------------%

:- pred read_int(Stream, int, int, stream.result(int, Error), State, State)
    <= stream.reader(Stream, int, State, Error).
:- mode read_int(in, in, di, out, di, uo) is det.

read_int(Stream, N, I, Result, !State) :-
    ( if
        N > 0
    then
        stream.get(Stream, ByteResult, !State),
        ( if
            ByteResult = stream.ok(Byte)
        then
            New = int.unchecked_left_shift(I, 8) \/ Byte,
            read_int(Stream, N - 8, New, Result, !State)
        else
            ByteResult = Result
        )
    else
        Result = stream.ok(I)
    ).

:- pragma inline(read_int/6).

%-----------------------------------------------------------------------------%

:- pred read_pixel(Stream, format, stream.result(int, Error), State, State)
    <= stream.reader(Stream, int, State, Error).
:- mode read_pixel(in, in, out, di, uo) is det.

read_pixel(Stream, Format, Result, !State) :-
    format(Bits, _, Format),
    read_int(Stream, Bits, 0, RawPixelResult, !State),
    ( if
        RawPixelResult = stream.ok(RawPixel)
    then
        Result = stream.ok(decode_pixel(Format, RawPixel))
    else
        RawPixelResult = Result
    ).

%-----------------------------------------------------------------------------%

:- type image_specification ---> image_specification(
    w::int,
    h::int,
    format::format).

:- pred load_image_specification(
    io.binary_input_stream,
    bitmap.bitmap, bitmap.bitmap,
    io.res(image_specification),
    io.io, io.io).

:- mode load_image_specification(
    in,
    bitmap.bitmap_di, bitmap.bitmap_uo,
    out,
    di, uo) is det.

load_image_specification(Stream, !Buffer, Result, !IO) :-
    io.read_bitmap(Stream, 0, 10, !Buffer, NBytes, SpecRes, !IO),
    ( if
        NBytes = 10
    then
        (
            SpecRes = io.error(Error),
            Result = io.error(Error)
        ;
            SpecRes = io.ok,
            W = !.Buffer ^ int16le(4),
            H = !.Buffer ^ int16le(6),
            Depth = !.Buffer ^ bitmap.byte(8),
            Alpha = !.Buffer ^ bitmap.byte(9) /\ 0x0F,
            ( if
                % TODO: It looks like many encoders/decoders consider
                % Depth=16, Alpha=1 to be Depth=16, Alpha=0?
                %( if
                %    Alpha = 1, Depth=16
                %then
                %    Format = rgb16
                %else
                    format(Depth, Alpha, Format)
                %)
            then
                Result = io.ok(image_specification(W, H, Format))
            else
                IOErr = io.make_io_error("Unsupported TGA pixel format"),
                trace [io(!XIO)] (
                    io.write_line({
                        "W: ", W,
                        "H: ", H,
                        "Depth: ", Depth,
                        "Alpha: ", Alpha},
                        !XIO)
                ),
                Result = io.error(IOErr)
            )
        )
    else
        Result = res_truncated
    ).

%-----------------------------------------------------------------------------%

:- func gen_bmp(image_specification) = (bitmap.bitmap).
:- mode gen_bmp(in) = (bitmap.bitmap_uo) is det.
gen_bmp(Spec) = bitmap.init(int.unchecked_left_shift(Spec ^ w * Spec ^ h, 5)).

%-----------------------------------------------------------------------------%

:- type load_pred == (pred(
    io.binary_input_stream,
    int,
    format,
    bitmap.bitmap, bitmap.bitmap,
    io.res,
    io.io, io.io)).

:- inst load_pred == (pred(
    in,
    in,
    in,
    bitmap.bitmap_di, bitmap.bitmap_uo,
    out,
    di, uo) is det).

%-----------------------------------------------------------------------------%

:- type read_rle(Stream, State, Error) == (pred(
    Stream,
    int,
    int,
    format,
    bitmap.bitmap, bitmap.bitmap,
    stream.result(Error),
    State, State)).

:- inst read_rle == (pred(
    in,
    in,
    in,
    in,
    bitmap.bitmap_di, bitmap.bitmap_uo,
    out,
    di, uo) is det).

%-----------------------------------------------------------------------------%

:- pred read_rle_literal
    `with_type` read_rle(Stream, State, Error)
    `with_inst` read_rle
    <= stream.reader(Stream, int, State, Error).

read_rle_literal(Stream, I, N, Format, !BMP, Result, !State) :-
    ( if
        N > 0
    then
        read_pixel(Stream, Format, PixelResult, !State),
        (
            PixelResult = stream.error(Err),
            Result = stream.error(Err)
        ;
            PixelResult = stream.eof,
            Result = stream.eof
        ;
            PixelResult = stream.ok(Pixel),
            write_pixel(I, Pixel, !BMP),
            read_rle_literal(Stream, I + 1, N - 1, Format, !BMP, Result, !State)
        )
    else
        Result = stream.ok
    ).

%-----------------------------------------------------------------------------%

:- pred read_rle_run
    `with_type` read_rle(Stream, State, Error)
    `with_inst` read_rle
    <= stream.reader(Stream, int, State, Error).
read_rle_run(Stream, I, N, Format, !BMP, Result, !State) :-
    read_pixel(Stream, Format, PixelResult, !State),
    (
        PixelResult = stream.error(Err),
        Result = stream.error(Err)
    ;
        PixelResult = stream.eof,
        Result = stream.eof
    ;
        PixelResult = stream.ok(Pixel),
        write_pixels(I, N, Pixel, !BMP),
        Result = stream.ok
    ).

%-----------------------------------------------------------------------------%

:- pred load_rle `with_type` load_pred `with_inst` load_pred.
load_rle(Stream, I, Format, !BMP, Res, !IO) :-
    ( if
        bitmap.byte_in_range(!.BMP, int.unchecked_left_shift(I, 2))
    then
        io.read_byte(Stream, CodeResult, !IO),
        (
            CodeResult = io.error(Err),
            Res = io.error(Err)
        ;
            CodeResult = io.eof,
            Res = res0_truncated
        ;
            CodeResult = io.ok(Code),
            N = (Code /\ 0x7F) + 1,
            Bit = Code /\ 0x80,
            ( Bit = 0 -> Pred = read_rle_literal ; Pred = read_rle_run ),
            % Do bounds checking so that the read func below doesn't need to.
            ( if
                bitmap.byte_in_range(!.BMP, int.unchecked_left_shift(I + N - 1, 2))
            then
                Pred(Stream, I, N, Format, !BMP, ReadRes, !IO),
                (
                    ReadRes = stream.error(Err),
                    Res = io.error(Err)
                ;
                    ReadRes = stream.eof,
                    Res = res0_truncated
                ;
                    ReadRes = stream.ok,
                    load_rle(Stream, I + N, Format, !BMP, Res, !IO)
                )
            else
                Res = res0_oversize
            )
        )
    else
        Res = io.ok
    ).

%-----------------------------------------------------------------------------%

:- pred load_uncompressed `with_type` load_pred `with_inst` load_pred.
load_uncompressed(Stream, I, Format, !BMP, Res, !IO) :-
    ( if
        bitmap.byte_in_range(!.BMP, int.unchecked_left_shift(I, 2))
    then
        read_pixel(Stream, Format, PixelResult, !IO),
        (
            PixelResult = stream.error(Err),
            Res = io.error(Err)
        ;
            PixelResult = stream.eof,
            Res = res0_truncated
        ;
            PixelResult = stream.ok(Pixel),
            write_pixel(I, Pixel, !BMP),
            load_uncompressed(Stream, I + 1, Format, !BMP, Res, !IO)
        )
    else
        Res = io.ok
    ).

%-----------------------------------------------------------------------------%

save_path(Path, Image, Res, !IO) :-
    save_path(Path, Image, [], Res, !IO).

%-----------------------------------------------------------------------------%

save_path(Path, Image, Options, Res, !IO) :-
    io.open_binary_output(Path, StreamResult, !IO),
    (
        StreamResult = io.error(_),
        IOErr = io.make_io_error(string.append("Could not open file ", Path)),
        Res = io.error(IOErr)
    ;
        StreamResult = io.ok(Stream),
        save_stream(Stream, Image, Options, !IO),
        io.close_binary_output(Stream, !IO),
        Res = io.ok
    ).

%-----------------------------------------------------------------------------%

save_stream(Stream, Image, !IO) :-
    save_stream(Stream, Image, [], !IO).

%-----------------------------------------------------------------------------%

save_stream(Stream, {BMP, W, H}, _Options, !IO) :-
    % If this is an empty bitmap, we can just save this as type 0.
    ( if
        int.unchecked_left_shift(W * H, 4) \= bitmap.num_bytes(BMP)
    then
        exception.throw(exception.software_error(
            "Bitmap and dimensions do not match"))
    else if
        W = 0, H = 0
    then
        int.fold_up((pred(_::in, !.XIO::di, !:XIO::uo) is det :-
            io.write_byte(Stream, 0, !XIO)), 1, 18, !IO)
    else
        % Check for any alpha.
        ( if
            fold_pixels((pred(I::in) is semidet :- I \/ 0xFF000000 = 0), BMP)
        then
            % No alpha.
            Format = rgb24
        else
            Format = argb32
        ),
        save_header(Stream, W, H, Format, !IO),
        fold_pixels(write_pixel(Stream, Format), BMP, !IO)
    ).

%-----------------------------------------------------------------------------%

:- func id_field = string.
id_field = "Saffron Graphics".

%-----------------------------------------------------------------------------%

:- pred save_header(io.binary_output_stream, int, int, format, io.io, io.io).
:- mode save_header(in, in, in, in, di, uo) is det.
save_header(Stream, W, H, Format, !IO) :-
    io.write_byte(Stream, string.length(id_field), !IO), % Use our ID
    io.write_byte(Stream, 0, !IO), % ColorMapType
    % We only do no-rle RGB right now.
    io.write_byte(Stream, 2, !IO),
    % Write an empty color spec and X/Y
    int.fold_up((pred(_::in, !.XIO::di, !:XIO::uo) is det :-
        io.write_byte(Stream, 0, !XIO)), 1, 9, !IO),
    % Image spec, post X/Y
    io.write_byte(Stream, W /\ 0xFF, !IO), 
    io.write_byte(Stream, int.unchecked_right_shift(W, 8), !IO), 
    io.write_byte(Stream, H /\ 0xFF, !IO), 
    io.write_byte(Stream, int.unchecked_right_shift(H, 8), !IO),
    
    format(Depth, Alpha, Format),
    io.write_byte(Stream, Depth, !IO),
    Descriptor = Alpha,
    io.write_byte(Stream, Descriptor, !IO),
    
    string.foldl((pred(C::in, !.XIO::di, !:XIO::uo) is det :-
        % Our ID is only ASCII, so just one int per char.
        char.to_int(C, I),
        io.write_byte(Stream, I, !XIO)),
        id_field, !IO).
    

%-----------------------------------------------------------------------------%

:- type pixel_encoding --->
    pixel(int, int) ;
    pixel(int, int, int) ;
    pixel(int, int, int, int).

:- func encode_pixel(format, int) = pixel_encoding.
encode_pixel(rgb16, P) = pixel(
    % GGGBBBBB
    (int.unchecked_right_shift(P, 19) /\ 0x1F) \/
    (int.unchecked_right_shift(P, 3) /\ 0xE0),
    % RRRRRGGG
    int.unchecked_left_shift(P, 3) \/
    (int.unchecked_right_shift(P, 13) /\ 0x07) /\ 0xFF).

encode_pixel(argb16, P) = pixel(
    % GGGBBBBB
    (int.unchecked_right_shift(P, 19) /\ 0x1F) \/
    (int.unchecked_right_shift(P, 3) /\ 0xE0),
    % ARRRRRGG
    (int.unchecked_left_shift(P, 2) /\ 0x7C) \/
    (int.unchecked_right_shift(P, 14) /\ 0x03) \/ A) :-
    ( P /\ 0x80000000 = 0 -> A = 0x80 ; A = 0 ).

encode_pixel(rgb24, P) = pixel(
    P /\ 0xFF,
    int.unchecked_right_shift(P, 8) /\ 0xFF,
    int.unchecked_right_shift(P, 16) /\ 0xFF).

encode_pixel(xrgb32, P) = pixel(
    0,
    P /\ 0xFF,
    int.unchecked_right_shift(P, 8) /\ 0xFF,
    int.unchecked_right_shift(P, 16) /\ 0xFF).

encode_pixel(argb32, P) = pixel(
    int.unchecked_right_shift(P, 24) /\ 0xFF,
    P /\ 0xFF,
    int.unchecked_right_shift(P, 8) /\ 0xFF,
    int.unchecked_right_shift(P, 16) /\ 0xFF).

:- pragma inline(encode_pixel/2).

%-----------------------------------------------------------------------------%

:- pred write_pixel(io.binary_output_stream, format, int, io.io, io.io).
:- mode write_pixel(in, in, in, di, uo) is det.

write_pixel(Stream, Format, Pixel, !IO) :-
    encode_pixel(Format, Pixel) = X,
    (
        X = pixel(A, B),
        io.write_byte(Stream, A, !IO),
        io.write_byte(Stream, B, !IO)
    ;
        X = pixel(A, B, C),
        io.write_byte(Stream, A, !IO),
        io.write_byte(Stream, B, !IO),
        io.write_byte(Stream, C, !IO)
    ;
        X = pixel(A, B, C, D),
        io.write_byte(Stream, A, !IO),
        io.write_byte(Stream, B, !IO),
        io.write_byte(Stream, C, !IO),
        io.write_byte(Stream, D, !IO)
    ).

%-----------------------------------------------------------------------------%

:- func pixel(int::in, bitmap.bitmap::bitmap.bitmap_ui) = (int::uo) is det.
pixel(I, BMP) = 
    BMP ^ bitmap.byte(O) \/
    int.unchecked_left_shift(BMP ^ bitmap.byte(O + 1), 8) \/
    int.unchecked_left_shift(BMP ^ bitmap.byte(O + 2), 16) \/
    int.unchecked_left_shift(BMP ^ bitmap.byte(O + 3), 24) :-
    O = int.unchecked_left_shift(I, 2).

:- pragma inline(pixel/2).

%-----------------------------------------------------------------------------%

:- pred fold_pixels(pred(int), bitmap.bitmap).
:- mode fold_pixels(pred(in) is semidet, bitmap.bitmap_ui) is semidet.

%-----------------------------------------------------------------------------%

fold_pixels(Pred, BMP) :-
    int.fold_up((pred(I::in, U::in, U::out) is semidet :- Pred(pixel(I, BMP))),
        0, int.unchecked_right_shift(bitmap.num_bytes(BMP), 2) - 1, 0, _).

%-----------------------------------------------------------------------------%

:- pred fold_pixels(pred(int, T, T), bitmap.bitmap, T, T).
:- mode fold_pixels(pred(in, in, out) is semidet, bitmap.bitmap_ui, in, out) is semidet.
:- mode fold_pixels(pred(in, mdi, muo) is semidet, bitmap.bitmap_ui, mdi, muo) is semidet.
:- mode fold_pixels(pred(in, in, out) is det, bitmap.bitmap_ui, in, out) is det.
:- mode fold_pixels(pred(in, mdi, muo) is det, bitmap.bitmap_ui, mdi, muo) is det.
:- mode fold_pixels(pred(in, di, uo) is det, bitmap.bitmap_ui, di, uo) is det.

fold_pixels(Pred, BMP, !T) :-
    int.fold_up(fold_pixels_x(Pred, BMP),
        0, int.unchecked_right_shift(bitmap.det_num_bytes(BMP), 2) - 1, !T).

%-----------------------------------------------------------------------------%
% This must be its own predicate to allow multiple modes.
% A lambda wouldn't work for this inside fold_pixels/4.
:- pred fold_pixels_x(pred(int, T, T), bitmap.bitmap, int, T, T).
:- mode fold_pixels_x(pred(in, in, out) is semidet, bitmap.bitmap_ui, in, in, out) is semidet.
:- mode fold_pixels_x(pred(in, mdi, muo) is semidet, bitmap.bitmap_ui, in, mdi, muo) is semidet.
:- mode fold_pixels_x(pred(in, in, out) is det, bitmap.bitmap_ui, in, in, out) is det.
:- mode fold_pixels_x(pred(in, mdi, muo) is det, bitmap.bitmap_ui, in, mdi, muo) is det.
:- mode fold_pixels_x(pred(in, di, uo) is det, bitmap.bitmap_ui, in, di, uo) is det.

fold_pixels_x(Pred, BMP, I, !T) :-
    Pred(pixel(I, BMP), !T).

%-----------------------------------------------------------------------------%

