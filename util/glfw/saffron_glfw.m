% Copyright (C) 2023 AlaskanEmily, Transnat Games
%
% this software is provided 'as-is', without any express or implied
% warranty.  in no event will the authors be held liable for any damages
% arising from the use of this software.
%
% permission is granted to anyone to use this software for any purpose,
% including commercial applications, and to alter it and redistribute it
% freely, subject to the following restrictions:
%
% 1. the origin of this software must not be misrepresented; you must not
%    claim that you wrote the original software. if you use this software
%    in a product, an acknowledgment in the product documentation would be
%    appreciated but is not required.
% 2. altered source versions must be plainly marked as such, and must not be
%    misrepresented as being the original software.
% 3. this notice may not be removed or altered from any source distribution.

:- module saffron_glfw.
%=============================================================================%
% Implementation of saffron_delegate using GLFW3.
%
% This has both C and Java implementations, but it is primarily intended to be
% used in Java through LWJGL.
%
% There isn't a real need to use this in the C backend, as the OS X
% implementation of saffron_delegate and the glow backend of saffron_window are
% generally better and require fewer dependencies. You could use it if you
% only want to use saffron_delegate instead of saffron_window on some
% platforms, but doing that will mean more dependencies and more code running
% on the most common platforms.
:- interface.
%=============================================================================%

:- use_module io.

:- use_module saffron.
:- use_module saffron.gl.
:- use_module saffron_delegate.

%-----------------------------------------------------------------------------%

:- inst io_res_uniq == unique(io.ok(unique) ; io.error(ground)).
:- mode io_res_uo == (free >> io_res_uniq).

%-----------------------------------------------------------------------------%

:- type window.

%-----------------------------------------------------------------------------%

:- instance saffron.context(window).

%-----------------------------------------------------------------------------%

:- instance saffron.gl.context(window).

%-----------------------------------------------------------------------------%

:- instance saffron_delegate.window(window).

%-----------------------------------------------------------------------------%
% create_window(T, W, H, Title, GLMajor, GLMinor, Result, !IO).
:- pred create_window(T, int, int, string, int, int, io.res(window), io.io, io.io)
    <= saffron_delegate.delegate(T).
:- mode create_window(in, in, in, in, in, in, io_res_uo, di, uo) is det.

%=============================================================================%
:- implementation.
%=============================================================================%

:- use_module maybe.
:- use_module bool.

:- pragma foreign_import_module("C", saffron_window).
:- pragma foreign_import_module("C", saffron_delegate).

%-----------------------------------------------------------------------------%

:- pragma foreign_decl("C",
    "
#include <GLFW/glfw3.h>
extern MR_Bool saffron_glfw_init_ok;

void SaffronGLFW_KeyCallback(
    GLFWwindow *win,
    int key,
    int scancode,
    int action,
    int mods);

void SaffronGLFW_MouseButtonCallback(
    GLFWwindow *win,
    int button,
    int action,
    int mods);

void SaffronGLFW_MouseMotionCallback(
    GLFWwindow *win,
    double x,
    double y);
    
#define SAFFRON_GLFW_GET_ERROR(OK, ERR) do{ \\
    const char *SAFFRON_GLFW_GET_ERROR_err; \\
    if(glfwGetError(&SAFFRON_GLFW_GET_ERROR_err) == 0){ \\
        (OK) = 1; \\
        (ERR) = (MR_String)""<NULL>""; \\
    } \\
    else{ \\
        (OK) = 0; \\
        const long SAFFRON_GLFW_GET_ERROR_len = \\
            strlen(SAFFRON_GLFW_GET_ERROR_err); \\
        MR_allocate_aligned_string_msg( \\
            ERR, \\
            SAFFRON_GLFW_GET_ERROR_len, \\
            MR_ALLOC_ID); \\
        memcpy((ERR), \\
            SAFFRON_GLFW_GET_ERROR_err, \\
            SAFFRON_GLFW_GET_ERROR_len + 1); \\
    } \\
}while(0)
    ").

%-----------------------------------------------------------------------------%

:- pragma foreign_code("C",
    "
MR_Bool saffron_glfw_init_ok = MR_NO;

void SaffronGLFW_KeyCallback(
    GLFWwindow *win,
    int key,
    int scancode,
    int action,
    int mods){
    
    MR_Word delegate;
    MR_Word mr_key;
    delegate = (MR_Word)glfwGetWindowUserPointer(win);
    
    /* Create the key data. */
    if(key >= 32 && key <= 96){
        mr_key = Saffron_CreateKey(key);
    }
    else{
        switch(key){
            case GLFW_KEY_ESCAPE:
                mr_key = SAFFRON_KEY_ESCAPE;
                break;
            case GLFW_KEY_ENTER:
                mr_key = SAFFRON_KEY_ENTER;
                break;
            case GLFW_KEY_TAB:
                mr_key = SAFFRON_KEY_TAB;
                break;
            case GLFW_KEY_BACKSPACE:
                mr_key = SAFFRON_KEY_BACKSPACE;
                break;
            case GLFW_KEY_DELETE:
                mr_key = SAFFRON_KEY_DELETE;
                break;
            case GLFW_KEY_RIGHT:
                mr_key = SAFFRON_KEY_RIGHT_ARROW;
                break;
            case GLFW_KEY_LEFT:
                mr_key = SAFFRON_KEY_LEFT_ARROW;
                break;
            case GLFW_KEY_DOWN:
                mr_key = SAFFRON_KEY_DOWN_ARROW;
                break;
            case GLFW_KEY_UP:
                mr_key = SAFFRON_KEY_UP_ARROW;
                break;
            case GLFW_KEY_LEFT_SHIFT: /* FALLTHROUGH */
            case GLFW_KEY_RIGHT_SHIFT:
                mr_key = SAFFRON_KEY_SHIFT;
                break;
            case GLFW_KEY_LEFT_CONTROL: /* FALLTHROUGH */
            case GLFW_KEY_RIGHT_CONTROL:
                mr_key = SAFFRON_KEY_CONTROL;
                break;
            default:
                return;
        }
        Saffron_CreateSpecialKey(mr_key, &mr_key);
    }
    switch(action){
        case GLFW_PRESS:
            Saffron_DelegateKeyPress(delegate, mr_key);
            break;
        case GLFW_RELEASE:
            Saffron_DelegateKeyRelease(delegate, mr_key);
            break;
    }
}

void SaffronGLFW_MouseButtonCallback(
    GLFWwindow *win,
    int button,
    int action,
    int mods){
    
    MR_Word delegate;
    MR_Word mr_button;
    double x, y;
    delegate = (MR_Word)glfwGetWindowUserPointer(win);
    
    switch(button){
        case GLFW_MOUSE_BUTTON_LEFT:
            mr_button = SAFFRON_BUTTON_LEFT;
            break;
        case GLFW_MOUSE_BUTTON_RIGHT:
            mr_button = SAFFRON_BUTTON_RIGHT;
            break;
        case GLFW_MOUSE_BUTTON_MIDDLE:
            mr_button = SAFFRON_BUTTON_MIDDLE;
            break;
        default:
            return;
    }
    glfwGetCursorPos(win, &x, &y);
    switch(action){
        case GLFW_PRESS:
            Saffron_DelegateButtonPress(delegate, mr_button, x, y);
            break;
        case GLFW_RELEASE:
            Saffron_DelegateButtonRelease(delegate, mr_button, x, y);
            break;
    }
}

void SaffronGLFW_MouseMotionCallback(
    GLFWwindow *win,
    double x,
    double y){
    
    MR_Word delegate;
    delegate = (MR_Word)glfwGetWindowUserPointer(win);
    
    Saffron_DelegateMouseMotion(delegate, x, y);
}

    ").

:- pragma foreign_decl("Java", "
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL;
import org.lwjgl.PointerBuffer;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWKeyCallbackI;
import org.lwjgl.glfw.GLFWCursorPosCallbackI;
import org.lwjgl.glfw.GLFWMouseButtonCallbackI;
import jmercury.saffron_delegate.Delegate_private_0;
import jmercury.saffron_window.Special_key_0;
import jmercury.saffron_window.Key_0;
import jmercury.saffron_window.Button_0;
import java.io.IOException;
    ").

:- pragma foreign_code("Java", "
public static boolean init_ok = false;
public static class GLFWException extends IOException{
    public GLFWException(String msg){
        super(msg);
    }
    public static void Create() throws IOException{
        final PointerBuffer buffer = PointerBuffer.allocateDirect(1);
        if(GLFW.glfwGetError(buffer) != 0)
            throw new GLFWException(buffer.getStringUTF8());
    }
}
public static class Window{
    private class KeyDelegate implements GLFWKeyCallbackI{
        public void invoke(long window, int key, int scancode, int action, int mods){
            final Key_0 skey = GLFWKeyToSaffron(key);
            if(skey == null)
                return;
            if(action == GLFW.GLFW_PRESS)
                jmercury.saffron_delegate.KeyPress(delegate, skey);
            else if(action == GLFW.GLFW_RELEASE)
                jmercury.saffron_delegate.KeyRelease(delegate, skey);
        }
    }
    private class MouseMotionDelegate implements GLFWCursorPosCallbackI{
        public void invoke(long window, double xpos, double ypos){
            final int x = (int)xpos;
            final int y = (int)ypos;
            cursor_x = x;
            cursor_y = y;
            jmercury.saffron_delegate.MouseMotion(delegate, x, y);
        }
    }
    private class MouseButtonDelegate implements GLFWMouseButtonCallbackI{
        public void invoke(long window, int button, int action, int mods){
            Button_0 sbutton = null;
            switch(button){
                case GLFW.GLFW_MOUSE_BUTTON_LEFT:
                    sbutton = jmercury.saffron_window.SAFFRON_BUTTON_LEFT;
                    break;
                case GLFW.GLFW_MOUSE_BUTTON_RIGHT:
                    sbutton = jmercury.saffron_window.SAFFRON_BUTTON_RIGHT;
                    break;
                case GLFW.GLFW_MOUSE_BUTTON_MIDDLE:
                    sbutton = jmercury.saffron_window.SAFFRON_BUTTON_MIDDLE;
                    break;
                default:
                    return;
            }
            switch(action){
                case GLFW.GLFW_PRESS:
                    jmercury.saffron_delegate.ButtonPress(delegate, sbutton, cursor_x, cursor_y);
                    break;
                case GLFW.GLFW_RELEASE:
                    jmercury.saffron_delegate.ButtonRelease(delegate, sbutton, cursor_x, cursor_y);
                    break;
            }
        }
    }
    final private long win;
    final private Delegate_private_0 delegate;
    private int cursor_x;
    private int cursor_y;
    
    public Window(Delegate_private_0 d, int w, int h, String title) throws IOException{
        win = GLFW.glfwCreateWindow(w, h, title, 0, 0);
        delegate = d;
        if(win == org.lwjgl.system.MemoryUtil.NULL){
            GLFWException.Create();
            throw new IllegalStateException(""Window wasn't created, but no GLFW error?"");
        }
        GLFW.glfwMakeContextCurrent(win);
        GL.createCapabilities(); // TODO: This should be the actual context!
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT|GL11.GL_DEPTH_BUFFER_BIT);
        GLFW.glfwSetKeyCallback(win, new KeyDelegate());
        GLFW.glfwSetMouseButtonCallback(win, new MouseButtonDelegate());
        GLFW.glfwSetCursorPosCallback(win, new MouseMotionDelegate());
    }
    public void makeCurrent(){
        GLFW.glfwMakeContextCurrent(win);
    }
    public jmercury.bool.Bool_0 update(){
        if(GLFW.glfwWindowShouldClose(win))
            return jmercury.bool.YES;
        int[] w = new int[1];
        int[] h = new int[1];
        GLFW.glfwMakeContextCurrent(win);
        GLFW.glfwGetFramebufferSize(win, w, h);
        GL11.glViewport(0, 0, w[0], h[0]);
        jmercury.saffron_delegate.Redraw(delegate);
        GLFW.glfwSwapBuffers(win);
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT|GL11.GL_DEPTH_BUFFER_BIT);
        GLFW.glfwPollEvents();
        return jmercury.bool.NO;
    }
    public void show(){
        GLFW.glfwShowWindow(win);
    }
    public void hide(){
        GLFW.glfwHideWindow(win);
    }
}
private static Special_key_0 SaffronSpecialKey(int key){
    switch(key){
        case GLFW.GLFW_KEY_ESCAPE:
            return jmercury.saffron_window.SAFFRON_KEY_ESCAPE;
        case GLFW.GLFW_KEY_ENTER:
            return jmercury.saffron_window.SAFFRON_KEY_ENTER;
        case GLFW.GLFW_KEY_TAB:
            return jmercury.saffron_window.SAFFRON_KEY_TAB;
        case GLFW.GLFW_KEY_BACKSPACE:
            return jmercury.saffron_window.SAFFRON_KEY_BACKSPACE;
        case GLFW.GLFW_KEY_DELETE:
            return jmercury.saffron_window.SAFFRON_KEY_DELETE;
        case GLFW.GLFW_KEY_RIGHT:
            return jmercury.saffron_window.SAFFRON_KEY_RIGHT_ARROW;
        case GLFW.GLFW_KEY_LEFT:
            return jmercury.saffron_window.SAFFRON_KEY_LEFT_ARROW;
        case GLFW.GLFW_KEY_DOWN:
            return jmercury.saffron_window.SAFFRON_KEY_DOWN_ARROW;
        case GLFW.GLFW_KEY_UP:
            return jmercury.saffron_window.SAFFRON_KEY_UP_ARROW;
        case GLFW.GLFW_KEY_LEFT_SHIFT: //FALLTHROUGH
        case GLFW.GLFW_KEY_RIGHT_SHIFT:
            return jmercury.saffron_window.SAFFRON_KEY_SHIFT;
        case GLFW.GLFW_KEY_LEFT_CONTROL: // FALLTHROUGH
        case GLFW.GLFW_KEY_RIGHT_CONTROL:
            return jmercury.saffron_window.SAFFRON_KEY_CONTROL;
        default:
            return null;
    }
}
public static Key_0 GLFWKeyToSaffron(int key){
    // Create the key data.
    if(key >= 32 && key <= 96)
        return new Key_0.Key_1(key);
    final Special_key_0 special = SaffronSpecialKey(key);
    return (special == null) ?
        null :
        jmercury.saffron_window.CreateSpecialKey(special);
}
public static int GLFWHint(Hint_0 hint){
    if(hint.equals(SAFFRON_GLFW_CONTEXT_MAJOR))
        return GLFW.GLFW_CONTEXT_VERSION_MAJOR;
    else if(hint.equals(SAFFRON_GLFW_CONTEXT_MINOR))
        return GLFW.GLFW_CONTEXT_VERSION_MINOR;
    else if(hint.equals(SAFFRON_GLFW_DOUBLEBUFFER))
        return GLFW.GLFW_DOUBLEBUFFER;
    else if(hint.equals(SAFFRON_GLFW_RED_BITS))
        return GLFW.GLFW_RED_BITS;
    else if(hint.equals(SAFFRON_GLFW_GREEN_BITS))
        return GLFW.GLFW_GREEN_BITS;
    else if(hint.equals(SAFFRON_GLFW_BLUE_BITS))
        return GLFW.GLFW_BLUE_BITS;
    else if(hint.equals(SAFFRON_GLFW_ALPHA_BITS))
        return GLFW.GLFW_ALPHA_BITS;
    else if(hint.equals(SAFFRON_GLFW_RETINA))
        return GLFW.GLFW_COCOA_RETINA_FRAMEBUFFER;
    else if(hint.equals(SAFFRON_GLFW_NO_ERROR))
        return GLFW.GLFW_CONTEXT_NO_ERROR;
    else
        throw new IllegalStateException(""Unknown GLFW hint value"");
}
    ").

%-----------------------------------------------------------------------------%

:- pragma foreign_type("C", window, "GLFWwindow*").
:- pragma foreign_type("Java", window, "jmercury.saffron_glfw.Window").

:- type delegate == saffron_delegate.delegate_private.

%-----------------------------------------------------------------------------%

:- impure pred glfw_initialize is det.
glfw_initialize.
:- pragma foreign_proc("C",
    glfw_initialize,
    [will_not_call_mercury, thread_safe, will_not_throw_exception],
    "saffron_glfw_init_ok = glfwInit();").

:- pragma foreign_proc("Java",
    glfw_initialize,
    [will_not_call_mercury, thread_safe, will_not_throw_exception],
    "jmercury.saffron_glfw.init_ok = GLFW.glfwInit();").

:- initialize glfw_initialize/0.

%-----------------------------------------------------------------------------%

:- impure pred glfw_finalize is det.
glfw_finalize.
:- pragma foreign_proc("C",
    glfw_finalize,
    [will_not_call_mercury, thread_safe, will_not_throw_exception],
    "if(saffron_glfw_init_ok) glfwTerminate();").
:- pragma foreign_proc("Java",
    glfw_finalize,
    [will_not_call_mercury, thread_safe, will_not_throw_exception],
    "if(jmercury.saffron_glfw.init_ok) GLFW.glfwTerminate();").

:- finalize glfw_finalize/0.

%-----------------------------------------------------------------------------%

:- func create_window_ok(window::di) = (io.res(window)::uo) is det.
create_window_ok(Ctx) = io.ok(Ctx).
:- pragma foreign_export("C",
    create_window_ok(di) = (uo),
    "SaffronGLFW_CreateWindowOK").

%-----------------------------------------------------------------------------%

:- func create_window_error(string::di) = (io.res(window)::io_res_uo) is det.
create_window_error(Str) = io.error(io.make_io_error(Str)).
:- pragma foreign_export("C",
    create_window_error(di) = (io_res_uo),
    "SaffronGLFW_CreateWindowError").
:- pragma foreign_export("Java",
    create_window_error(di) = (io_res_uo),
    "CreateWindowError").

%-----------------------------------------------------------------------------%

:- pred make_current(window::in, io.io::di, io.io::uo) is det.
:- pragma foreign_proc("C",
    make_current(Win::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
     does_not_affect_liveness, may_duplicate],
    "
    IOo = IOi;
    glfwMakeContextCurrent(Win);
    ").
:- pragma foreign_proc("Java",
    make_current(Win::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    may_duplicate],
    "
    IOo = IOi;
    Win.makeCurrent();
    ").

%-----------------------------------------------------------------------------%

:- pred load_function(window, string, maybe.maybe_error(saffron.ctx_function), io.io, io.io).
:- mode load_function(in, in, out, di, uo) is det.

load_function(Win, Name, MaybeFunc, !IO) :-
    saffron.default_load_function(Win, Name, MaybeFunc, !IO).

:- pragma foreign_proc("C",
    load_function(Win::in, Name::in, MaybeFunc::out, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
     does_not_affect_liveness, may_duplicate],
    "
    GLFWglproc proc;
    MR_String err;
    int ok;
    IOo = IOi;
    (void)Win;
    proc = glfwGetProcAddress(Name);
    SAFFRON_GLFW_GET_ERROR(ok, err);
    MaybeFunc = ok ?
        Saffron_CreateCtxFunctionOK((MR_Word)proc) :
        Saffron_CreateCtxFunctionError(err);
    ").

%-----------------------------------------------------------------------------%

:- instance saffron.context(window) where [
    pred(saffron.make_current/3) is saffron_glfw.make_current,
    pred(saffron.load_function/5) is saffron_glfw.load_function
].

%-----------------------------------------------------------------------------%

:- instance saffron.gl.context(window) where [].

%-----------------------------------------------------------------------------%

:- pred show_window(window::in, io.io::di, io.io::uo) is det.
:- pragma foreign_proc("C",
    show_window(Win::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
     does_not_affect_liveness, may_duplicate],
    "
    IOo = IOi;
    glfwShowWindow(Win);
    ").
:- pragma foreign_proc("Java",
    show_window(Win::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    may_duplicate],
    "
    IOo = IOi;
    Win.show();
    ").

%-----------------------------------------------------------------------------%

:- pred hide_window(window::in, io.io::di, io.io::uo) is det.
:- pragma foreign_proc("C",
    hide_window(Win::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
     does_not_affect_liveness, may_duplicate],
    "
    IOo = IOi;
    glfwHideWindow(Win);
    ").
:- pragma foreign_proc("Java",
    hide_window(Win::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    may_duplicate],
    "
    IOo = IOi;
    Win.hide();
    ").

%-----------------------------------------------------------------------------%

:- pred update(window::in, bool.bool::uo, io.io::di, io.io::uo) is det.
:- pragma foreign_proc("C",
    update(Win::in, ShouldQuit::uo, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
     does_not_affect_liveness, may_duplicate],
    "
    int w, h;
    MR_Word delegate;
    IOo = IOi;
    ShouldQuit = glfwWindowShouldClose(Win);
    if(!ShouldQuit){
        glfwMakeContextCurrent(Win);
        glfwGetFramebufferSize(Win, &w, &h);
        glViewport(0, 0, w, h);
        delegate = (MR_Word)glfwGetWindowUserPointer(Win);
        Saffron_DelegateRedraw(delegate);
        glfwSwapBuffers(Win);
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
        glfwPollEvents();
    }
    ").

:- pragma foreign_proc("Java",
    update(Win::in, ShouldQuit::uo, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    may_duplicate],
    "
    IOo = IOi;
    ShouldQuit = Win.update();
    ").

%-----------------------------------------------------------------------------%

:- instance saffron_delegate.window(window) where [
    pred(saffron_delegate.show_window/3) is saffron_glfw.show_window,
    pred(saffron_delegate.hide_window/3) is saffron_glfw.hide_window,
    pred(saffron_delegate.update/4) is saffron_glfw.update
].

%-----------------------------------------------------------------------------%

:- type hint --->
    context_major ;
    context_minor ;
    doublebuffer ;
    red_bits ;
    green_bits ;
    blue_bits ;
    alpha_bits ;
    retina ;
    no_error.

:- pragma foreign_enum("C", hint/0, [
    context_major - "GLFW_CONTEXT_VERSION_MAJOR",
    context_minor - "GLFW_CONTEXT_VERSION_MINOR",
    doublebuffer - "GLFW_DOUBLEBUFFER",
    red_bits - "GLFW_RED_BITS",
    green_bits - "GLFW_GREEN_BITS",
    blue_bits - "GLFW_BLUE_BITS",
    alpha_bits - "GLFW_ALPHA_BITS",
    retina - "GLFW_COCOA_RETINA_FRAMEBUFFER",
    no_error - "GLFW_CONTEXT_NO_ERROR"
]).

:- pragma foreign_export_enum(
    "Java",
    hint/0,
    [prefix("SAFFRON_GLFW_")|[uppercase|[]]]).

%-----------------------------------------------------------------------------%

:- pred window_hint(hint::in, int::in, io.io::di, io.io::uo) is det.
:- pred reset_window_hints(io.io::di, io.io::uo) is det.

:- pragma foreign_proc("C",
    window_hint(Hint::in, I::in, IOi::di, IOo::uo),
    [promise_pure, will_not_call_mercury, will_not_throw_exception, thread_safe,
    may_duplicate, does_not_affect_liveness],
    "
    IOo = IOi;
    glfwWindowHint(Hint, I);
    ").

:- pragma foreign_proc("Java",
    window_hint(Hint::in, I::in, IOi::di, IOo::uo),
    [promise_pure, will_not_call_mercury, thread_safe, may_duplicate],
    "
    IOo = IOi;
    org.lwjgl.glfw.GLFW.glfwWindowHint(jmercury.saffron_glfw.GLFWHint(Hint), I);
    ").

:- pragma foreign_proc("C",
    reset_window_hints(IOi::di, IOo::uo),
    [promise_pure, will_not_call_mercury, will_not_throw_exception, thread_safe,
    may_duplicate, does_not_affect_liveness],
    "
    IOo = IOi;
    glfwDefaultWindowHints();
    ").

:- pragma foreign_proc("Java",
    reset_window_hints(IOi::di, IOo::uo),
    [promise_pure, will_not_call_mercury, will_not_throw_exception, thread_safe,
    may_duplicate],
    "
    IOo = IOi;
    org.lwjgl.glfw.GLFW.glfwDefaultWindowHints();
    ").


%-----------------------------------------------------------------------------%

:- pred create_window(delegate, int, int, string, io.res(window), io.io, io.io).
:- mode create_window(in, in, in, in, io_res_uo, di, uo) is det.

:- pragma foreign_proc("C",
    create_window(D::in, W::in, H::in, Title::in, Result::io_res_uo, IOi::di, IOo::uo),
    [promise_pure, will_not_call_mercury, will_not_throw_exception, thread_safe,
    may_duplicate, does_not_affect_liveness],
    "
    GLFWwindow *window;
    MR_String err;
    int ok;
    
    IOo = IOi;
    if(!saffron_glfw_init_ok){
        ok = 0;
        err = (MR_String)""GLFW did not initialize"";
    }
    else{
        // glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);
        window = glfwCreateWindow(W, H, Title, NULL, NULL);
        SAFFRON_GLFW_GET_ERROR(ok, err);
    }
    if(ok && window != NULL){
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
        glfwSetWindowUserPointer(window, (void*)D);
        glfwSetKeyCallback(window, SaffronGLFW_KeyCallback);
        glfwSetMouseButtonCallback(window, SaffronGLFW_MouseButtonCallback);
        glfwSetCursorPosCallback(window, SaffronGLFW_MouseMotionCallback);
        Result = SaffronGLFW_CreateWindowOK(window);
    }
    else{
        Result = SaffronGLFW_CreateWindowError(err);
    }
    ").

:- pragma foreign_proc("Java",
    create_window(D::in, W::in, H::in, Title::in, Result::io_res_uo, IOi::di, IOo::uo),
    [promise_pure, will_not_call_mercury, will_not_throw_exception, thread_safe,
    may_duplicate],
    "
    IOo = IOi;
    try{
        Result = (!jmercury.saffron_glfw.init_ok) ?
            jmercury.saffron_glfw.CreateWindowError(""GLFW did not initialize"") :
            new jmercury.io.Res_1.Ok_1(new jmercury.saffron_glfw.Window(D, W, H, Title));
    }
    catch(java.io.IOException error){
        Result = jmercury.saffron_glfw.CreateWindowError(error.getMessage());
    }
    ").


%-----------------------------------------------------------------------------%

% create_window(T, W, H, Title, GLMajor, GLMinor, Result, !IO).
create_window(D, W, H, Title, GLMajor, GLMinor, Result, !IO) :-
    reset_window_hints(!IO),
    window_hint(context_major, GLMajor, !IO),
    window_hint(context_minor, GLMinor, !IO),
    window_hint(retina, 1, !IO),
    Delegate = saffron_delegate.create_delegate_private(D),
    create_window(Delegate, W, H, Title, Result, !IO).

%-----------------------------------------------------------------------------%

