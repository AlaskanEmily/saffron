% Copyright (C) 2023 AlaskanEmily, Transnat Games
%
% this software is provided 'as-is', without any express or implied
% warranty.  in no event will the authors be held liable for any damages
% arising from the use of this software.
%
% permission is granted to anyone to use this software for any purpose,
% including commercial applications, and to alter it and redistribute it
% freely, subject to the following restrictions:
%
% 1. the origin of this software must not be misrepresented; you must not
%    claim that you wrote the original software. if you use this software
%    in a product, an acknowledgment in the product documentation would be
%    appreciated but is not required.
% 2. altered source versions must be plainly marked as such, and must not be
%    misrepresented as being the original software.
% 3. this notice may not be removed or altered from any source distribution.

:- module saffron_delegate.
%=============================================================================%
% This implements a windowing and event interface similar to saffron_window,
% but for asynchronous/delegate-based windowing libraries.
%
% This is intended to be used on OS X, Android, and with GLFW (including lwjgl
% from Java).
%
% This is a util rather than core because it isn't necessary from a rendering
% perspective, but is provided because something of this nature will be needed
% to implement the demos, and most games and applications will need to
% implement something resembling this on certain platforms.
%
% See GLFW and OSX utils for code that implements these backends. The planned
% Android backend will also use this.
%
% This uses saffron_window for event types and helpers.
:- interface.
%=============================================================================%

:- use_module bool.
:- use_module io.

:- use_module saffron_window.

%-----------------------------------------------------------------------------%

:- type key == saffron_window.key.
:- type button == saffron_window.button.

%-----------------------------------------------------------------------------%

:- type key_handler(T) == (pred(T, key, io.io, io.io)).
:- mode key_handler == (pred(in, in, di, uo) is det).
:- inst key_handler == (pred(in, in, di, uo) is det).

%-----------------------------------------------------------------------------%

:- type mouse_handler(T) == (pred(T, button, int, int, io.io, io.io)).
:- mode mouse_handler == (pred(in, in, in, in, di, uo) is det).
:- inst mouse_handler == (pred(in, in, in, in, di, uo) is det).

%-----------------------------------------------------------------------------%

:- type motion_handler(T) == (pred(T, int, int, io.io, io.io)).
:- mode motion_handler == (pred(in, in, in, di, uo) is det).
:- inst motion_handler == (pred(in, in, in, di, uo) is det).

%-----------------------------------------------------------------------------%

:- type resize_handler(T) == motion_handler(T).
:- mode resize_handler == motion_handler.
:- inst resize_handler == motion_handler.

%-----------------------------------------------------------------------------%

:- type redraw_handler(T) == (pred(T, io.io, io.io)).
:- mode redraw_handler == (pred(in, di, uo) is det).
:- inst redraw_handler == (pred(in, di, uo) is det).

%-----------------------------------------------------------------------------%

:- type quit_handler(T) == redraw_handler(T).
:- mode quit_handler == redraw_handler.
:- inst quit_handler == redraw_handler.

%-----------------------------------------------------------------------------%

:- typeclass delegate(T) where [
    pred key_press(T::in, key::in, io.io::di, io.io::uo) is det,
    
    pred key_release(T::in, key::in, io.io::di, io.io::uo) is det,
    
    pred button_press(T, button, int, int, io.io, io.io),
    mode button_press(in, in, in, in, di, uo) is det,
    
    pred button_release(T, button, int, int, io.io, io.io),
    mode button_release(in, in, in, in, di, uo) is det,
    
    pred mouse_motion(T::in, int::in, int::in, io.io::di, io.io::uo) is det,
    pred resize(T::in, int::in, int::in, io.io::di, io.io::uo) is det,
    pred quit(T::in, io.io::di, io.io::uo) is det,

    pred redraw(T::in, io.io::di, io.io::uo) is det
].

%-----------------------------------------------------------------------------%
% No-op handlers, for convenience.
:- pred key_nop
    `with_type` key_handler(_)
    `with_inst` key_handler.

:- pred button_nop
    `with_type` mouse_handler(_)
    `with_inst` mouse_handler.

:- pred motion_nop
    `with_type` motion_handler(_)
    `with_inst` motion_handler.

:- pred resize_nop
    `with_type` resize_handler(_)
    `with_inst` resize_handler.

:- pred quit_nop
    `with_type` quit_handler(_)
    `with_inst` quit_handler.

%-----------------------------------------------------------------------------%

:- pred handle_event(T, saffron_window.window_event, io.io, io.io)
    <= delegate(T).
:- mode handle_event(in, in, di, uo) is det.

%-----------------------------------------------------------------------------%

:- typeclass window(Win) where [
    pred show_window(Win::in, io.io::di, io.io::uo) is det,
    pred hide_window(Win::in, io.io::di, io.io::uo) is det,
    % update(Win, ShouldQuit, !IO)
    pred update(Win::in, bool.bool::uo, io.io::di, io.io::uo) is det
].

%-----------------------------------------------------------------------------%
% This is only intended to be used by implementations of saffron_delegate
:- type delegate_private.

%-----------------------------------------------------------------------------%

:- func create_delegate_private(T) = delegate_private <= delegate(T).

%=============================================================================%
:- implementation.
%=============================================================================%

:- type delegate_private --->
    some [T] delegate_private(T) => delegate(T).

%-----------------------------------------------------------------------------%

create_delegate_private(T) = 'new delegate_private'(T).
:- pragma foreign_export("Java", create_delegate_private(in) = (out),
    "CreateDelegate").

%-----------------------------------------------------------------------------%

:- pred private_key_press
    `with_type` key_handler(delegate_private)
    `with_inst` key_handler.
private_key_press(delegate_private(T), K, !IO) :- key_press(T, K, !IO).
:- pragma foreign_export("C", private_key_press(in, in, di, uo),
    "Saffron_DelegateKeyPress").
:- pragma foreign_export("Java", private_key_press(in, in, di, uo), "KeyPress").

%-----------------------------------------------------------------------------%

:- pred private_key_release
    `with_type` key_handler(delegate_private)
    `with_inst` key_handler.
private_key_release(delegate_private(T), K, !IO) :- key_release(T, K, !IO).
:- pragma foreign_export("C", private_key_release(in, in, di, uo),
    "Saffron_DelegateKeyRelease").
:- pragma foreign_export("Java", private_key_release(in, in, di, uo),
    "KeyRelease").

%-----------------------------------------------------------------------------%

:- pred private_button_press
    `with_type` mouse_handler(delegate_private)
    `with_inst` mouse_handler.
private_button_press(delegate_private(T), B, X, Y, !IO) :-
    button_press(T, B, X, Y, !IO).
:- pragma foreign_export("C", private_button_press(in, in, in, in, di, uo),
    "Saffron_DelegateButtonPress").
:- pragma foreign_export("Java", private_button_press(in, in, in, in, di, uo),
    "ButtonPress").

%-----------------------------------------------------------------------------%

:- pred private_button_release
    `with_type` mouse_handler(delegate_private)
    `with_inst` mouse_handler.
private_button_release(delegate_private(T), B, X, Y, !IO) :-
    button_release(T, B, X, Y, !IO).
:- pragma foreign_export("C", private_button_release(in, in, in, in, di, uo),
    "Saffron_DelegateButtonRelease").
:- pragma foreign_export("Java", private_button_release(in, in, in, in, di, uo),
    "ButtonRelease").

%-----------------------------------------------------------------------------%

:- pred private_mouse_motion
    `with_type` motion_handler(delegate_private)
    `with_inst` motion_handler.
private_mouse_motion(delegate_private(T), X, Y, !IO) :-
    mouse_motion(T, X, Y, !IO).
:- pragma foreign_export("C", private_mouse_motion(in, in, in, di, uo),
    "Saffron_DelegateMouseMotion").
:- pragma foreign_export("Java", private_mouse_motion(in, in, in, di, uo),
    "MouseMotion").

%-----------------------------------------------------------------------------%

:- pred private_resize
    `with_type` resize_handler(delegate_private)
    `with_inst` resize_handler.
private_resize(delegate_private(T), W, H, !IO) :- resize(T, W, H, !IO).
:- pragma foreign_export("C", private_resize(in, in, in, di, uo),
    "Saffron_DelegateResize").
:- pragma foreign_export("Java", private_resize(in, in, in, di, uo), "Resize").

%-----------------------------------------------------------------------------%

:- pred private_quit
    `with_type` quit_handler(delegate_private)
    `with_inst` quit_handler.
private_quit(delegate_private(T), !IO) :- quit(T, !IO).
:- pragma foreign_export("C", private_quit(in, di, uo), "Saffron_DelegateQuit").
:- pragma foreign_export("Java", private_quit(in, di, uo), "Quit").

%-----------------------------------------------------------------------------%

:- pred private_redraw
    `with_type` redraw_handler(delegate_private)
    `with_inst` redraw_handler.
private_redraw(delegate_private(T), !IO) :- redraw(T, !IO).
:- pragma foreign_export("C", private_redraw(in, di, uo),
    "Saffron_DelegateRedraw").
:- pragma foreign_export("Java", private_redraw(in, di, uo), "Redraw").

%-----------------------------------------------------------------------------%

key_nop(_, _, !IO).

button_nop(_, _, _, _, !IO).

motion_nop(_, _, _, !IO).

resize_nop(_, _, _, !IO).

quit_nop(_, !IO).

%-----------------------------------------------------------------------------%

:- pragma inline(handle_event/4).

handle_event(T, saffron_window.quit, !IO) :-
    quit(T, !IO).

handle_event(T, saffron_window.press(Key), !IO) :-
    key_press(T, Key, !IO).

handle_event(T, saffron_window.release(Key), !IO) :-
    key_release(T, Key, !IO).

handle_event(T, saffron_window.press(Button, X, Y), !IO) :-
    button_press(T, Button, X, Y, !IO).

handle_event(T, saffron_window.release(Button, X, Y), !IO) :-
    button_release(T, Button, X, Y, !IO).

handle_event(T, saffron_window.motion(X, Y), !IO) :-
    mouse_motion(T, X, Y, !IO).

handle_event(T, saffron_window.resize(W, H), !IO) :-
    resize(T, W, H, !IO).

%-----------------------------------------------------------------------------%

