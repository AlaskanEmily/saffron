% Copyright (C) 2018-2023 AlaskanEmily, Transnat Games
%
% this software is provided 'as-is', without any express or implied
% warranty.  in no event will the authors be held liable for any damages
% arising from the use of this software.
%
% permission is granted to anyone to use this software for any purpose,
% including commercial applications, and to alter it and redistribute it
% freely, subject to the following restrictions:
%
% 1. the origin of this software must not be misrepresented; you must not
%    claim that you wrote the original software. if you use this software
%    in a product, an acknowledgment in the product documentation would be
%    appreciated but is not required.
% 2. altered source versions must be plainly marked as such, and must not be
%    misrepresented as being the original software.
% 3. this notice may not be removed or altered from any source distribution.

:- module saffron_window.
%=============================================================================%
% Windowing typeclasses. The provider of the rendering context will usually
% also provide the windowing binding.
%
% This is a util rather than core because it isn't necessary from a rendering
% perspective, but is provided because something of this nature will be needed
% to implement the demos, and most games and applications will need to
% implement something resembling this in any case.
%
% In addition to the typeclasses and types, this module exports some helpers
% for constructing these types from foreign code in the modules that implement
% these typeclasses.
%
% See also the (Undocumeted? But it has unit tests in the Mercury compiler...)
% :- pragma foreign_import_module("Lang", <module_name>)
:- interface.
%=============================================================================%

:- use_module io.
:- use_module maybe.

%------------------------------------------------------------------------------%

:- typeclass window(Win) where [
    pred get_window_event(Win, maybe.maybe(window_event), io.io, io.io),
    mode get_window_event(in, out, di, uo) is det,
    
    pred show_window(Win::in, io.io::di, io.io::uo) is det,
    pred hide_window(Win::in, io.io::di, io.io::uo) is det,
    
    pred flip_screen(Win::in, io.io::di, io.io::uo) is det
].

%------------------------------------------------------------------------------%

:- type button --->
    left ;
    right ;
    middle.

%------------------------------------------------------------------------------%

:- pred button_name(button, string).
:- mode button_name(in, out) is det.
:- mode button_name(out, in) is semidet.
:- mode button_name(in, in) is semidet. % Implied.

%------------------------------------------------------------------------------%

:- type mouse --->
    press(button, int, int) ;
    held(button, int, int) ;
    up(int, int).

%------------------------------------------------------------------------------%

:- pred mouse(mouse, int, int).
:- mode mouse(in, out, out) is det.
:- mode mouse(in, in, in) is semidet. % Implied.

%------------------------------------------------------------------------------%

:- type special_key --->
    escape ;
    shift ;
    control ;
    backspace ;
    delete ;
    up_arrow ;
    down_arrow ;
    right_arrow ;
    left_arrow ;
    enter ;
    tab.

%------------------------------------------------------------------------------%

:- type key --->
    key(character) ;
    special_key(special_key).

%------------------------------------------------------------------------------%

:- type window_event --->
    quit ;
    press(key) ;
    release(key) ;
    press(button, int, int) ;
    release(button, int, int) ;
    motion(int, int) ;
    resize(w::int, h::int).

%------------------------------------------------------------------------------%

:- pred special_key(special_key, key).
:- mode special_key(in, out) is det.
:- mode special_key(di, uo) is det.
:- mode special_key(mdi, muo) is det.
:- mode special_key(out, in) is semidet.
:- mode special_key(muo, mdi) is semidet.
:- mode special_key(in, in) is semidet. % Implied.

%==============================================================================%
:- implementation.
%==============================================================================%

:- pragma foreign_code("Java",
    "
static public Window_event_0 CreateMouseEvent(Button_0 button, int x, int y, boolean press){
    if(press)
        return CreateMousePressEvent(button, x, y);
    else
        return CreateMouseReleaseEvent(button, x, y);
}
static public Window_event_0 CreateKeyEvent(int key, boolean press){
    if(press)
        return CreateKeyPressEvent(key);
    else
        return CreateKeyReleaseEvent(key);
}

static public Window_event_0 CreateSpecialKeyEvent(Special_key_0 key, boolean press){
    if(press)
        return CreateSpecialKeyPressEvent(key);
    else
        return CreateSpecialKeyReleaseEvent(key);
}
    ").

%------------------------------------------------------------------------------%

:- pragma foreign_export_enum("C", button/0,
    [prefix("SAFFRON_BUTTON_"), uppercase]).

:- pragma foreign_export_enum("Java", button/0,
    [prefix("SAFFRON_BUTTON_"), uppercase]).

%------------------------------------------------------------------------------%

:- pragma foreign_export_enum("C", special_key/0,
    [prefix("SAFFRON_KEY_"), uppercase]).

:- pragma foreign_export_enum("Java", special_key/0,
    [prefix("SAFFRON_KEY_"), uppercase]).

%------------------------------------------------------------------------------%

button_name(left, "left").
button_name(right, "right").
button_name(middle, "middle").

%------------------------------------------------------------------------------%

mouse(press(_, X, Y), X, Y).
mouse(held(_, X, Y), X, Y).
mouse(up(X, Y), X, Y).

%------------------------------------------------------------------------------%

:- func create_no_event = (maybe.maybe(window_event)::out) is det.
create_no_event = maybe.no.
:- pragma foreign_export("C", create_no_event = (out), "Saffron_CreateNoEvent").
:- pragma foreign_export("Java", create_no_event = (out), "CreateNoEvent").

%------------------------------------------------------------------------------%

:- func create_yes_event(window_event::di) = (maybe.maybe(window_event)::uo) is det.
create_yes_event(E) = maybe.yes(E).
:- pragma foreign_export("C", create_yes_event(di) = (uo), "Saffron_CreateYesEvent").
:- pragma foreign_export("Java", create_yes_event(di) = (uo), "CreateYesEvent").

%------------------------------------------------------------------------------%

:- func create_quit_event = (window_event::uo) is det.
create_quit_event = quit.
:- pragma foreign_export("C", create_quit_event = (uo), "Saffron_CreateQuitEvent").
:- pragma foreign_export("Java", create_quit_event = (uo), "CreateQuitEvent").

%------------------------------------------------------------------------------%

:- func create_press_event(character::di) = (window_event::uo) is det.
create_press_event(C) = press(key(C)).
:- pragma foreign_export("C", create_press_event(di) = (uo), "Saffron_CreateKeyPressEvent").
:- pragma foreign_export("Java", create_press_event(di) = (uo), "CreateKeyPressEvent").

%------------------------------------------------------------------------------%

:- func create_release_event(character::di) = (window_event::uo) is det.
create_release_event(C) = release(key(C)).
:- pragma foreign_export("C", create_release_event(di) = (uo), "Saffron_CreateKeyReleaseEvent").
:- pragma foreign_export("Java", create_release_event(di) = (uo), "CreateKeyReleaseEvent").

%------------------------------------------------------------------------------%

:- func create_special_press_event(special_key::in) = (window_event::uo) is det.
create_special_press_event(C) = press(special_key(CC)) :-
    builtin.copy(C, CC).
:- pragma foreign_export("C", create_special_press_event(in) = (uo), "Saffron_CreateSpecialKeyPressEvent").
:- pragma foreign_export("Java", create_special_press_event(in) = (uo), "CreateSpecialKeyPressEvent").

%------------------------------------------------------------------------------%

:- func create_special_release_event(special_key::in) = (window_event::uo) is det.
create_special_release_event(C) = release(special_key(CC)) :-
    builtin.copy(C, CC).
:- pragma foreign_export("C", create_special_release_event(in) = (uo), "Saffron_CreateSpecialKeyReleaseEvent").
:- pragma foreign_export("Java", create_special_release_event(in) = (uo), "CreateSpecialKeyReleaseEvent").

%------------------------------------------------------------------------------%

:- func create_press_event(button::in, int::in, int::in) = (window_event::uo) is det.
create_press_event(B, X, Y) = press(BC, XC, YC) :-
    builtin.copy(B, BC),
    builtin.copy(X, XC),
    builtin.copy(Y, YC).
:- pragma foreign_export("C", create_press_event(in, in, in) = (uo), "Saffron_CreateMousePressEvent").
:- pragma foreign_export("Java", create_press_event(in, in, in) = (uo), "CreateMousePressEvent").

%------------------------------------------------------------------------------%

:- func create_release_event(button::in, int::in, int::in) = (window_event::uo) is det.
create_release_event(B, X, Y) = release(BC, XC, YC) :-
    builtin.copy(B, BC),
    builtin.copy(X, XC),
    builtin.copy(Y, YC).
:- pragma foreign_export("C", create_release_event(in, in, in) = (uo), "Saffron_CreateMouseReleaseEvent").
:- pragma foreign_export("Java", create_release_event(in, in, in) = (uo), "CreateMouseReleaseEvent").

%------------------------------------------------------------------------------%

:- func create_resize_event(int::in, int::in) = (window_event::uo) is det.
create_resize_event(W, H) = resize(WC, HC) :-
    builtin.copy(W, WC),
    builtin.copy(H, HC).
:- pragma foreign_export("C", create_resize_event(in, in) = (uo), "Saffron_CreateResizeEvent").
:- pragma foreign_export("Java", create_resize_event(in, in) = (uo), "CreateResizeEvent").

%------------------------------------------------------------------------------%

:- func create_motion_event(int::in, int::in) = (window_event::uo) is det.
create_motion_event(X, Y) = motion(XC, YC) :-
    builtin.copy(X, XC),
    builtin.copy(Y, YC).
:- pragma foreign_export("C", create_motion_event(in, in) = (uo), "Saffron_CreateMotionEvent").
:- pragma foreign_export("Java", create_motion_event(in, in) = (uo), "CreateMotionEvent").

%------------------------------------------------------------------------------%

:- func create_key(character::in) = (key::uo) is det.
create_key(C) = key(CC) :- builtin.copy(C, CC).
:- pragma foreign_export("C", create_key(in) = (uo), "Saffron_CreateKey").

%------------------------------------------------------------------------------%

special_key(SK, special_key(SK)).
:- pragma foreign_export("C", special_key(in, out), "Saffron_CreateSpecialKey").
:- pragma foreign_export("Java", special_key(in, out), "CreateSpecialKey").

%------------------------------------------------------------------------------%

