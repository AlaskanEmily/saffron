% Copyright (C) 2018-2022 Transnat Games. All rights reserved.
%
% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.

:- module saffron_glow.

%=============================================================================%
:- interface.
%=============================================================================%

:- import_module char.
:- use_module io.
:- use_module maybe.

:- use_module saffron.
:- use_module saffron.gl.
:- use_module saffron.texture.
:- use_module saffron_window.

%-----------------------------------------------------------------------------%

:- inst io_res_uniq == unique(io.ok(unique) ; io.error(ground)).
:- mode io_res_uo == (free >> io_res_uniq).

%-----------------------------------------------------------------------------%

:- type window.

%-----------------------------------------------------------------------------%

:- type context.

%-----------------------------------------------------------------------------%

:- pred viewport_size(int::in, int::in, int::uo, int::uo) is det.

%-----------------------------------------------------------------------------%
% create_window(Width, Height, Title, WinRes, !IO)
:- pred create_window(int, int, string, io.res(window), io.io, io.io).
:- mode create_window(in, in, in, io_res_uo, di, uo) is det.

%-----------------------------------------------------------------------------%

:- pred show_window(window, io.io, io.io).
:- mode show_window(in, di, uo) is det.

%-----------------------------------------------------------------------------%

:- pred hide_window(window, io.io, io.io).
:- mode hide_window(in, di, uo) is det.

%-----------------------------------------------------------------------------%

:- pred get_window_event(window, maybe.maybe(saffron_window.window_event), io.io, io.io).
:- mode get_window_event(in, uo, di, uo) is det.

%-----------------------------------------------------------------------------%
% create_context(Win, GLMajor, GLMinor, CtxRes, !IO)
:- pred create_context(window, int, int, io.res(context), io.io, io.io).
:- mode create_context(in, in, in, io_res_uo, di, uo) is det.

%-----------------------------------------------------------------------------%

:- pred flip_screen(window, io.io, io.io).
:- mode flip_screen(in, di, uo) is det.

%-----------------------------------------------------------------------------%

:- pred make_current(context, io.io, io.io).
:- mode make_current(in, di, uo) is det.

%-----------------------------------------------------------------------------%

:- pred load_function(
    context,
    string,
    maybe.maybe_error(saffron.ctx_function),
    io.io, io.io).
:- mode load_function(in, in, out, di, uo) is det.

%-----------------------------------------------------------------------------%

:- instance saffron_window.window(window).

%-----------------------------------------------------------------------------%

:- instance saffron.context(context).

%-----------------------------------------------------------------------------%

:- instance saffron.gl.context(context).

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(context, saffron.gl.texture).

%-----------------------------------------------------------------------------%

:- instance saffron.texture.texture(context, saffron.gl.texture).

%=============================================================================%
:- implementation.
%=============================================================================%

:- use_module string.

%-----------------------------------------------------------------------------%

:- pragma foreign_decl("C", "#include ""glow.h"" ").
:- pragma foreign_import_module("C", saffron_window).

%-----------------------------------------------------------------------------%
% Internal binding types.
:- type button ---> left ; right ; middle.

:- pragma foreign_enum("C", button/0, [
    left - "eGlowLeft",
    right - "eGlowRight",
    middle - "eGlowMiddle"
]).

:- pragma foreign_type("C", window, "struct Glow_Window*").
:- pragma foreign_type("C", context, "struct Glow_Context*").

%-----------------------------------------------------------------------------%

:- pred button(saffron_window.button, saffron_glow.button).
:- mode button(in, out) is det.
:- mode button(out, in) is det.
:- mode button(in, in) is semidet. % Implied.

button(saffron_window.left, saffron_glow.left).
button(saffron_window.right, saffron_glow.right).
button(saffron_window.middle, saffron_glow.middle).

:- pragma foreign_export("C", button(out, in), "SaffronGlow_ConvertButton").

%-----------------------------------------------------------------------------%

:- pred string_is_special_key(string::in, saffron_window.special_key::uo) is semidet.

%-----------------------------------------------------------------------------%

:- pragma foreign_proc("C",
    string_is_special_key(String::in, Key::uo),
    [promise_pure, will_not_call_mercury, will_not_throw_exception, thread_safe,
    may_duplicate, does_not_affect_liveness],
    "
    MR_Integer len;
    len = strnlen(String, GLOW_MAX_KEY_NAME_SIZE + 1);
    SUCCESS_INDICATOR = MR_NO;
    
#define SAFFRON_GLOW_SPECIAL_KEY_CASE_EX(GLOW_NAME, SAFFRON_NAME) \\
    if(len == sizeof(GLOW_NAME) - 1 && \\
        memcmp(GLOW_NAME, String, len) == 0){ \\
        SUCCESS_INDICATOR = MR_YES; \\
        Key = SAFFRON_NAME; \\
        break; \\
    }

#define SAFFRON_GLOW_SPECIAL_KEY_CASE(NAME) \
SAFFRON_GLOW_SPECIAL_KEY_CASE_EX(GLOW_ ## NAME, SAFFRON_KEY_ ## NAME)

    /* The do...while is so that the break keyword will work properly. */
    do{
        if(len <= GLOW_MAX_KEY_NAME_SIZE){
            SAFFRON_GLOW_SPECIAL_KEY_CASE(ESCAPE)
            SAFFRON_GLOW_SPECIAL_KEY_CASE(SHIFT)
            SAFFRON_GLOW_SPECIAL_KEY_CASE(CONTROL)
            SAFFRON_GLOW_SPECIAL_KEY_CASE(BACKSPACE)
            SAFFRON_GLOW_SPECIAL_KEY_CASE(UP_ARROW)
            SAFFRON_GLOW_SPECIAL_KEY_CASE(DOWN_ARROW)
            SAFFRON_GLOW_SPECIAL_KEY_CASE(LEFT_ARROW)
            SAFFRON_GLOW_SPECIAL_KEY_CASE(RIGHT_ARROW)
            SAFFRON_GLOW_SPECIAL_KEY_CASE(LEFT_ARROW)
            SAFFRON_GLOW_SPECIAL_KEY_CASE(ENTER)
            SAFFRON_GLOW_SPECIAL_KEY_CASE(TAB)
        }
    }while(0);
#undef SAFFRON_GLOW_SPECIAL_KEY_CASE
#undef SAFFRON_GLOW_SPECIAL_KEY_CASE_EX
    ").

% Re-export.
:- pragma foreign_export("C",
    string_is_special_key(in, uo), "SaffronGlow_StringIsSpecialKey").

%-----------------------------------------------------------------------------%

:- pragma foreign_proc("C",
    viewport_size(WIn::in, HIn::in, WOut::uo, HOut::uo),
    [promise_pure, will_not_call_mercury, will_not_throw_exception, thread_safe,
    may_duplicate, does_not_affect_liveness],
    "
        unsigned w, h;
        Glow_ViewportSize(WIn, HIn, &w, &h);
        WOut = w;
        HOut = h;
    ").

%-----------------------------------------------------------------------------%

:- func create_window_ok(window::di) = (io.res(window)::uo) is det.
create_window_ok(Win) = io.ok(Win).
:- pragma foreign_export("C",
    create_window_ok(di) = (uo),
    "SaffronGlow_CreateWindowOK").

%-----------------------------------------------------------------------------%

:- func create_window_error = (io.res(window)::io_res_uo) is det.
create_window_error = io.error(io.make_io_error("Could not create Glow window")).
:- pragma foreign_export("C",
    create_window_error = (io_res_uo),
    "SaffronGlow_CreateWindowError").

%-----------------------------------------------------------------------------%

:- pragma foreign_proc("C",
    create_window(W::in, H::in, Title::in, WinRes::io_res_uo, IOi::di, IOo::uo),
    [promise_pure, will_not_call_mercury, will_not_throw_exception, thread_safe,
    tabled_for_io, may_duplicate],
    "
        struct Glow_Window *win = MR_GC_malloc_atomic(Glow_WindowStructSize());
        if(Glow_CreateWindow(win, W, H, Title, 0) == 0){
            WinRes = SaffronGlow_CreateWindowOK(win);
        }
        else{
            MR_GC_free(win);
            WinRes = SaffronGlow_CreateWindowError();
        }
        IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- pragma foreign_proc("C",
    show_window(Win::in, IOi::di, IOo::uo),
    [promise_pure, will_not_call_mercury, will_not_throw_exception, thread_safe,
    tabled_for_io, may_duplicate, does_not_affect_liveness],
    "
        IOo = IOi;
        Glow_ShowWindow(Win);
    ").

%-----------------------------------------------------------------------------%

:- pragma foreign_proc("C",
    hide_window(Win::in, IOi::di, IOo::uo),
    [promise_pure, will_not_call_mercury, will_not_throw_exception, thread_safe,
    tabled_for_io, may_duplicate, does_not_affect_liveness],
    "
        IOo = IOi;
        Glow_HideWindow(Win);
    ").

%-----------------------------------------------------------------------------%

:- pragma foreign_proc("C",
    get_window_event(Win::in, Event::uo, IOi::di, IOo::uo),
    [promise_pure, may_call_mercury, will_not_throw_exception, thread_safe,
    tabled_for_io, may_duplicate, does_not_affect_liveness],
    "
        struct Glow_Event event;
        IOo = IOi;
        MR_Word tmp;
        
#ifndef NDEBUG
#define REPORT_INVALID_KEY(FROM) do{ \\
    char buffer[GLOW_MAX_KEY_NAME_SIZE + 1]; \\
    strncpy(buffer, FROM, GLOW_MAX_KEY_NAME_SIZE); \\
    buffer[GLOW_MAX_KEY_NAME_SIZE] = 0; \\
    fprintf(stderr, \\
        ""[saffron_glow] Warning: unknown key %s\\n"", \\
        buffer); \\
}while(0)
#else
#define REPORT_INVALID_KEY (void)0
#endif
        if(Glow_GetEvent(Win, &event)){
            switch(event.type){
                case eGlowQuit:
                    Event = Saffron_CreateYesEvent(Saffron_CreateQuitEvent());
                    break;
                case eGlowKeyboardPressed:
                    if(SaffronGlow_StringIsSpecialKey(event.value.key, &tmp)){
                        Event = Saffron_CreateYesEvent(Saffron_CreateSpecialKeyPressEvent(tmp));
                    }
                    else{
                        int width;
                        MR_Char c = MR_utf8_get_mb(event.value.key, 0, &width);
                        if(width <= 0){
                            /* TODO: We should fetch another event. */
                            REPORT_INVALID_KEY(event.value.key);
                            Event = Saffron_CreateNoEvent();
                        }
                        else{
                             Event = Saffron_CreateYesEvent(Saffron_CreateKeyPressEvent(c));
                        }
                    }
                    break;
                case eGlowKeyboardReleased:
                    if(SaffronGlow_StringIsSpecialKey(event.value.key, &tmp)){
                        Event = Saffron_CreateYesEvent(Saffron_CreateSpecialKeyReleaseEvent(tmp));
                    }
                    else{
                        int width;
                        MR_Char c = MR_utf8_get_mb(event.value.key, 0, &width);
                        if(width <= 0){
                            /* TODO: We should fetch another event. */
                            REPORT_INVALID_KEY(event.value.key);
                            Event = Saffron_CreateYesEvent(Saffron_CreateNoEvent());
                        }
                        else{
                             Event = Saffron_CreateYesEvent(Saffron_CreateKeyReleaseEvent(c));
                        }
                    }
                    break;
                case eGlowMousePressed:
                    SaffronGlow_ConvertButton(&tmp, event.value.mouse.button);
                    Event = Saffron_CreateYesEvent(Saffron_CreateMousePressEvent(
                        tmp,
                        event.value.mouse.xy[0],
                        event.value.mouse.xy[1]));
                    break;
                case eGlowMouseReleased:
                    SaffronGlow_ConvertButton(&tmp, event.value.mouse.button);
                    Event = Saffron_CreateYesEvent(Saffron_CreateMouseReleaseEvent(
                        tmp,
                        event.value.mouse.xy[0],
                        event.value.mouse.xy[1]));
                    break;
                case eGlowMouseMoved:
                    Event = Saffron_CreateYesEvent(Saffron_CreateMotionEvent(
                        event.value.mouse.xy[0],
                        event.value.mouse.xy[1]));
                    break;
                case eGlowResized:
                    Event = Saffron_CreateYesEvent(Saffron_CreateResizeEvent(
                        event.value.resize[0],
                        event.value.resize[1]));
                    break;
                default:
                    MR_assert(0);
            }
        }
        else{
            Event = Saffron_CreateNoEvent();
        }
    ").

%-----------------------------------------------------------------------------%

:- func create_context_ok(context::di) = (io.res(context)::uo) is det.
create_context_ok(Ctx) = io.ok(Ctx).
:- pragma foreign_export("C",
    create_context_ok(di) = (uo),
    "SaffronGlow_CreateContextOK").

%-----------------------------------------------------------------------------%

:- func create_context_error = (io.res(context)::io_res_uo) is det.
create_context_error = io.error(io.make_io_error("Could not create Glow context")).
:- pragma foreign_export("C",
    create_context_error = (io_res_uo),
    "SaffronGlow_CreateContextError").

%-----------------------------------------------------------------------------%

:- pragma foreign_proc("C",
    create_context(Win::in, Major::in, Minor::in, CtxRes::io_res_uo, IOi::di, IOo::uo),
    [promise_pure, will_not_call_mercury, will_not_throw_exception, thread_safe,
    tabled_for_io, may_duplicate],
    "
        struct Glow_Context *ctx =
            MR_GC_malloc_atomic(Glow_ContextStructSize());
        if(Glow_CreateContext(Win, NULL, Major, Minor, ctx) == 0){
            CtxRes = SaffronGlow_CreateContextOK(ctx);
        }
        else{
            MR_GC_free(ctx);
            CtxRes = SaffronGlow_CreateContextError();
        }

        IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- pragma foreign_proc("C",
    flip_screen(Win::in, IOi::di, IOo::uo),
    [promise_pure, will_not_call_mercury, will_not_throw_exception, thread_safe,
    tabled_for_io, may_duplicate, does_not_affect_liveness],
    "
        IOo = IOi;
        Glow_FlipScreen(Win);
    ").

%-----------------------------------------------------------------------------%

:- pragma foreign_proc("C",
    make_current(Ctx::in, IOi::di, IOo::uo),
    [promise_pure, will_not_call_mercury, will_not_throw_exception, thread_safe,
    tabled_for_io, may_duplicate, does_not_affect_liveness],
    "
        IOo = IOi;
        Glow_MakeCurrent(Ctx);
    ").

%-----------------------------------------------------------------------------%

:- pragma foreign_proc("C",
    load_function(Ctx::in, Name::in, MaybeFunc::out, IOi::di, IOo::uo),
    [promise_pure, may_call_mercury, will_not_throw_exception, thread_safe,
    may_duplicate, does_not_affect_liveness],
    "
        void *proc_addr = Glow_GetProcAddress(Name);
        (void)Ctx;
        if(proc_addr == NULL){
            MaybeFunc = Saffron_CreateCtxFunctionError(
                (char*)""Function not found"");
        }
        else{
            MR_Word w;
            memcpy(&w, &proc_addr, sizeof(MR_Word));
            MaybeFunc = Saffron_CreateCtxFunctionOK(w);
        }
        IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- instance saffron_window.window(window) where [
    pred(saffron_window.get_window_event/4) is saffron_glow.get_window_event,
    pred(saffron_window.show_window/3) is saffron_glow.show_window,
    pred(saffron_window.hide_window/3) is saffron_glow.hide_window,
    pred(saffron_window.flip_screen/3) is saffron_glow.flip_screen
].

%-----------------------------------------------------------------------------%

:- instance saffron.context(context) where [
    pred(saffron.make_current/3) is saffron_glow.make_current,
    pred(saffron.load_function/5) is saffron_glow.load_function
].

%-----------------------------------------------------------------------------%

:- instance saffron.gl.context(context) where [].

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(context, saffron.gl.texture) where [
    pred(saffron.destroy/4) is saffron.gl.destroy_texture
].

%-----------------------------------------------------------------------------%

:- instance saffron.texture.texture(context, saffron.gl.texture) where [
    pred(saffron.texture.create_empty/6) is saffron.gl.create_empty_texture,
    pred(saffron.texture.create_from_bitmap/7) is saffron.gl.create_texture_from_bitmap,
    pred(saffron.texture.upload_at/9) is saffron.gl.texture_upload_at
].

