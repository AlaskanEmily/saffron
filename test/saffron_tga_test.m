% Released as public domain by Transnat Games for testing purposes.
%
% Any copyright is dedicated to the Public Domain.
% https://creativecommons.org/publicdomain/zero/1.0/

:- module saffron_tga_test.

%=============================================================================%
:- interface.
%=============================================================================%

:- use_module io.

%-----------------------------------------------------------------------------%

:- pred main(io.io::di, io.io::uo) is det.

%=============================================================================%
:- implementation.
%=============================================================================%

:- use_module bitmap.
:- import_module int.
:- use_module maybe.
:- use_module pair.
:- use_module string.

:- use_module transunit.
:- use_module transunit.compare.

:- import_module saffron_tga.

%-----------------------------------------------------------------------------%

:- pred run_test(string, int, int, maybe.maybe_error, io.io, io.io).
:- mode run_test(in, in, in, out, di, uo) is det.

run_test(Name, RefW, RefH, Result, !IO) :-
    TestPath = string.append(Name, ".tga"),
    RefPath = string.append(Name, ".raw"),
    
    % Open the test image
    saffron_tga.load_path(TestPath, TestResult, !IO),
    (
        TestResult = io.error(IOErr),
        Result = maybe.error(io.error_message(IOErr))
    ;
        TestResult = io.ok({TestBMP, TestW, TestH}),
        TestDim = pair.pair(TestW, TestH),
        transunit.compare(TestDim, pair.pair(RefW, RefH)) = DimResult,
        % Validate the W/H
        ( if
            DimResult = maybe.ok
        then
            % Open the reference case.
            io.open_binary_input(RefPath, RefStreamResult, !IO),
            ( if
                RefStreamResult = io.ok(RefStream)
            then
                NBytes = TestW * TestH * 4,
                io.read_bitmap(RefStream, bitmap.init(NBytes * 8), RefBMP, N, Res, !IO),
                ( if
                    N = NBytes, Res = io.ok
                then
                    Result = transunit.compare(TestBMP, RefBMP)
                else
                    Result = maybe.error("Invalid reference file")
                ),
                io.close_binary_input(RefStream, !IO)
            else
                Result = maybe.error("Cound not open reference file")
            )
        else
            Result = DimResult
        )
    ).

%-----------------------------------------------------------------------------%

main(!IO) :-
    transunit.run_result_test(run_test("crate", 32, 32), "crate", !IO),
    transunit.run_result_test(run_test("ctc24", 128, 128), "ctc24", !IO),
    transunit.run_result_test(run_test("utc16", 128, 128), "utc16", !IO),
    transunit.run_result_test(run_test("utc24", 128, 128), "utc24", !IO),
    transunit.run_result_test(run_test("utc32", 128, 128), "utc32", !IO).

