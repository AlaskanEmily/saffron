% Copyright (c) 2021-2023 Transnat Games, AlaskanEmily
%
% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.

:- module saffron.draw.
%=============================================================================%
% Drawing typeclasses and predicates.
:- interface.
%=============================================================================%

:- use_module array.
:- import_module list.
:- import_module pair.
:- use_module thread.
:- use_module thread.mvar.

:- use_module mmath.
:- use_module mmath.vector.
:- use_module mmath.matrix.

%-----------------------------------------------------------------------------%

:- typeclass color_buffer(Ctx, Buffer)
    <= destroy(Ctx, Buffer) where [
    
    pred create_color_buffer_list(Ctx, list.list(color), Buffer, io.io, io.io),
    mode create_color_buffer_list(in, in, out, di, uo) is det,
    
    pred create_color_buffer_array(Ctx, array.array(color), Buffer, io.io, io.io),
    mode create_color_buffer_array(in, in, out, di, uo) is det
].

%-----------------------------------------------------------------------------%

:- typeclass basic_transform(Ctx) where [
    pred set_matrix(Ctx::in, mmath.matrix.matrix::in, io.io::di, io.io::uo) is det
].

%-----------------------------------------------------------------------------%
% Basic functions of drawing transformations.
% It will often be handled fully in software, where only set_matrix is actually
% used, but for GL2 we can just implement this with the GL2 calls.
:- typeclass transform_stack(Ctx) where [
    pred transform(Ctx::in, mmath.matrix.matrix::in, io.io::di, io.io::uo) is det,
    pred identity(Ctx::in, io.io::di, io.io::uo) is det,
    pred push_matrix(Ctx::in, io.io::di, io.io::uo) is det,
    % Should throw exception if the matrix stack is empty.
    pred pop_matrix(Ctx::in, io.io::di, io.io::uo) is det
].

%-----------------------------------------------------------------------------%

:- typeclass transform(Ctx)
    <= (basic_transform(Ctx), transform_stack(Ctx)) where [].

%-----------------------------------------------------------------------------%
% Use a type here just to make it structurally required that there is always
% one matrix.
:- type transform_state ---> transform_state(
    mmath.matrix.matrix,
    stack::list.list(mmath.matrix.matrix)).

%-----------------------------------------------------------------------------%
% Used to implement transform with a stack of matrices and basic_transform.
:- type transform_context(Ctx) ---> transform_context(
    transform_state::thread.mvar.mvar(transform_state),
    Ctx).

%-----------------------------------------------------------------------------%

:- func top_matrix(transform_state) = mmath.matrix.matrix.

%-----------------------------------------------------------------------------%

:- func 'top_matrix :='(transform_state, mmath.matrix.matrix) = transform_state.

%-----------------------------------------------------------------------------%

:- instance basic_transform(transform_context(Ctx)).

%-----------------------------------------------------------------------------%

:- instance transform_stack(transform_context(Ctx)).

%-----------------------------------------------------------------------------%

:- typeclass group(Ctx, Group, Shape)
    <= (draw(Ctx, Group), destroy(Ctx, Shape), (Shape -> Group)) where [
    
    pred create_group_list(Ctx, list.list(Shape), Group, io.io, io.io),
    mode create_group_list(in, in, out, di, uo) is det,
    
    pred create_group_array(Ctx, array.array(Shape), Group, io.io, io.io),
    mode create_group_array(in, in, out, di, uo) is det
].

%-----------------------------------------------------------------------------%
% Note that while some backends, such as GL4, do require shaders for all Groups
% they must provide a default too.
:- typeclass group(Ctx, Group, Shape, Shader) 
    <= (group(Ctx, Group, Shape), destroy(Ctx, Shader)) where [
    
    pred create_group_list(Ctx, Shader, list.list(Shape), Group, io.io, io.io),
    mode create_group_list(in, in, in, out, di, uo) is det,
    
    pred create_group_array(Ctx, Shader, array.array(Shape), Group, io.io, io.io),
    mode create_group_array(in, in, in, out, di, uo) is det
].

%-----------------------------------------------------------------------------%

:- typeclass draw(Ctx, Group)
    <= destroy(Ctx, Group) where [
    
    pred draw(Ctx::in, Group::in, io.io::di, io.io::uo) is det
].

%-----------------------------------------------------------------------------%

:- typeclass draw(Ctx, Group, Buffer)
    <= (draw(Ctx, Group), color_buffer(Ctx, Buffer), (Ctx -> Buffer)) where [
    
    pred draw_color_mask(Ctx, Group, list.list(Buffer), io.io, io.io),
    mode draw_color_mask(in, in, in, di, uo) is det
].

%-----------------------------------------------------------------------------%

:- pred draw_color_mask(Ctx, pair(Group, list.list(Buffer)), io.io, io.io)
    <= draw(Ctx, Group, Buffer).
:- mode draw_color_mask(in, in, di, uo) is det.

%-----------------------------------------------------------------------------%

:- pred raise_matrix_stack_error is erroneous.

%-----------------------------------------------------------------------------%
% TODO: shaders
% - color masking
% - per-draw color attributes
% - fragment effects?
%-----------------------------------------------------------------------------%

%=============================================================================%
:- implementation.
%=============================================================================%

:- use_module exception.

%-----------------------------------------------------------------------------%

top_matrix(transform_state(Matrix, [])) = Matrix.
top_matrix(transform_state(_, [Matrix|_])) = Matrix.

%-----------------------------------------------------------------------------%

'top_matrix :='(transform_state(_, []), Matrix) = transform_state(Matrix, []).
'top_matrix :='(transform_state(Base, [_|Tail]), Matrix) =
    transform_state(Base, [Matrix|Tail]).

%-----------------------------------------------------------------------------%

:- instance basic_transform(transform_context(Ctx)) where [
    (set_matrix(Ctx, Matrix, !IO) :-
        MVar = Ctx ^ transform_state,
        thread.mvar.take(MVar, State, !IO),
        thread.mvar.put(MVar, State ^ top_matrix := Matrix, !IO) )
].

%-----------------------------------------------------------------------------%

:- instance transform_stack(transform_context(Ctx)) where [
    
    (transform(Ctx, Matrix, !IO) :-
        MVar = Ctx ^ transform_state,
        thread.mvar.take(MVar, State, !IO),
        NewMatrix = mmath.matrix.multiply(State ^ top_matrix, Matrix),
        thread.mvar.put(MVar, State ^ top_matrix := NewMatrix, !IO) ),
    
    (identity(Ctx, !IO) :-
        MVar = Ctx ^ transform_state,
        thread.mvar.take(MVar, State, !IO),
        thread.mvar.put(MVar, State ^ top_matrix := mmath.matrix.identity, !IO) ),
    
    (push_matrix(Ctx, !IO) :-
        MVar = Ctx ^ transform_state,
        thread.mvar.take(MVar, State, !IO),
        NewStack = [State ^ top_matrix|State ^ stack],
        thread.mvar.put(MVar, State ^ stack := NewStack, !IO) ),
    
    % Should throw exception if the matrix stack is empty.
    (pop_matrix(Ctx, !IO) :-
        MVar = Ctx ^ transform_state,
        thread.mvar.take(MVar, State, !IO),
        ( if
            State ^ stack = [_|Tail]
        then
            thread.mvar.put(MVar, State ^ stack := Tail, !IO)
        else
            % Fix the transform state.
            % Not strictly necessary, but in theory a caller mighty want to
            % catch this exception and try to continue. It's certainly not
            % truly fatal.
            thread.mvar.try_put(Ctx ^ transform_state, State, _, !IO),
            raise_matrix_stack_error
        ) )
].

%-----------------------------------------------------------------------------%

draw_color_mask(Ctx, (Group - Buffers), !IO) :-
    draw_color_mask(Ctx, Group, Buffers, !IO).

%-----------------------------------------------------------------------------%

raise_matrix_stack_error :-
    exception.throw(exception.software_error(
        "Tried to pop from an empty transform stack.")).

