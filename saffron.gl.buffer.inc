#ifndef SAFFRON_GL_FUNC_VAR
#define SAFFRON_GL_FUNC_VAR(X) SaffronGL_ ## X ## _PROC X;
#endif
#ifndef SAFFRON_GL_FUNC_LOAD
#define SAFFRON_GL_FUNC_LOAD(X, EXT) \
    SaffronGL_LoadFunction(gl_ctx, (char*)("gl" #X EXT), &maybe_func); \
    if(!Saffron_GetCtxFunctionOK(maybe_func, &func)) break; \
    (ctx->X) = (SaffronGL_ ## X ## _PROC)func; 

#define SAFFRON_GL_LOAD_FUNC_CORE(X) SAFFRON_GL_FUNC_LOAD(X, "")
#endif

#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_GenBuffers_PROC)(
    GLsizei,
    GLuint*);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_DeleteBuffers_PROC)(
    GLsizei,
    const GLuint*);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_BindBuffer_PROC)(
    GLenum,
    GLuint);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_BufferData_PROC)(
    GLenum,
    SaffronGLsizeiptr,
    const void*,
    GLenum);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void*(APIENTRY*SaffronGL_MapBuffer_PROC)(
    GLenum,
    GLenum);
#endif
#ifndef SAFFRON_GL_IMPL
typedef GLboolean(APIENTRY*SaffronGL_UnmapBuffer_PROC)(
    GLenum);
#endif
#ifndef SAFFRON_GL_IMPL
#define SAFFRON_GL_BUFFER_FUNCS(X) \
    X(GenBuffers) \
    X(DeleteBuffers) \
    X(BindBuffer) \
    X(BufferData) \
    X(MapBuffer) \
    X(UnmapBuffer) \
/* */
struct SaffronGL_BufferCtx{
    SAFFRON_GL_BUFFER_FUNCS(SAFFRON_GL_FUNC_VAR)
};
#endif
#ifndef SAFFRON_GL_IMPL
#ifdef SAFFRON_GL_EXPECT_BUFFER
#define SAFFRON_GL_BUFFER_FUNC(CTX, X) (gl ## X)
#define SAFFRON_GL_BUFFER_CTX MR_Word
#define SAFFRON_GL_BUFFER_CTX_FIELD(NAME)
#define SAFFRON_GL_ALLOC_BUFFER_CTX() 0
#define SAFFRON_GL_LOAD_BUFFER_CTX(GLCTX, CTX) 0
#else
struct SaffronGL_BufferCtx;
int SaffronGL_LoadBufferCtx(MR_Word gl_ctx, struct SaffronGL_BufferCtx *ctx);
#define SAFFRON_GL_BUFFER_FUNC(CTX, X) ((CTX)->X)
#define SAFFRON_GL_BUFFER_CTX struct SaffronGL_BufferCtx*
#define SAFFRON_GL_BUFFER_CTX_FIELD(NAME) struct SaffronGL_BufferCtx NAME;
#define SAFFRON_GL_ALLOC_BUFFER_CTX() \
    MR_GC_malloc_atomic(sizeof(struct SaffronGL_BufferCtx))
#define SAFFRON_GL_LOAD_BUFFER_CTX(GLCTX, CTX) SaffronGL_LoadBufferCtx((GLCTX), (CTX))
#endif
#else
#ifndef SAFFRON_GL_EXPECT_BUFFER
int SaffronGL_LoadBufferCtx(MR_Word gl_ctx, struct SaffronGL_BufferCtx *ctx){
    MR_Word maybe_func, func;
    MR_Bool have_ext;
    MR_String err = NULL;
    MR_Integer major, minor;
    SaffronGL_Version(gl_ctx, &major, &minor);
    if(major > 2 || (major == 2 && minor >= 0)) do{
        SAFFRON_GL_BUFFER_FUNCS(SAFFRON_GL_LOAD_FUNC_CORE)
        return 0;
    }while(0);
#define SAFFRON_GL_LOAD_FUNC_BUFFER_ARB(X) SAFFRON_GL_FUNC_LOAD(X, "ARB")
    SaffronGL_SupportsExtension(gl_ctx, (MR_String)"GL_ARB_vertex_buffer_object", &have_ext);
    if(have_ext){
        do{
            SAFFRON_GL_BUFFER_FUNCS(SAFFRON_GL_LOAD_FUNC_BUFFER_ARB)
            return 0;
        }while(0);
    }
    return 1;
}
#endif
#endif
#ifndef GL_ARRAY_BUFFER
#define GL_ARRAY_BUFFER 0x8892
#endif
#ifndef GL_ELEMENT_ARRAY_BUFFER
#define GL_ELEMENT_ARRAY_BUFFER 0x8893
#endif
#ifndef GL_STREAM_DRAW
#define GL_STREAM_DRAW 0x88E0
#endif
#ifndef GL_STREAM_READ
#define GL_STREAM_READ 0x88E1
#endif
#ifndef GL_STREAM_COPY
#define GL_STREAM_COPY 0x88E2
#endif
#ifndef GL_STATIC_DRAW
#define GL_STATIC_DRAW 0x88E4
#endif
#ifndef GL_STATIC_READ
#define GL_STATIC_READ 0x88E5
#endif
#ifndef GL_STATIC_COPY
#define GL_STATIC_COPY 0x88E6
#endif
#ifndef GL_DYNAMIC_DRAW
#define GL_DYNAMIC_DRAW 0x88E8
#endif
#ifndef GL_DYNAMIC_READ
#define GL_DYNAMIC_READ 0x88E9
#endif
#ifndef GL_DYNAMIC_COPY
#define GL_DYNAMIC_COPY 0x88EA
#endif
