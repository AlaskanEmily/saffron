BEGIN{
    print("\
#ifndef SAFFRON_GL_FUNC_VAR\n\
#define SAFFRON_GL_FUNC_VAR(X) SaffronGL_ ## X ## _PROC X;\n\
#endif\n\
#ifndef SAFFRON_GL_FUNC_LOAD\n\
#define SAFFRON_GL_FUNC_LOAD(X, EXT) \\\n\
    SaffronGL_LoadFunction(gl_ctx, (char*)(\"gl\" #X EXT), &maybe_func); \\\n\
    if(!Saffron_GetCtxFunctionOK(maybe_func, &func)) break; \\\n\
    (ctx->X) = (SaffronGL_ ## X ## _PROC)func; \n\
\n\
#define SAFFRON_GL_LOAD_FUNC_CORE(X) SAFFRON_GL_FUNC_LOAD(X, \"\")\n\
#endif\n\
");

}

$1 == "const" {
    print("\
#ifndef " $2 "\n\
#define " $2 " " $3 "\n\
#endif");
    next;
}

$1 !~ /#/ && $0 != "" {
    print("#ifndef SAFFRON_GL_IMPL");
    upper2 = toupper($2);
}

$1 == "struct" || $1 == "load"{
    struct = "struct SaffronGL_" $2 "Ctx";
}

$1 == "func" {
    if($NF ~ /^=/){
        last = NF - 1;
        ret = substr($NF, 2);
    }
    else{
        last = NF;
        ret = "void";
    }
    sig = "typedef " ret "(APIENTRY*SaffronGL_" $2 "_PROC)(";
    if(last < 3){
        print(sig "void);");
    }
    else{
        print(sig);
        for(i = 3; i < last; i++)
            print("    " $i ",");
        print("    " $last ");");
    }
}

$1 == "struct" {
    print("#define SAFFRON_GL_" upper2 "_FUNCS(X) \\");
    for(i = 3; i <= NF; i++){
        print("    X(" $i ") \\");
    }
    print("/* */");
    print(struct "{");
    print("    SAFFRON_GL_" upper2 "_FUNCS(SAFFRON_GL_FUNC_VAR)");
    print("};");
}

$1 == "load" {
    # Check for "core" spec, and if we find it we should generate an 'expect'
    # C preprocessor check to avoid unnecessary indirection.
    core = 0;
    for(i = 3; i <= NF; i++){
        if($i ~ /^core [0-9]\.[0-9]$/){
            core = i;
            break;
        }
    }
    if(core != 0){
        print("\
#ifdef SAFFRON_GL_EXPECT_" upper2 "\n\
#define SAFFRON_GL_" upper2 "_FUNC(CTX, X) (gl ## X)\n\
#define SAFFRON_GL_" upper2 "_CTX MR_Word\n\
#define SAFFRON_GL_" upper2 "_CTX_FIELD(NAME)\n\
#define SAFFRON_GL_ALLOC_" upper2 "_CTX() 0\n\
#define SAFFRON_GL_LOAD_" upper2 "_CTX(GLCTX, CTX) 0\n\
#else");
    }
    # Write the default, loader implementation.
    sig = "int SaffronGL_Load" $2 "Ctx(MR_Word gl_ctx, " struct " *ctx)";
    print(struct ";\n\
" sig ";\n\
#define SAFFRON_GL_" upper2 "_FUNC(CTX, X) ((CTX)->X)\n\
#define SAFFRON_GL_" upper2 "_CTX " struct "*\n\
#define SAFFRON_GL_" upper2 "_CTX_FIELD(NAME) " struct " NAME;\n\
#define SAFFRON_GL_ALLOC_" upper2 "_CTX() \\\n    MR_GC_malloc_atomic(sizeof(" struct "))\n\
#define SAFFRON_GL_LOAD_" upper2 "_CTX(GLCTX, CTX) SaffronGL_Load" $2 "Ctx((GLCTX), (CTX))");
    if(core != 0){
        print("#endif");
    }
    
    # Write the impl (we are in an #ifdef for the interface)
    print("#else");
    if(core != 0){
        print("#ifndef SAFFRON_GL_EXPECT_" upper2);
    }

    # Write the loader function
    print(sig "{\n\
    MR_Word maybe_func, func;\n\
    MR_Bool have_ext;\n\
    MR_String err = NULL;");
    if(core != 0){
        print("\
    MR_Integer major, minor;\n\
    SaffronGL_Version(gl_ctx, &major, &minor);");
    }
    for(i = 3; i <= NF; i++){
        # Core has special handling.
        if($i ~ /^core [0-9]\.[0-9]$/){
            major = substr($i, 6, 1);
            minor = substr($i, 8, 1);
            print("\
    if(major > " major " || (major == " major " && minor >= " minor ")) do{\n\
        SAFFRON_GL_" upper2 "_FUNCS(SAFFRON_GL_LOAD_FUNC_CORE)\n\
        return 0;\n\
    }while(0);");
        }
        else if($i ~ /^ext GL_[A-Za-z0-9_]+ [A-Za-z0-9_]+$/){
            ext_i = match($i, / GL_/) + 1;
            sfx_i = match($i, / [A-Za-z0-9_]+$/) + 1;
            ext = substr($i, ext_i, sfx_i - ext_i - 1);
            sfx = substr($i, sfx_i);
            # Some extension types only make sense on certain OSes.
            cpp_if = "";
            
            if(sfx == "_"){
                sfx = ""; # Note for empty suffix
            }
            else if(sfx == "APPLE"){
                cpp_if = "(defined __APPLE__)";
            }
            else if(sfx == "NV"){
                cpp_if = "!(defined __APPLE__) || (defined __ppc__)";
            }
            else if(sfx == "MESA"){
                cpp_if = "!(defined __APPLE__) && !(defined MR_WIN32)";
            }
            
            if(cpp_if != ""){
                print("#if " cpp_if);
            }
            if(sfx == "none"){
                macro = "SAFFRON_GL_LOAD_FUNC_CORE";
            }
            else{
                macro = "SAFFRON_GL_LOAD_FUNC_" upper2 "_" sfx;
                print("\
#define " macro "(X) SAFFRON_GL_FUNC_LOAD(X, \"" sfx "\")");
            }
            print("\
    SaffronGL_SupportsExtension(gl_ctx, (MR_String)\"" ext "\", &have_ext);\n\
    if(have_ext){\n\
        do{\n\
            SAFFRON_GL_" upper2 "_FUNCS(" macro ")\n\
            return 0;\n\
        }while(0);\n\
    }");
            if(cpp_if != ""){
               print("#endif /* " cpp_if " */");
            }
        }
    }
    print("    return 1;\n}");
    if(core != 0){
        print("#endif"); # SAFFRON_GL_EXPECT_
    }
}

$1 !~ /#/ && $0 != "" {
    print("#endif");
}

