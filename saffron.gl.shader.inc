#ifndef SAFFRON_GL_FUNC_VAR
#define SAFFRON_GL_FUNC_VAR(X) SaffronGL_ ## X ## _PROC X;
#endif
#ifndef SAFFRON_GL_FUNC_LOAD
#define SAFFRON_GL_FUNC_LOAD(X, EXT) \
    SaffronGL_LoadFunction(gl_ctx, (char*)("gl" #X EXT), &maybe_func); \
    if(!Saffron_GetCtxFunctionOK(maybe_func, &func)) break; \
    (ctx->X) = (SaffronGL_ ## X ## _PROC)func; 

#define SAFFRON_GL_LOAD_FUNC_CORE(X) SAFFRON_GL_FUNC_LOAD(X, "")
#endif

#ifndef SAFFRON_GL_IMPL
typedef GLuint(APIENTRY*SaffronGL_CreateShader_PROC)(
    GLenum);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_DeleteShader_PROC)(
    GLuint);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_ShaderSource_PROC)(
    GLuint,
    GLsizei,
    const SaffronGLchar**,
    const GLint*);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_CompileShader_PROC)(
    GLuint);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_GetShaderiv_PROC)(
    GLuint,
    GLenum,
    GLint*);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_GetShaderInfoLog_PROC)(
    GLuint,
    GLsizei,
    GLsizei*,
    SaffronGLchar*);
#endif
#ifndef SAFFRON_GL_IMPL
typedef GLuint(APIENTRY*SaffronGL_CreateProgram_PROC)(
    void);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_DeleteProgram_PROC)(
    GLuint);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_LinkProgram_PROC)(
    GLuint);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_AttachShader_PROC)(
    GLuint,
    GLuint);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_UseProgram_PROC)(
    GLuint);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_GetProgramiv_PROC)(
    GLuint,
    GLenum,
    GLint*);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_GetProgramInfoLog_PROC)(
    GLuint,
    GLsizei,
    GLsizei*,
    SaffronGLchar*);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_EnableVertexAttribArray_PROC)(
    GLuint);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_DisableVertexAttribArray_PROC)(
    GLuint);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_BindAttribLocation_PROC)(
    GLuint,
    GLuint,
    const SaffronGLchar*);
#endif
#ifndef SAFFRON_GL_IMPL
typedef GLint(APIENTRY*SaffronGL_GetAttribLocation_PROC)(
    GLuint,
    const SaffronGLchar*);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_VertexAttribPointer_PROC)(
    GLuint,
    GLint,
    GLenum,
    GLboolean,
    GLsizei,
    const void*);
#endif
#ifndef SAFFRON_GL_IMPL
typedef GLint(APIENTRY*SaffronGL_GetUniformLocation_PROC)(
    GLuint,
    const SaffronGLchar*);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_Uniform1f_PROC)(
    GLint,
    GLfloat);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_Uniform2f_PROC)(
    GLint,
    GLfloat,
    GLfloat);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_Uniform3f_PROC)(
    GLint,
    GLfloat,
    GLfloat,
    GLfloat);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_Uniform4f_PROC)(
    GLint,
    GLfloat,
    GLfloat,
    GLfloat,
    GLfloat);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_Uniform1fv_PROC)(
    GLint,
    const GLfloat*);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_Uniform2fv_PROC)(
    GLint,
    const GLfloat*);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_Uniform3fv_PROC)(
    GLint,
    const GLfloat*);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_Uniform4fv_PROC)(
    GLint,
    const GLfloat*);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_UniformMatrix2fv_PROC)(
    GLint,
    const GLfloat*);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_UniformMatrix3fv_PROC)(
    GLint,
    const GLfloat*);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_UniformMatrix4fv_PROC)(
    GLint,
    const GLfloat*);
#endif
#ifndef SAFFRON_GL_IMPL
#define SAFFRON_GL_SHADERCORE_FUNCS(X) \
    X(CreateShader) \
    X(DeleteShader) \
    X(ShaderSource) \
    X(CompileShader) \
    X(GetShaderiv) \
    X(GetShaderInfoLog) \
    X(CreateProgram) \
    X(DeleteProgram) \
    X(LinkProgram) \
    X(AttachShader) \
    X(UseProgram) \
    X(GetProgramiv) \
    X(GetProgramInfoLog) \
    X(EnableVertexAttribArray) \
    X(DisableVertexAttribArray) \
    X(BindAttribLocation) \
    X(GetAttribLocation) \
    X(VertexAttribPointer) \
    X(GetUniformLocation) \
    X(Uniform1f) \
    X(Uniform2f) \
    X(Uniform3f) \
    X(Uniform4f) \
    X(Uniform1fv) \
    X(Uniform2fv) \
    X(Uniform3fv) \
    X(Uniform4fv) \
    X(UniformMatrix2fv) \
    X(UniformMatrix3fv) \
    X(UniformMatrix4fv) \
/* */
struct SaffronGL_ShaderCoreCtx{
    SAFFRON_GL_SHADERCORE_FUNCS(SAFFRON_GL_FUNC_VAR)
};
#endif
#ifndef SAFFRON_GL_IMPL
#ifdef SAFFRON_GL_EXPECT_SHADERCORE
#define SAFFRON_GL_SHADERCORE_FUNC(CTX, X) (gl ## X)
#define SAFFRON_GL_SHADERCORE_CTX MR_Word
#define SAFFRON_GL_SHADERCORE_CTX_FIELD(NAME)
#define SAFFRON_GL_ALLOC_SHADERCORE_CTX() 0
#define SAFFRON_GL_LOAD_SHADERCORE_CTX(GLCTX, CTX) 0
#else
struct SaffronGL_ShaderCoreCtx;
int SaffronGL_LoadShaderCoreCtx(MR_Word gl_ctx, struct SaffronGL_ShaderCoreCtx *ctx);
#define SAFFRON_GL_SHADERCORE_FUNC(CTX, X) ((CTX)->X)
#define SAFFRON_GL_SHADERCORE_CTX struct SaffronGL_ShaderCoreCtx*
#define SAFFRON_GL_SHADERCORE_CTX_FIELD(NAME) struct SaffronGL_ShaderCoreCtx NAME;
#define SAFFRON_GL_ALLOC_SHADERCORE_CTX() \
    MR_GC_malloc_atomic(sizeof(struct SaffronGL_ShaderCoreCtx))
#define SAFFRON_GL_LOAD_SHADERCORE_CTX(GLCTX, CTX) SaffronGL_LoadShaderCoreCtx((GLCTX), (CTX))
#endif
#else
#ifndef SAFFRON_GL_EXPECT_SHADERCORE
int SaffronGL_LoadShaderCoreCtx(MR_Word gl_ctx, struct SaffronGL_ShaderCoreCtx *ctx){
    MR_Word maybe_func, func;
    MR_Bool have_ext;
    MR_String err = NULL;
    MR_Integer major, minor;
    SaffronGL_Version(gl_ctx, &major, &minor);
    if(major > 2 || (major == 2 && minor >= 0)) do{
        SAFFRON_GL_SHADERCORE_FUNCS(SAFFRON_GL_LOAD_FUNC_CORE)
        return 0;
    }while(0);
    return 1;
}
#endif
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_Uniform1d_PROC)(
    GLint,
    double);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_Uniform2d_PROC)(
    GLint,
    double,
    double);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_Uniform3d_PROC)(
    GLint,
    double,
    double,
    double);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_Uniform4d_PROC)(
    GLint,
    double,
    double,
    double,
    double);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_Uniform1dv_PROC)(
    GLint,
    const GLdouble*);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_Uniform2dv_PROC)(
    GLint,
    const GLdouble*);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_Uniform3dv_PROC)(
    GLint,
    const GLdouble*);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_Uniform4dv_PROC)(
    GLint,
    const GLdouble*);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_UniformMatrix2dv_PROC)(
    GLint,
    const GLdouble*);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_UniformMatrix3dv_PROC)(
    GLint,
    const GLdouble*);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_UniformMatrix4dv_PROC)(
    GLint,
    const GLdouble*);
#endif
#ifndef SAFFRON_GL_IMPL
#define SAFFRON_GL_SHADERFP64_FUNCS(X) \
    X(Uniform1d) \
    X(Uniform2d) \
    X(Uniform3d) \
    X(Uniform4d) \
    X(Uniform1dv) \
    X(Uniform2dv) \
    X(Uniform3dv) \
    X(Uniform4dv) \
    X(UniformMatrix2dv) \
    X(UniformMatrix3dv) \
    X(UniformMatrix4dv) \
/* */
struct SaffronGL_ShaderFP64Ctx{
    SAFFRON_GL_SHADERFP64_FUNCS(SAFFRON_GL_FUNC_VAR)
};
#endif
#ifndef SAFFRON_GL_IMPL
struct SaffronGL_ShaderFP64Ctx;
int SaffronGL_LoadShaderFP64Ctx(MR_Word gl_ctx, struct SaffronGL_ShaderFP64Ctx *ctx);
#define SAFFRON_GL_SHADERFP64_FUNC(CTX, X) ((CTX)->X)
#define SAFFRON_GL_SHADERFP64_CTX struct SaffronGL_ShaderFP64Ctx*
#define SAFFRON_GL_SHADERFP64_CTX_FIELD(NAME) struct SaffronGL_ShaderFP64Ctx NAME;
#define SAFFRON_GL_ALLOC_SHADERFP64_CTX() \
    MR_GC_malloc_atomic(sizeof(struct SaffronGL_ShaderFP64Ctx))
#define SAFFRON_GL_LOAD_SHADERFP64_CTX(GLCTX, CTX) SaffronGL_LoadShaderFP64Ctx((GLCTX), (CTX))
#else
int SaffronGL_LoadShaderFP64Ctx(MR_Word gl_ctx, struct SaffronGL_ShaderFP64Ctx *ctx){
    MR_Word maybe_func, func;
    MR_Bool have_ext;
    MR_String err = NULL;
#define SAFFRON_GL_LOAD_FUNC_SHADERFP64_ARB(X) SAFFRON_GL_FUNC_LOAD(X, "ARB")
    SaffronGL_SupportsExtension(gl_ctx, (MR_String)"GL_ARB_gpu_shader_fp64", &have_ext);
    if(have_ext){
        do{
            SAFFRON_GL_SHADERFP64_FUNCS(SAFFRON_GL_LOAD_FUNC_SHADERFP64_ARB)
            return 0;
        }while(0);
    }
    return 1;
}
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_GetProgramBinary_PROC)(
    GLuint,
    GLsizei,
    GLsizei*,
    GLenum*,
    void*);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_ProgramBinary_PROC)(
    GLuint,
    GLenum,
    const void*,
    GLsizei);
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_ProgramParameteri_PROC)(
    GLuint,
    GLenum,
    GLint);
#endif
#ifndef SAFFRON_GL_IMPL
#define SAFFRON_GL_PROGRAMBINARY_FUNCS(X) \
    X(GetProgramBinary) \
    X(ProgramBinary) \
    X(ProgramParameteri) \
/* */
struct SaffronGL_ProgramBinaryCtx{
    SAFFRON_GL_PROGRAMBINARY_FUNCS(SAFFRON_GL_FUNC_VAR)
};
#endif
#ifndef SAFFRON_GL_IMPL
#ifdef SAFFRON_GL_EXPECT_PROGRAMBINARY
#define SAFFRON_GL_PROGRAMBINARY_FUNC(CTX, X) (gl ## X)
#define SAFFRON_GL_PROGRAMBINARY_CTX MR_Word
#define SAFFRON_GL_PROGRAMBINARY_CTX_FIELD(NAME)
#define SAFFRON_GL_ALLOC_PROGRAMBINARY_CTX() 0
#define SAFFRON_GL_LOAD_PROGRAMBINARY_CTX(GLCTX, CTX) 0
#else
struct SaffronGL_ProgramBinaryCtx;
int SaffronGL_LoadProgramBinaryCtx(MR_Word gl_ctx, struct SaffronGL_ProgramBinaryCtx *ctx);
#define SAFFRON_GL_PROGRAMBINARY_FUNC(CTX, X) ((CTX)->X)
#define SAFFRON_GL_PROGRAMBINARY_CTX struct SaffronGL_ProgramBinaryCtx*
#define SAFFRON_GL_PROGRAMBINARY_CTX_FIELD(NAME) struct SaffronGL_ProgramBinaryCtx NAME;
#define SAFFRON_GL_ALLOC_PROGRAMBINARY_CTX() \
    MR_GC_malloc_atomic(sizeof(struct SaffronGL_ProgramBinaryCtx))
#define SAFFRON_GL_LOAD_PROGRAMBINARY_CTX(GLCTX, CTX) SaffronGL_LoadProgramBinaryCtx((GLCTX), (CTX))
#endif
#else
#ifndef SAFFRON_GL_EXPECT_PROGRAMBINARY
int SaffronGL_LoadProgramBinaryCtx(MR_Word gl_ctx, struct SaffronGL_ProgramBinaryCtx *ctx){
    MR_Word maybe_func, func;
    MR_Bool have_ext;
    MR_String err = NULL;
    MR_Integer major, minor;
    SaffronGL_Version(gl_ctx, &major, &minor);
    if(major > 4 || (major == 4 && minor >= 1)) do{
        SAFFRON_GL_PROGRAMBINARY_FUNCS(SAFFRON_GL_LOAD_FUNC_CORE)
        return 0;
    }while(0);
#define SAFFRON_GL_LOAD_FUNC_PROGRAMBINARY_ARB(X) SAFFRON_GL_FUNC_LOAD(X, "ARB")
    SaffronGL_SupportsExtension(gl_ctx, (MR_String)"GL_ARB_get_program_binary", &have_ext);
    if(have_ext){
        do{
            SAFFRON_GL_PROGRAMBINARY_FUNCS(SAFFRON_GL_LOAD_FUNC_PROGRAMBINARY_ARB)
            return 0;
        }while(0);
    }
    return 1;
}
#endif
#endif
#ifndef SAFFRON_GL_IMPL
typedef void(APIENTRY*SaffronGL_SpecializeShader_PROC)(
    GLuint,
    const SaffronGLchar*,
    GLuint,
    const GLuint*,
    const GLuint*);
#endif
#ifndef SAFFRON_GL_IMPL
#define SAFFRON_GL_SPIRV_FUNCS(X) \
    X(SpecializeShader) \
/* */
struct SaffronGL_SPIRVCtx{
    SAFFRON_GL_SPIRV_FUNCS(SAFFRON_GL_FUNC_VAR)
};
#endif
#ifndef SAFFRON_GL_IMPL
struct SaffronGL_SPIRVCtx;
int SaffronGL_LoadSPIRVCtx(MR_Word gl_ctx, struct SaffronGL_SPIRVCtx *ctx);
#define SAFFRON_GL_SPIRV_FUNC(CTX, X) ((CTX)->X)
#define SAFFRON_GL_SPIRV_CTX struct SaffronGL_SPIRVCtx*
#define SAFFRON_GL_SPIRV_CTX_FIELD(NAME) struct SaffronGL_SPIRVCtx NAME;
#define SAFFRON_GL_ALLOC_SPIRV_CTX() \
    MR_GC_malloc_atomic(sizeof(struct SaffronGL_SPIRVCtx))
#define SAFFRON_GL_LOAD_SPIRV_CTX(GLCTX, CTX) SaffronGL_LoadSPIRVCtx((GLCTX), (CTX))
#else
int SaffronGL_LoadSPIRVCtx(MR_Word gl_ctx, struct SaffronGL_SPIRVCtx *ctx){
    MR_Word maybe_func, func;
    MR_Bool have_ext;
    MR_String err = NULL;
#define SAFFRON_GL_LOAD_FUNC_SPIRV_ARB(X) SAFFRON_GL_FUNC_LOAD(X, "ARB")
    SaffronGL_SupportsExtension(gl_ctx, (MR_String)"GL_ARB_gl_spirv", &have_ext);
    if(have_ext){
        do{
            SAFFRON_GL_SPIRV_FUNCS(SAFFRON_GL_LOAD_FUNC_SPIRV_ARB)
            return 0;
        }while(0);
    }
    return 1;
}
#endif
#ifndef GL_SHADING_LANGUAGE_VERSION
#define GL_SHADING_LANGUAGE_VERSION 0x8B8C
#endif
#ifndef GL_VERTEX_SHADER
#define GL_VERTEX_SHADER 0x8B31
#endif
#ifndef GL_FRAGMENT_SHADER
#define GL_FRAGMENT_SHADER 0x8B30
#endif
#ifndef GL_GEOMETRY_SHADER
#define GL_GEOMETRY_SHADER 0x8DD9
#endif
#ifndef GL_COMPILE_STATUS
#define GL_COMPILE_STATUS 0x8B81
#endif
#ifndef GL_LINK_STATUS
#define GL_LINK_STATUS 0x8B82
#endif
#ifndef GL_VALIDATE_STATUS
#define GL_VALIDATE_STATUS 0x8B83
#endif
#ifndef GL_INFO_LOG_LENGTH
#define GL_INFO_LOG_LENGTH 0x8B84
#endif
#ifndef GL_SHADER_BINARY_FORMAT_SPIR_V
#define GL_SHADER_BINARY_FORMAT_SPIR_V 0x9551
#endif
#ifndef GL_SPIR_V_BINARY
#define GL_SPIR_V_BINARY 0x9552
#endif
#ifndef GL_PROGRAM_BINARY_RETRIEVABLE_HINT
#define GL_PROGRAM_BINARY_RETRIEVABLE_HINT 0x8257
#endif
#ifndef GL_PROGRAM_BINARY_LENGTH
#define GL_PROGRAM_BINARY_LENGTH 0x8741
#endif
#ifndef GL_NUM_PROGRAM_BINARY_FORMATS
#define GL_NUM_PROGRAM_BINARY_FORMATS 0x87FE
#endif
#ifndef GL_PROGRAM_BINARY_FORMATS
#define GL_PROGRAM_BINARY_FORMATS 0x87FF
#endif
