% Copyright (c) 2022 Transnat Games, AlaskanEmily
%
% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.

:- module saffron.gl.buffer.
%=============================================================================%
% OpenGL buffer bindings, including native definitions to load these functions
% at runtime.
%
% Currently, we require these functions to exist on Linux (including Android),
% FreeBSD, OpenBSD, and OS X, as we expect them to have either Mesa or modern
% NVidia or ATI/AMD drivers, and OS X has had buffer functions since around
% OS X 10.3.4
%
% Ideally, we would have either some kind of test or build-time option to
% disable this assumption, but currently it's just a #ifdef below.
:- interface.
%=============================================================================%

:- use_module array.
:- use_module list.

:- use_module saffron.geometry.

%-----------------------------------------------------------------------------%
% The function pointers to let us create buffers.
:- type buffer_ctx.

%-----------------------------------------------------------------------------%

:- type buffer.

%-----------------------------------------------------------------------------%

:- type buffer_type --->
    array_buffer ;
    element_array_buffer.

%-----------------------------------------------------------------------------%

:- pred create_buffer_ctx(Ctx, maybe.maybe_error(buffer_ctx), io.io, io.io)
    <= saffron.gl.context(Ctx).
:- mode create_buffer_ctx(in, out, di, uo) is det.

%-----------------------------------------------------------------------------%

:- pred destroy_buffer(buffer_ctx::in, buffer::in, io.io::di, io.io::uo) is det.

%-----------------------------------------------------------------------------%

:- pred create_buffer(buffer_ctx::in, buffer::uo, io.io::di, io.io::uo) is det.

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(buffer_ctx, buffer).

%-----------------------------------------------------------------------------%

:- pred bind_buffer(buffer_ctx, buffer, buffer_type, io.io, io.io).
:- mode bind_buffer(in, in, in, di, uo) is det.

%-----------------------------------------------------------------------------%

:- type buffer_data(T) == (pred(buffer_ctx, buffer_type, T, io.io, io.io)).
:- inst buffer_data == (pred(in, in, in, di, uo) is det).

:- pred buffer_data_float_array
    `with_type` buffer_data(array.array(float))
    `with_inst` buffer_data.

:- pred buffer_data_float_list
    `with_type` buffer_data(list.list(float))
    `with_inst` buffer_data.

:- pred buffer_data_int_array
    `with_type` buffer_data(array.array(int))
    `with_inst` buffer_data.

:- pred buffer_data_int_list
    `with_type` buffer_data(list.list(int))
    `with_inst` buffer_data.

:- pred buffer_data_vertex2d_array
    `with_type` buffer_data(array.array(saffron.geometry.vertex2d))
    `with_inst` buffer_data.

:- pred buffer_data_vertex2d_list
    `with_type` buffer_data(list.list(saffron.geometry.vertex2d))
    `with_inst` buffer_data.

:- pred buffer_data_vertex3d_array
    `with_type` buffer_data(array.array(saffron.geometry.vertex3d))
    `with_inst` buffer_data.

:- pred buffer_data_vertex3d_list
    `with_type` buffer_data(list.list(saffron.geometry.vertex3d))
    `with_inst` buffer_data.

%=============================================================================%
:- implementation.
%=============================================================================%

:- use_module mmath.
:- use_module mmath.vector.
:- use_module mmath.vector.vector2.
:- use_module mmath.vector.vector3.

:- pragma foreign_import_module("C", saffron.gl).
:- pragma foreign_import_module("C", saffron.geometry).

%-----------------------------------------------------------------------------%
% Define the native prototypes to handle buffers.
:- pragma foreign_decl("C", "
#include ""saffron.gl.buffer.inc""
    ").

%-----------------------------------------------------------------------------%

:- pragma foreign_code("C", "
#define SAFFRON_GL_IMPL
#include ""saffron.gl.buffer.inc""
#undef SAFFRON_GL_IMPL
    ").

% Imports for the Java grade.
:- pragma foreign_decl("Java", "
import org.lwjgl.opengl.ARBVertexBufferObject;
import org.lwjgl.opengl.GLCapabilities;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11; // Base constants
import org.lwjgl.opengl.GL20;
import jmercury.list;
import jmercury.list.List_1;
import jmercury.maybe.Maybe_error_2;
import jmercury.saffron__geometry.Vertex_1;
    ").

%-----------------------------------------------------------------------------%

:- pragma foreign_enum("C", buffer_type/0, [
    array_buffer            - "GL_ARRAY_BUFFER",
    element_array_buffer    - "GL_ELEMENT_ARRAY_BUFFER"
]).

:- pragma foreign_export_enum(
    "Java",
    buffer_type/0,
    [prefix("BUFFER_TYPE_")|[uppercase|[]]]).

:- pragma foreign_code("Java", "
public static int BufferTypeToGL(Buffer_type_0 t, boolean ARB){
    if(t.equals(BUFFER_TYPE_ARRAY_BUFFER)){
        return ARB ?
            ARBVertexBufferObject.GL_ARRAY_BUFFER_ARB :
            GL20.GL_ARRAY_BUFFER;
    }
    else if(t.equals(BUFFER_TYPE_ELEMENT_ARRAY_BUFFER)){
        return ARB ?
            ARBVertexBufferObject.GL_ELEMENT_ARRAY_BUFFER_ARB :
            GL20.GL_ELEMENT_ARRAY_BUFFER;
    }
    else{
        throw new IllegalStateException(""Unknown buffer type"");
    }
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Base class for a Buffer context.
///
/// This is a base class so that we can have either a context of sufficient
/// version to have vertex buffers, or use ARB extensions.
///
/// This uses the bufferType method to convert from Buffer_type_0 to an int, so
/// that no other subclass-implemented method needs to deal with Buffer_type_0.
///
/// Derived classes only need to implement primitive array versions of
/// bufferData, all mercury types and Object-ified arrays will be unwrapped
/// and placed into primitive arrays by the base methods.
public static abstract class Context{
    protected abstract int bufferType(Buffer_type_0 t);
    public abstract int createBuffer();
    public abstract void destroyBuffer(int buffer);
    protected abstract void bindBuffer(int buffer, int t);
    
    protected abstract void bufferData(int t, int[] data);
    public void bufferData(Buffer_type_0 t, int[] data){
        bufferData(bufferType(t), data);
    }
    public void bufferData(Buffer_type_0 t, Integer[] data){
        int[] l_data = new int[data.length];
        for(int i = 0; i < data.length; i++)
            l_data[i] = data[i].intValue();
        bufferData(bufferType(t), l_data);
    }
    public void bufferDataI(Buffer_type_0 t, List_1<Integer> data){
        final int length = jmercury.saffron.ListLength(data);
        int[] l_data = new int[length];
        for(int i = 0; i < length; i++){
            l_data[i] = list.det_head(data).intValue();
            data = list.det_tail(data);
        }
        bufferData(bufferType(t), l_data);
    }
    
    protected abstract void bufferData(int t, double[] data);
    public void bufferData(Buffer_type_0 t, double[] data){
        bufferData(bufferType(t), data);
    }
    public void bufferData(Buffer_type_0 t, Double[] data){
        double[] l_data = new double[data.length];
        for(int i = 0; i < data.length; i++)
            l_data[i] = data[i].doubleValue();
        bufferData(bufferType(t), l_data);
    }
    public void bufferDataD(Buffer_type_0 t, list.List_1<Double> data){
        final int length = jmercury.saffron.ListLength(data);
        double[] l_data = new double[length];
        for(int i = 0; i < length; i++){
            l_data[i] = list.det_head(data).doubleValue();
            data = list.det_tail(data);
        }
        bufferData(bufferType(t), l_data);
    }
    
    public void bufferData2(
        Buffer_type_0 t,
        Vertex_1<jmercury.mmath__vector__vector2.Vector_0>[] data){
        double[] l_data = new double[data.length * 4];
        int i = 0;
        for(Vertex_1<jmercury.mmath__vector__vector2.Vector_0> vertex : data)
            i = jmercury.saffron.WriteVertex2(l_data, vertex, i);
        bufferData(bufferType(t), l_data);
    }
    
    public void bufferData2(
        Buffer_type_0 t,
        List_1<Vertex_1<jmercury.mmath__vector__vector2.Vector_0>> data){
        final int length = jmercury.saffron.ListLength(data);
        double[] l_data = new double[length * 4];
        int i = 0;
        for(int n = 0; n < length; n++){
            i = jmercury.saffron.WriteVertex2(l_data, list.det_head(data), i);
            data = list.det_tail(data);
        }
        bufferData(bufferType(t), l_data);
    }
    
    public void bufferData3(
        Buffer_type_0 t,
        Vertex_1<jmercury.mmath__vector__vector3.Vector_0>[] data){
        double[] l_data = new double[data.length * 5];
        int i = 0;
        for(Vertex_1<jmercury.mmath__vector__vector3.Vector_0> vertex : data){
            i = jmercury.saffron.WriteVertex3(l_data, vertex, i);
        }
        bufferData(bufferType(t), l_data);
    }
    
    public void bufferData3(
        Buffer_type_0 t,
        List_1<Vertex_1<jmercury.mmath__vector__vector3.Vector_0>> data){
        final int length = jmercury.saffron.ListLength(data);
        double[] l_data = new double[length * 5];
        int i = 0;
        for(int n = 0; n < length; n++){
            i = jmercury.saffron.WriteVertex3(l_data, list.det_head(data), i);
            data = list.det_tail(data);
        }
        bufferData(bufferType(t), l_data);
    }
    
    public void bindBuffer(int buffer, Buffer_type_0 t){
        bindBuffer(buffer, bufferType(t));
    }
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Implements a buffer context using core OpenGL functions.
public static class GL20Context extends Context{
    protected int bufferType(Buffer_type_0 t){
        if(t.equals(BUFFER_TYPE_ARRAY_BUFFER))
            return GL20.GL_ARRAY_BUFFER;
        else if(t.equals(BUFFER_TYPE_ELEMENT_ARRAY_BUFFER))
            return GL20.GL_ELEMENT_ARRAY_BUFFER;
        else
            throw new IllegalStateException(""Unknown buffer type"");
    }
    public int createBuffer(){
        return GL20.glGenBuffers();
    }
    public void destroyBuffer(int buffer){
        GL20.glDeleteBuffers(buffer);
    }
    protected void bindBuffer(int buffer, int t){
        GL20.glBindBuffer(t, buffer);
    }
    protected void bufferData(int t, int[] data){
        GL20.glBufferData(t, data, GL20.GL_STATIC_DRAW);
    }
    protected void bufferData(int t, double[] data){
        GL20.glBufferData(t, data, GL20.GL_STATIC_DRAW);
    }
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Implements a buffer context using GL_ARB_vertex_buffer_object
public static class ARBContext extends Context{
    protected int bufferType(Buffer_type_0 t){
        if(t.equals(BUFFER_TYPE_ARRAY_BUFFER))
            return ARBVertexBufferObject.GL_ARRAY_BUFFER_ARB;
        else if(t.equals(BUFFER_TYPE_ELEMENT_ARRAY_BUFFER))
            return ARBVertexBufferObject.GL_ELEMENT_ARRAY_BUFFER_ARB;
        else
            throw new IllegalStateException(""Unknown buffer type"");
    }
    public int createBuffer(){
        return ARBVertexBufferObject.glGenBuffersARB();
    }
    public void destroyBuffer(int buffer){
        ARBVertexBufferObject.glDeleteBuffersARB(buffer);
    }
    protected void bindBuffer(int buffer, int t){
        ARBVertexBufferObject.glBindBufferARB(t, buffer);
    }
    protected void bufferData(int t, int[] data){
        ARBVertexBufferObject.glBufferDataARB(t, data, ARBVertexBufferObject.GL_STATIC_DRAW_ARB);
    }
    protected void bufferData(int t, double[] data){
        ARBVertexBufferObject.glBufferDataARB(t, data, ARBVertexBufferObject.GL_STATIC_DRAW_ARB);
    }
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Creates a shader context, using OpenGL capabilities to determine
///
/// if a GL2.0 or an ARB context is appropriate.
/// May return null if no context can be created.
public static Context CreateContext(){
    final GLCapabilities caps = GL.getCapabilities();
    if(caps.OpenGL20){
        return new GL20Context();
    }
    else if(caps.GL_ARB_vertex_buffer_object){
        return new ARBContext();
    }
    else{
        return null;
    }
}
    ").

%-----------------------------------------------------------------------------%

:- pragma foreign_type("C", buffer_ctx, "SAFFRON_GL_BUFFER_CTX").
:- pragma foreign_type("Java", buffer_ctx, "jmercury.saffron__gl__buffer.Context").

%-----------------------------------------------------------------------------%

:- pragma foreign_type("C", buffer, "GLuint").
:- pragma foreign_type("Java", buffer, "int").

%-----------------------------------------------------------------------------%

:- func create_buffer_ctx_ok(buffer_ctx) = maybe.maybe_error(buffer_ctx).
create_buffer_ctx_ok(Ctx) = maybe.ok(Ctx).
:- pragma foreign_export("C",
    create_buffer_ctx_ok(in) = (out),
    "SaffronGL_CreateBufferCtxOK").

%-----------------------------------------------------------------------------%

:- func create_buffer_ctx_error(string) = maybe.maybe_error(buffer_ctx).
create_buffer_ctx_error(E) = maybe.error(E).
:- pragma foreign_export("C",
    create_buffer_ctx_error(in) = (out),
    "SaffronGL_CreateBufferCtxError").

%-----------------------------------------------------------------------------%

:- pragma foreign_code("Java", "
final public static Maybe_error_2<Context, String> create_context_error = 
    new Maybe_error_2.Error_1(
        ""Buffers require OpenGL 2.0 or GL_ARB_vertex_buffer_objects"");
    ").

:- pred create_buffer_ctx_internal(
    gl_load_func_ctx,
    maybe.maybe_error(buffer_ctx),
    io.io, io.io).
:- mode create_buffer_ctx_internal(
    in,
    out,
    di, uo) is det.

:- pragma foreign_proc("C",
    create_buffer_ctx_internal(Ctx::in, MaybeBufferCtx::out, IOi::di, IOo::uo),
    [may_call_mercury, promise_pure, thread_safe, will_not_throw_exception],
    "
    SAFFRON_GL_BUFFER_CTX ctx = SAFFRON_GL_ALLOC_BUFFER_CTX();
    if(SAFFRON_GL_LOAD_BUFFER_CTX(Ctx, ctx) == 0){
        MaybeBufferCtx = SaffronGL_CreateBufferCtxOK(ctx);
    }
    else{
        MaybeBufferCtx = SaffronGL_CreateBufferCtxError(
            (MR_String)""Could not create context"");
    }
    IOo = IOi;
    ").

:- pragma foreign_proc("Java",
    create_buffer_ctx_internal(Ctx::in, MaybeBufferCtx::out, IOi::di, IOo::uo),
    [may_call_mercury, promise_pure, thread_safe, will_not_throw_exception],
    "
    final jmercury.saffron__gl__buffer.Context ctx =
        jmercury.saffron__gl__buffer.CreateContext();
    MaybeBufferCtx = (ctx == null) ?
        jmercury.saffron__gl__buffer.create_context_error :
        new jmercury.maybe.Maybe_error_2.Ok_1(ctx);
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

create_buffer_ctx(Ctx, MaybeBufferCtx, !IO) :-
    saffron.make_current(Ctx, !IO),
    LoadFuncCtx = 'new gl_load_func_ctx'(Ctx),
    create_buffer_ctx_internal(LoadFuncCtx, MaybeBufferCtx, !IO).

%-----------------------------------------------------------------------------%

:- pragma foreign_proc("C",
    create_buffer(Ctx::in, Buffer::uo, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    tabled_for_io, may_duplicate, does_not_affect_liveness],
    "
    GLuint b;
    SAFFRON_GL_BUFFER_FUNC(Ctx, GenBuffers)(1, &b);
    Buffer = b;
    IOo = IOi;
    ").

:- pragma foreign_proc("Java",
    create_buffer(Ctx::in, Buffer::uo, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, may_duplicate],
    "
    Buffer = Ctx.createBuffer();
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- pragma foreign_proc("C",
    destroy_buffer(Ctx::in, Buffer::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    tabled_for_io, may_duplicate, does_not_affect_liveness],
    "
    GLuint b = Buffer;
    SAFFRON_GL_BUFFER_FUNC(Ctx, DeleteBuffers)(1, &b);
    IOo = IOi;
    ").

:- pragma foreign_proc("Java",
    destroy_buffer(Ctx::in, Buffer::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, may_duplicate],
    "
    Ctx.destroyBuffer(Buffer);
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(buffer_ctx, buffer) where [
    pred(saffron.destroy/4) is saffron.gl.buffer.destroy_buffer
].

%-----------------------------------------------------------------------------%

:- pragma foreign_proc("C",
    bind_buffer(Ctx::in, Buffer::in, Type::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    may_duplicate, does_not_affect_liveness],
    "
    SAFFRON_GL_BUFFER_FUNC(Ctx, BindBuffer)(Type, Buffer);
    IOo = IOi;
    ").

:- pragma foreign_proc("Java",
    bind_buffer(Ctx::in, Buffer::in, Type::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, may_duplicate],
    "
    Ctx.bindBuffer(Buffer, Type);
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- pragma foreign_proc("C",
    buffer_data_float_array(Ctx::in, Type::in, T::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    may_duplicate, does_not_affect_liveness],
    "
    const MR_Integer len = T->size;
    const MR_Integer buffer_size = len * sizeof(MR_Float);
#ifdef SAFFRON_GL_RAW_FLOATS
    SAFFRON_GL_BUFFER_FUNC(Ctx, BufferData)(
        Type,
        buffer_size,
        T->elements;
        GL_STATIC_DRAW);
#else
    SAFFRON_GL_BUFFER_FUNC(Ctx, BufferData)(
        Type,
        buffer_size,
        NULL,
        GL_STATIC_DRAW);
    MR_Float *const buffer = SAFFRON_GL_BUFFER_FUNC(Ctx, MapBuffer)(
        Type,
        GL_WRITE_ONLY);
    MR_Integer i;
    for(i = 0; i < len; i++){
        buffer[i] = MR_word_to_float(T->elements[i]);
    }
    SAFFRON_GL_BUFFER_FUNC(Ctx, UnmapBuffer)(Type);
#endif
    IOo = IOi;
    ").

:- pragma foreign_proc("Java",
    buffer_data_float_array(Ctx::in, Type::in, T::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, may_duplicate],
    "
    Ctx.bufferData(Type, T);
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- pragma foreign_proc("C",
    buffer_data_int_array(Ctx::in, Type::in, T::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    may_duplicate, does_not_affect_liveness],
    "
    /* MR_Integer is guaranteed to be unboxed, but it might not be 32-bits. */
    const MR_Integer len = T->size;
    const MR_Integer buffer_size = len << 2;
#if MR_BYTES_PER_WORD == 4
    SAFFRON_GL_BUFFER_FUNC(Ctx, BufferData)(
        Type,
        buffer_size,
        T->elements,
        GL_STATIC_DRAW);
#else
    SAFFRON_GL_BUFFER_FUNC(Ctx, BufferData)(
        Type,
        buffer_size,
        NULL,
        GL_STATIC_DRAW);
    int *const buffer = SAFFRON_GL_BUFFER_FUNC(Ctx, MapBuffer)(
        Type,
        GL_WRITE_ONLY);
    MR_Integer i;
    for(i = 0; i < len; i++){
        buffer[i] = (int)(T->elements[i]);
    }
    SAFFRON_GL_BUFFER_FUNC(Ctx, UnmapBuffer)(Type);
#endif
    IOo = IOi;
    ").

:- pragma foreign_proc("Java",
    buffer_data_int_array(Ctx::in, Type::in, T::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, may_duplicate],
    "
    Ctx.bufferData(Type, T);
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%
% TODO: This should work, and is optimal when we have unboxed floats, but
% we should eventually implement a better C version for when we have boxed
% floats.
buffer_data_float_list(Ctx, Type, List, !IO) :-
    buffer_data_float_array(Ctx, Type, array.from_list(List), !IO).

% Java will always have boxed/Object-ified values in lists.
% Convert from that to a primitive array in one go.
:- pragma foreign_proc("Java",
    buffer_data_float_list(Ctx::in, Type::in, T::in, IOi::di, IOo::uo),
    [may_call_mercury, promise_pure, thread_safe, may_duplicate],
    "
    Ctx.bufferDataD(Type, T);
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%
% As integers are always unboxed, this actually is optimal.
buffer_data_int_list(Ctx, Type, List, !IO) :-
    buffer_data_int_array(Ctx, Type, array.from_list(List), !IO).

% Java will always have boxed/Object-ified values in lists.
% Convert from that to a primitive array in one go.
:- pragma foreign_proc("Java",
    buffer_data_int_list(Ctx::in, Type::in, T::in, IOi::di, IOo::uo),
    [may_call_mercury, promise_pure, thread_safe, may_duplicate],
    "
    Ctx.bufferDataI(Type, T);
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- pragma foreign_proc("C",
    buffer_data_vertex2d_array(Ctx::in, Type::in, T::in, IOi::di, IOo::uo),
    [may_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    may_duplicate],
    "
    const MR_Integer len = T->size;
    const MR_Integer buffer_size = len * sizeof(MR_Float) * 4;
    SAFFRON_GL_BUFFER_FUNC(Ctx, BufferData)(
        Type,
        buffer_size,
        NULL,
        GL_STATIC_DRAW);
    MR_Float *const buffer = SAFFRON_GL_BUFFER_FUNC(Ctx, MapBuffer)(Type, GL_WRITE_ONLY);
    MR_Integer i;
    for(i = 0; i < len; i++){
        SaffronGeometry_GetVertex2D(
            buffer + (i * 4) + 0,
            buffer + (i * 4) + 1,
            buffer + (i * 4) + 2,
            buffer + (i * 4) + 3,
            T->elements[i]);
    }
    SAFFRON_GL_BUFFER_FUNC(Ctx, UnmapBuffer)(Type);
    IOo = IOi;
    ").

:- pragma foreign_proc("Java",
    buffer_data_vertex2d_array(Ctx::in, Type::in, T::in, IOi::di, IOo::uo),
    [may_call_mercury, promise_pure, thread_safe, may_duplicate],
    "
    Ctx.bufferData2(Type, (Vertex_1[])T);
    IOo = IOi;
    ").

:- pragma foreign_proc("C",
    buffer_data_vertex2d_list(Ctx::in, Type::in, T::in, IOi::di, IOo::uo),
    [may_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    may_duplicate],
    "
    MR_Word X, Y, U, V;
    MR_Integer i = 0, alloc_size = 32;
    MR_Float *buffer = MR_malloc(alloc_size * sizeof(MR_Float));
    
    while(!MR_list_is_empty(T)){
        if(i + 4 >= alloc_size){
            alloc_size <<= 1;
            buffer = MR_realloc(buffer, alloc_size * sizeof(MR_Float));
        }
        SaffronGeometry_GetVertex2D(
            buffer + i + 0,
            buffer + i + 1,
            buffer + i + 2,
            buffer + i + 3,
            MR_list_head(T));
        i += 4;
        T = MR_list_tail(T);
    }
    SAFFRON_GL_BUFFER_FUNC(Ctx, BufferData)(
        Type,
        i * sizeof(MR_Float),
        buffer,
        GL_STATIC_DRAW);
    MR_free(buffer);
    IOo = IOi;
    ").

:- pragma foreign_proc("Java",
    buffer_data_vertex2d_list(Ctx::in, Type::in, T::in, IOi::di, IOo::uo),
    [may_call_mercury, promise_pure, thread_safe, may_duplicate],
    "
    Ctx.bufferData2(Type, T);
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- pragma foreign_proc("C",
    buffer_data_vertex3d_array(Ctx::in, Type::in, T::in, IOi::di, IOo::uo),
    [may_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    may_duplicate],
    "
    const MR_Integer len = T->size;
    const MR_Integer buffer_size = len * sizeof(MR_Float) * 5;
    SAFFRON_GL_BUFFER_FUNC(Ctx, BufferData)(
        Type,
        buffer_size,
        NULL,
        GL_STATIC_DRAW);
    MR_Float *const buffer = SAFFRON_GL_BUFFER_FUNC(Ctx, MapBuffer)(Type, GL_WRITE_ONLY);
    MR_Integer i;
    
    for(i = 0; i < len; i++){
        SaffronGeometry_GetVertex3D(
            buffer + (i * 5) + 0,
            buffer + (i * 5) + 1,
            buffer + (i * 5) + 2,
            buffer + (i * 5) + 3,
            buffer + (i * 5) + 4,
            T->elements[i]);
    }
    
    SAFFRON_GL_BUFFER_FUNC(Ctx, UnmapBuffer)(Type);
    IOo = IOi;
    ").

:- pragma foreign_proc("Java",
    buffer_data_vertex3d_array(Ctx::in, Type::in, T::in, IOi::di, IOo::uo),
    [may_call_mercury, promise_pure, thread_safe, may_duplicate],
    "
    Ctx.bufferData3(Type, (Vertex_1[])T);
    IOo = IOi;
    ").

:- pragma foreign_proc("C",
    buffer_data_vertex3d_list(Ctx::in, Type::in, T::in, IOi::di, IOo::uo),
    [may_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    may_duplicate],
    "
    MR_Integer i = 0, alloc_size = 32;
    MR_Float *buffer = MR_malloc(alloc_size * sizeof(MR_Float));
    
    while(!MR_list_is_empty(T)){
        if(i + 5 >= alloc_size){
            alloc_size <<= 1;
            buffer = MR_realloc(buffer, alloc_size * sizeof(MR_Float));
        }
        SaffronGeometry_GetVertex3D(
            buffer + i + 0,
            buffer + i + 1,
            buffer + i + 2,
            buffer + i + 3,
            buffer + i + 4,
            MR_list_head(T));
        i += 5;
        T = MR_list_tail(T);
    }
    SAFFRON_GL_BUFFER_FUNC(Ctx, BufferData)(
        Type,
        i * sizeof(MR_Float),
        buffer,
        GL_STATIC_DRAW);
    MR_free(buffer);
    IOo = IOi;
    ").

:- pragma foreign_proc("Java",
    buffer_data_vertex3d_list(Ctx::in, Type::in, T::in, IOi::di, IOo::uo),
    [may_call_mercury, promise_pure, thread_safe, may_duplicate],
    "
    Ctx.bufferData3(Type, T);
    IOo = IOi;
    ").

