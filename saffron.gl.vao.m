% Copyright (c) 2024 Transnat Games, AlaskanEmily
%
% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.

:- module saffron.gl.vao.
%=============================================================================%
% OpenGL Vertex Array Object bindings. including native definitions to load
% these functions at runtime.
%
% These are either loaded as core in OpenGL 3.1 or higher (technically this was
% in 3.0, but I can't test this), or has one of the extensions:
%   - GL_APPLE_vertex_array_object (Not available in Java)
%   - GL_ARB_vertex_array_object
%
:- interface.
%=============================================================================%

:- use_module maybe.

%-----------------------------------------------------------------------------%
% The function pointers to let us create vaos.
:- type vao_ctx.

%-----------------------------------------------------------------------------%

:- type vao.

%-----------------------------------------------------------------------------%

:- pred create_vao_ctx(Ctx, maybe.maybe_error(vao_ctx), io.io, io.io)
    <= saffron.gl.context(Ctx).
:- mode create_vao_ctx(in, out, di, uo) is det.

%-----------------------------------------------------------------------------%

:- pred create_vao(vao_ctx::in, vao::uo, io.io::di, io.io::uo) is det.

%-----------------------------------------------------------------------------%

:- pred destroy_vao(vao_ctx::in, vao::in, io.io::di, io.io::uo) is det.

%-----------------------------------------------------------------------------%

:- pred bind_vao(vao_ctx::in, vao::in, io.io::di, io.io::uo) is det.

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(vao_ctx, vao).

%=============================================================================%
:- implementation.
%=============================================================================%

:- use_module exception.

:- pragma foreign_decl("C", "
#include ""saffron.gl.vao.inc""
    ").

:- pragma foreign_code("C", "
#define SAFFRON_GL_IMPL
#include ""saffron.gl.vao.inc""
#undef SAFFRON_GL_IMPL
    ").

% Imports for the Java grade.
:- pragma foreign_decl("Java", "
import org.lwjgl.opengl.ARBVertexArrayObject;
import org.lwjgl.opengl.GLCapabilities;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11; // Base constants
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30; // Geometry vaos
import org.lwjgl.opengl.GLCapabilities;
import jmercury.maybe.Maybe_error_2;
    ").

:- pragma foreign_import_module("C", saffron.gl).

:- pragma foreign_code("Java", "
///////////////////////////////////////////////////////////////////////////////
/// \brief Base class for a VAO context.
///
/// This is a base class so that we can have either a context of sufficient
/// version to have vaos, or use ARB extensions to implement vaos.
public static abstract class Context{
    public abstract int createVAO();
    public abstract void deleteVAO(int vao);
    public abstract void bindVAO(int vao);
}

///////////////////////////////////////////////////////////////////////////////
/// VAO context that uses OpenGL core features.
private static class GL30Context extends Context{
    public int createVAO(){ return GL30.glGenVertexArrays(); }
    public void deleteVAOs(int vao){ GL30.glDeleteVertexArrays(vao); }
    public void bindVAO(int vao){ GL30.glBindVertexArray(vao); }
}

///////////////////////////////////////////////////////////////////////////////
// A vao context that uses ARB extensions.
private static class ARBContext extends Context{
    public int createVAO(){ return ARBVertexArrayObject.glGenVertexArrays(); }
    public void deleteVAOs(int vao){
        ARBVertexArrayObject.glDeleteVertexArrays(vao);
    }
    public void bindVAO(int vao){
        ARBVertexArrayObject.glBindVertexArray(vao);
    }
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Creates a VAO context, using OpenGL capabilities to determine
///
/// if a GL3.0 or an ARB context is appropriate.
/// May return null if no context can be created.
public static Context CreateContext(){
    final GLCapabilities caps = GL.getCapabilities();
    if(caps.OpenGL30){
        return new GL30Context();
    }
    else if(caps.GL_ARB_vertex_array_object){
        return new ARBContext();
    }
    else{
        return null;
    }
}
    ").

%-----------------------------------------------------------------------------%

:- pragma foreign_type("C", vao_ctx, "SAFFRON_GL_VAO_CTX").
:- pragma foreign_type("Java", vao_ctx, "jmercury.saffron__gl__vao.Context").

%-----------------------------------------------------------------------------%

:- pragma foreign_type("C", vao, "GLuint").
:- pragma foreign_type("Java", vao, "int").

%-----------------------------------------------------------------------------%

:- func create_vao_ctx_ok(vao_ctx) = maybe.maybe_error(vao_ctx).
create_vao_ctx_ok(Ctx) = maybe.ok(Ctx).
:- pragma foreign_export("C",
    create_vao_ctx_ok(in) = (out),
    "SaffronGL_CreateVAOCtxOK").

%-----------------------------------------------------------------------------%

:- func create_vao_ctx_error(string) = maybe.maybe_error(vao_ctx).
create_vao_ctx_error(E) = maybe.error(E).
:- pragma foreign_export("C",
    create_vao_ctx_error(in) = (out),
    "SaffronGL_CreateVAOCtxError").

%-----------------------------------------------------------------------------%

:- pred create_vao_ctx_internal(
    gl_load_func_ctx,
    maybe.maybe_error(vao_ctx),
    io.io, io.io).
:- mode create_vao_ctx_internal(
    in,
    out,
    di, uo) is det.

:- pragma foreign_proc("C",
    create_vao_ctx_internal(Ctx::in, MaybeVAOCtx::out, IOi::di, IOo::uo),
    [may_call_mercury, promise_pure, thread_safe, will_not_throw_exception],
    "
    SAFFRON_GL_VAO_CTX
    ctx = SAFFRON_GL_ALLOC_VAO_CTX();
    if(SAFFRON_GL_LOAD_VAO_CTX(Ctx, ctx) == 0){
        MaybeVAOCtx = SaffronGL_CreateVAOCtxOK(ctx);
    }
    else{
        MaybeVAOCtx = SaffronGL_CreateVAOCtxError(
            (MR_String)""Could not create context"");
    }
    IOo = IOi;
    ").


:- pragma foreign_proc("Java",
    create_vao_ctx_internal(Ctx::in, MaybeVAOCtx::out, IOi::di, IOo::uo),
    [may_call_mercury, promise_pure, thread_safe, will_not_throw_exception],
    "
    final jmercury.saffron__gl__vao.Context ctx =
        jmercury.saffron__gl__vao.CreateContext();
    MaybeVAOCtx = (ctx == null) ?
        jmercury.saffron__gl__vao.create_context_error :
        new jmercury.maybe.Maybe_error_2.Ok_1(ctx);
    IOo = IOi;
    ").

:- pragma foreign_code("Java", "
final public static Maybe_error_2<Context, String> create_context_error = 
    new Maybe_error_2.Error_1(
        ""Vertex Array Objects require OpenGL 3.0 or GL_ARB_vertex_array_object"");
    ").

create_vao_ctx(Ctx, MaybeVAOCtx, !IO) :-
    create_vao_ctx_internal('new gl_load_func_ctx'(Ctx), MaybeVAOCtx, !IO).

%-----------------------------------------------------------------------------%

:- pragma foreign_proc("C",
    create_vao(Ctx::in, VAO::uo, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    tabled_for_io, may_duplicate, does_not_affect_liveness],
    "
    GLuint vao;
    SAFFRON_GL_VAO_FUNC(Ctx, GenVertexArrays)(1, &vao);
    VAO = vao;
    IOo = IOi;
    ").

:- pragma foreign_proc("Java",
    create_vao(Ctx::in, VAO::uo, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, may_duplicate],
    "
    VAO = Ctx.createVAO();
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- pragma foreign_proc("C",
    destroy_vao(Ctx::in, VAO::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    tabled_for_io, may_duplicate, does_not_affect_liveness],
    "
    GLuint vao = VAO;
    SAFFRON_GL_VAO_FUNC(Ctx, DeleteVertexArrays)(1, &vao);
    IOo = IOi;
    ").

:- pragma foreign_proc("Java",
    destroy_vao(Ctx::in, VAO::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, may_duplicate],
    "
    Ctx.destroyVAO(VAO);
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- instance saffron.destroy(vao_ctx, vao) where [
    pred(saffron.destroy/4) is saffron.gl.vao.destroy_vao
].

%-----------------------------------------------------------------------------%

:- pragma foreign_proc("C",
    bind_vao(Ctx::in, VAO::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    may_duplicate, does_not_affect_liveness],
    "
    SAFFRON_GL_VAO_FUNC(Ctx, BindVertexArray)(VAO);
    IOo = IOi;
    ").

:- pragma foreign_proc("Java",
    bind_vao(Ctx::in, VAO::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, may_duplicate],
    "
    Ctx.bindVAO(VAO);
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

