% Copyright (c) 2021-2024 Transnat Games, AlaskanEmily
%
% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.

:- module saffron.geometry.
%=============================================================================%
% Geometry types, typeclasses, and preds
:- interface.
%=============================================================================%

:- use_module array.
:- use_module list.

:- use_module mmath.
:- use_module mmath.vector.

:- use_module saffron.texture.

%-----------------------------------------------------------------------------%

:- type vertex(Vec) ---> vertex(vec::Vec, u::float, v::float).
:- type vertex2d == vertex(mmath.vector.vector2).
:- type vertex3d == vertex(mmath.vector.vector3).

%-----------------------------------------------------------------------------%
% This is intended to be used as if it was a functor for vertex, to construct
% or destructure the vector so that the vector's fields appear directly in the
% vertex itself.
:- func vertex(float, float, float, float) = vertex2d.
:- mode vertex(in, in, in, in) = (out) is det.
:- mode vertex(di, di, di, di) = (uo) is det.
:- mode vertex(mdi, mdi, mdi, mdi) = (muo) is det.
:- mode vertex(out, out, out, out) = (in) is det.
:- mode vertex(uo, uo, uo, uo) = (di) is det.
:- mode vertex(muo, muo, muo, muo) = (mdi) is det.
:- mode vertex(in, in, in, in) = (in) is semidet. % Implied.

:- pred vertex(float, float, float, float, vertex2d).
:- mode vertex(in, in, in, in, out) is det.
:- mode vertex(di, di, di, di, uo) is det.
:- mode vertex(mdi, mdi, mdi, mdi, muo) is det.
:- mode vertex(out, out, out, out, in) is det.
:- mode vertex(uo, uo, uo, uo, di) is det.
:- mode vertex(muo, muo, muo, muo, mdi) is det.
:- mode vertex(in, in, in, in, in) is semidet. % Implied.

%-----------------------------------------------------------------------------%
% This is intended to be used as if it was a functor for vertex, to construct
% or destructure the vector so that the vector's fields appear directly in the
% vertex itself.
:- func vertex(float, float, float, float, float) = vertex3d.
:- mode vertex(in, in, in, in, in) = (out) is det.
:- mode vertex(di, di, di, di, di) = (uo) is det.
:- mode vertex(mdi, mdi, mdi, mdi, mdi) = (muo) is det.
:- mode vertex(out, out, out, out, out) = (in) is det.
:- mode vertex(uo, uo, uo, uo, uo) = (di) is det.
:- mode vertex(muo, muo, muo, muo, muo) = (mdi) is det.
:- mode vertex(in, in, in, in, in) = (in) is semidet. % Implied.

:- pred vertex(float, float, float, float, float, vertex3d).
:- mode vertex(in, in, in, in, in, out) is det.
:- mode vertex(di, di, di, di, di, uo) is det.
:- mode vertex(mdi, mdi, mdi, mdi, mdi, muo) is det.
:- mode vertex(out, out, out, out, out, in) is det.
:- mode vertex(uo, uo, uo, uo, uo, di) is det.
:- mode vertex(muo, muo, muo, muo, muo, mdi) is det.
:- mode vertex(in, in, in, in, in, in) is semidet. % Implied.

%-----------------------------------------------------------------------------%

:- type primitive_type --->
    line_strip ;
    line_loop ;
    triangles ;
    triangle_strip ;
    triangle_fan.

%-----------------------------------------------------------------------------%

:- typeclass buffer(Ctx, Buffer, Vec)
    <= ((Buffer -> Vec), (Ctx, Vec -> Buffer), destroy(Ctx, Buffer)) where [
    
    pred create_buffer_list(Ctx, list.list(vertex(Vec)), Buffer, io.io, io.io),
    mode create_buffer_list(in, in, out, di, uo) is det,
    
    pred create_buffer_array(Ctx, array.array(vertex(Vec)), Buffer, io.io, io.io),
    mode create_buffer_array(in, in, out, di, uo) is det
].

%-----------------------------------------------------------------------------%

:- typeclass shape(Ctx, Shape, Buffer, Tex)
    <= ((Shape -> Buffer),
        (Ctx, Buffer -> Shape),
        destroy(Ctx, Shape),
        saffron.texture.texture(Ctx, Tex)) where[
    
    pred create_shape(Ctx, primitive_type, Buffer, Tex, Shape, io.io, io.io),
    mode create_shape(in, in, in, in, out, di, uo) is det,
    
    pred create_shape_index_list(
        Ctx,
        primitive_type,
        Buffer,
        list.list(int),
        Tex,
        Shape,
        io.io, io.io),
    mode create_shape_index_list(
        in,
        in,
        in, 
        in, 
        in,
        out,
        di, uo) is det,
    
    pred create_shape_index_array(
        Ctx,
        primitive_type,
        Buffer,
        array.array(int),
        Tex,
        Shape,
        io.io, io.io),
    mode create_shape_index_array(
        in,
        in,
        in, 
        in, 
        in,
        out,
        di, uo) is det
].

% Different order of arguments, can be more useful for currying arguments in
% some situations.
:- pred create_shape2(Ctx, primitive_type, Tex, Buffer, Shape, io.io, io.io)
    <= shape(Ctx, Shape, Buffer, Tex).
:- mode create_shape2(in, in, in, in, out, di, uo) is det.

%-----------------------------------------------------------------------------%
% Shorthand for making one-off geometry without building a buffer separately.
:- pred create_shape_list(  
    Ctx,
    list.list(vertex(Vec)),
    primitive_type,
    Tex,
    Buffer,
    Shape,
    io.io, io.io)
    <= (buffer(Ctx, Buffer, Vec),
        shape(Ctx, Shape, Buffer, Tex)).

:- mode create_shape_list(
    in,
    in,
    in,
    in,
    out,
    out,
    di, uo) is det.

%-----------------------------------------------------------------------------%
% Shorthand for making one-off geometry without building a buffer separately.
:- pred create_shape_array(
    Ctx,
    array.array(vertex(Vec)),
    primitive_type,
    Tex,
    Buffer,
    Shape,
    io.io, io.io)
    <= (buffer(Ctx, Buffer, Vec),
        shape(Ctx, Shape, Buffer, Tex)).

:- mode create_shape_array(
    in,
    in,
    in,
    in,
    out,
    out,
    di, uo) is det.

%=============================================================================%
:- implementation.
%=============================================================================%

:- use_module mmath.vector.vector2.
:- use_module mmath.vector.vector3.

%-----------------------------------------------------------------------------%

:- pragma foreign_export("C",
    vertex(in, in, in, in) = (out),
    "SaffronGeometry_CreateVertex2D").

:- pragma foreign_export("C",
    vertex(in, in, in, in) = (out),
    "CreateVertex").

%-----------------------------------------------------------------------------%

:- pragma foreign_export("C",
    vertex(out, out, out, out, in),
    "SaffronGeometry_GetVertex2D").

:- pragma foreign_export("Java",
    vertex(out, out, out, out, in),
    "GetVertex").

%-----------------------------------------------------------------------------%

:- pragma foreign_export("C",
    vertex(in, in, in, in, in) = (out),
    "SaffronGeometry_CreateVertex3D").

:- pragma foreign_export("Java",
    vertex(in, in, in, in, in) = (out),
    "CreateVertex").

%-----------------------------------------------------------------------------%

:- pragma foreign_export("C",
    vertex(out, out, out, out, out, in),
    "SaffronGeometry_GetVertex3D").

:- pragma foreign_export("Java",
    vertex(out, out, out, out, out, in),
    "GetVertex").

%-----------------------------------------------------------------------------%

vertex(X, Y, U, V) = vertex(mmath.vector.vector2.vector(X, Y), U, V).
vertex(X, Y, U, V, vertex(mmath.vector.vector2.vector(X, Y), U, V)).

%-----------------------------------------------------------------------------%

vertex(X, Y, Z, U, V) = vertex(mmath.vector.vector3.vector(X, Y, Z), U, V).
vertex(X, Y, Z, U, V, vertex(mmath.vector.vector3.vector(X, Y, Z), U, V)).

%-----------------------------------------------------------------------------%

create_shape2(Ctx, Type, Tex, Buffer, Shape, !IO) :-
    create_shape(Ctx, Type, Buffer, Tex, Shape, !IO).

:- pragma inline(create_shape2/7).

%-----------------------------------------------------------------------------%

create_shape_list(Ctx, Vertices, Type, Tex, Buffer, Shape, !IO) :-
    create_buffer_list(Ctx, Vertices, Buffer, !IO),
    create_shape(Ctx, Type, Buffer, Tex, Shape, !IO).

%-----------------------------------------------------------------------------%

create_shape_array(Ctx, Vertices, Type, Tex, Buffer, Shape, !IO) :-
    create_buffer_array(Ctx, Vertices, Buffer, !IO),
    create_shape(Ctx, Type, Buffer, Tex, Shape, !IO).

