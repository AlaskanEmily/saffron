% Copyright (c) 2022, 2024 Transnat Games, AlaskanEmily
%
% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.

:- module saffron.gl.shader.
%=============================================================================%
% OpenGL shader bindings, including native definitions to load these functions
% at runtime.
%
% Currently, we require these functions to exist on Linux (including Android),
% FreeBSD, and OpenBSD as we expect them to have either Mesa or modern NVidia
% or ATI/AMD drivers.
%
% Ideally, we would have either some kind of test or build-time option to
% disable this assumption, but currently it's just a #ifdef below.
%
%=============================================================================%
% Using GLSL Shaders with Saffron:
%=============================================================================%
%
% Saffron provides the following variables to GLSL code by default. Note that
% these have preferred binding locations, though these will only be used when
% the OpenGL implementation reports AMD/ATI as the vendor.
%
% FS? | VS? | Loc | Name              | Description
% ----+-----+-------------------+----------------------------------------------
%     |  X  |   1 | saffron_position  | Position of the current vertex
%  X  |  X  |   2 | saffron_tex_coord | Texture coordinates
%  X  |  X  |   3 | saffron_color     | Color value for the vertex/fragment
%  X  |     |   - | saffron_sampler   | Default texture sampler
%
:- interface.
%=============================================================================%

:- use_module list.
:- use_module array.

%-----------------------------------------------------------------------------%
% The function pointers to let us create shaders.
:- type shader_ctx.

%-----------------------------------------------------------------------------%

:- pred create_shader_ctx(Ctx, maybe.maybe_error(shader_ctx), io.io, io.io)
    <= saffron.gl.context(Ctx).
:- mode create_shader_ctx(in, out, di, uo) is det.

%-----------------------------------------------------------------------------%
% This is the overall shader, which represents a vertex shader, at least one
% fragment shader, and optionally a geometry shader when supported.
% This is created by linking together the individual shaders.
:- type shader_program.

%-----------------------------------------------------------------------------%

:- pred destroy_shader_program(shader_ctx, shader_program, io.io, io.io).
:- mode destroy_shader_program(in, in, di, uo) is det.

%-----------------------------------------------------------------------------%

:- pred use_program(shader_ctx, shader_program, io.io, io.io).
:- mode use_program(in, in, di, uo) is det.

%-----------------------------------------------------------------------------%
% create_program(ShaderCtx, FragShader, VertShader, Program, !IO)
%
% Creates a program with two shaders. This is the common case, for a single
% vertex shader and a single fragment shader.
% This is functually equivalent to:
% create_program(ShaderCtx, [FragShader|[VertShader|[]]], Program, !IO)
:- pred create_program(
    shader_ctx,
    shader,
    shader,
    maybe.maybe_error(shader_program),
    io.io, io.io).
:- mode create_program(in, in, in, uo, di, uo) is det.

%-----------------------------------------------------------------------------%
% create_program(ShaderCtx, Shaders, Program, !IO)
:- pred create_program(
    shader_ctx,
    list.list(shader),
    maybe.maybe_error(shader_program),
    io.io, io.io).
:- mode create_program(in, in, uo, di, uo) is det.

%-----------------------------------------------------------------------------%
% create_program_array(ShaderCtx, Shaders, Program, !IO)
:- pred create_program_array(
    shader_ctx,
    array.array(shader),
    maybe.maybe_error(shader_program),
    io.io, io.io).
:- mode create_program_array(in, in, uo, di, uo) is det.

%-----------------------------------------------------------------------------%

% Note that this is NOT always valid. The caller must be in a compatibility
% context for this to work, as it just returns (GLuint)0
:- func no_shader_program = shader_program.

%-----------------------------------------------------------------------------%

:- type shader_type --->
    fragment ;
    vertex ;
    geometry.

%-----------------------------------------------------------------------------%
% Represents a vertex shader, fragment shader, or geometry shader.
% It is safe to destroy a shader immediately after it is linked, as the object
% is ref-counted in the OpenGL implementation.
:- type shader.

%-----------------------------------------------------------------------------%

:- pred destroy_shader(shader_ctx, shader, io.io, io.io).
:- mode destroy_shader(in, in, di, uo) is det.

%-----------------------------------------------------------------------------%

:- pred create_shader(
    shader_ctx,
    shader_type,
    string, % Source
    maybe.maybe_error(shader),
    io.io, io.io).

:- mode create_shader(
    in,
    in,
    in,
    out,
    di, uo) is det.

%-----------------------------------------------------------------------------%
% create_fragment_shader(Ctx, Src, MaybeShader, !IO)
% Equivalent to:
%   create_shader(Ctx, fragment, Src, MaybeShader, !IO)
:- pred create_fragment_shader(
    shader_ctx,
    string, % Source
    maybe.maybe_error(shader),
    io.io, io.io).

:- mode create_fragment_shader(
    in,
    in,
    out,
    di, uo) is det.

%-----------------------------------------------------------------------------%
% create_vertex_shader(Ctx, Src, MaybeShader, !IO)
% Equivalent to:
%   create_shader(Ctx, vertex, Src, MaybeShader, !IO)
:- pred create_vertex_shader(
    shader_ctx,
    string, % Source
    maybe.maybe_error(shader),
    io.io, io.io).

:- mode create_vertex_shader(
    in,
    in,
    out,
    di, uo) is det.

%-----------------------------------------------------------------------------%

:- type uniform.

%-----------------------------------------------------------------------------%

:- pred get_uniform_location(shader_ctx, shader_program, string, maybe.maybe(uniform), io.io, io.io).
:- mode get_uniform_location(in, in, in, uo, di, uo) is det.

%-----------------------------------------------------------------------------%

:- type uniformv(T) == (pred(shader_ctx, uniform, array.array(T), io.io, io.io)).
:- inst uniformv == (pred(in, in, in, di, uo) is det).
:- mode uniformv == (pred(in, in, in, di, uo) is det).

%-----------------------------------------------------------------------------%

:- pred uniform(shader_ctx, uniform, float, io.io, io.io).
:- mode uniform(in, in, in, di, uo) is det.

%-----------------------------------------------------------------------------%

:- pred uniform(shader_ctx, uniform, float, float, io.io, io.io).
:- mode uniform(in, in, in, in, di, uo) is det.

%-----------------------------------------------------------------------------%

:- pred uniform(shader_ctx, uniform, float, float, float, io.io, io.io).
:- mode uniform(in, in, in, in, in, di, uo) is det.

%-----------------------------------------------------------------------------%

:- pred uniform(shader_ctx, uniform, float, float, float, float, io.io, io.io).
:- mode uniform(in, in, in, in, in, in, di, uo) is det.

%-----------------------------------------------------------------------------%

:- pred uniform1 `with_type` uniformv(float) `with_inst` uniformv.

%-----------------------------------------------------------------------------%

:- pred uniform2 `with_type` uniformv(float) `with_inst` uniformv.

%-----------------------------------------------------------------------------%

:- pred uniform3 `with_type` uniformv(float) `with_inst` uniformv.

%-----------------------------------------------------------------------------%

:- pred uniform4 `with_type` uniformv(float) `with_inst` uniformv.

%-----------------------------------------------------------------------------%

:- pred uniform(shader_ctx,
    uniform,
    float, float, float,
    float, float, float,
    float, float, float,
    io.io, io.io).
:- mode uniform(in,
    in,
    in, in, in,
    in, in, in,
    in, in, in,
    di, uo) is det.

%-----------------------------------------------------------------------------%

:- pred uniform3x3 `with_type` uniformv(float) `with_inst` uniformv.

%-----------------------------------------------------------------------------%

:- pred uniform(shader_ctx,
    uniform,
    float, float, float, float,
    float, float, float, float,
    float, float, float, float,
    float, float, float, float,
    io.io, io.io).
:- mode uniform(in,
    in,
    in, in, in, in,
    in, in, in, in,
    in, in, in, in,
    in, in, in, in,
    di, uo) is det.

%-----------------------------------------------------------------------------%

:- pred uniform4x4 `with_type` uniformv(float) `with_inst` uniformv.

%-----------------------------------------------------------------------------%

:- type vertex_attrib.

%-----------------------------------------------------------------------------%

:- pred bind_attrib_location(shader_ctx, shader_program, string, int, vertex_attrib, io.io, io.io).
:- mode bind_attrib_location(in, in, in, in, uo, di, uo) is det.

%-----------------------------------------------------------------------------%

:- pred get_attrib_location(shader_ctx, shader_program, string, maybe.maybe(vertex_attrib), io.io, io.io).
:- mode get_attrib_location(in, in, in, uo, di, uo) is det.

%-----------------------------------------------------------------------------%
% vertex_attrib_pointer(Ctx, Attrib, ComponentsPerVertex, Type, Stride, Offset, !IO)
:- pred vertex_attrib_pointer(shader_ctx, vertex_attrib, int, data_type, int, int, io.io, io.io).
:- mode vertex_attrib_pointer(in, in, in, in, in, in, di, uo) is det.

%=============================================================================%
:- implementation.
%=============================================================================%

:- use_module exception.
:- use_module string.

:- pragma foreign_import_module("C", saffron.gl).

% Imports for the Java grade.
:- pragma foreign_decl("Java", "
import org.lwjgl.opengl.ARBShaderObjects;
import org.lwjgl.opengl.ARBFragmentShader;
import org.lwjgl.opengl.ARBVertexShader;
import org.lwjgl.opengl.GLCapabilities;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11; // Base constants
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL32; // Geometry shaders
import org.lwjgl.opengl.GLCapabilities;
import jmercury.maybe.Maybe_error_2;
    ").

:- pragma foreign_export_enum(
    "Java",
    shader_type/0,
    [prefix("SHADER_TYPE_")|[uppercase|[]]]).

:- pragma foreign_code("Java", "
///////////////////////////////////////////////////////////////////////////////
/// \brief Base class for a Shader context.
///
/// This is a base class so that we can have either a context of sufficient
/// version to have shaders, or use ARB extensions to implement shaders.
public static abstract class Context{
    public final boolean supports_geometry_shaders;
    protected Context(boolean l_supports_geometry_shaders){
        supports_geometry_shaders = l_supports_geometry_shaders;
    }
    public abstract int createProgram();
    public int createShader(Shader_type_0 type){
        if(type.equals(SHADER_TYPE_FRAGMENT)){
            return createFragmentShader();
        }
        else if(type.equals(SHADER_TYPE_VERTEX)){
            return createVertexShader();
        }
        else if(type.equals(SHADER_TYPE_GEOMETRY)){
            if(supports_geometry_shaders)
                return createGeometryShader();
            // Otherwise, fallthrough below to be unsupported rather than
            // unknown shader type.
        }
        else{
            throw new IllegalStateException(""Unknown shader type"");
        }
        throw new UnsupportedOperationException(""Unsupported shader type"");
    }
    protected abstract int createFragmentShader();
    protected abstract int createVertexShader();
    protected int createGeometryShader(){
        throw new UnsupportedOperationException(
            ""Cannot create geometry shaders"");
    }
    public abstract void deleteShader(int shader);
    public abstract void deleteProgram(int program);
    public abstract void useProgram(int program);
    public abstract void shaderSource(int shader, String src);
    public abstract void compileShader(int shader);
    public abstract void linkProgram(int program);
    public abstract void attachShader(int program, int shader);
    public abstract boolean shaderCompileStatus(int shader);
    public abstract boolean programLinkStatus(int program);
    public abstract String shaderInfoLog(int shader);
    public abstract String programInfoLog(int program);
    public abstract int getUniformLocation(int program, String name);
    public abstract void uniform(int location, float f1);
    public abstract void uniform(int location, float f1, float f2);
    public abstract void uniform(int location, float f1, float f2, float f3);
    public abstract void uniform(int location, float f1, float f2, float f3, float f4);
    public abstract void uniformMat3x3(int location, float[] matrix);
    public abstract void uniformMat4x4(int location, float[] matrix);
    public abstract void bindAttribLocation(int program, int loc, String name);
    public abstract int getAttribLocation(int program, String name);
    public void vertexAttribPointer(int attrib, int n, jmercury.saffron__gl.Data_type_0 t, int stride, long ptr){
        final int type = jmercury.saffron__gl.DataTypeToGL(t);
        vertexAttribPointer(attrib, n, type, stride, ptr);
    }
    public abstract void vertexAttribPointer(int attrib, int n, int type, int stride, long ptr);
    public void uniform(int location, double f1){
        uniform(location, (float)f1);
    }
    public void uniform(int location, double f1, double f2){
        uniform(location, (float)f1, (float)f2);
    }
    public void uniform(int location, double f1, double f2, double f3){
        uniform(location, (float)f1, (float)f2, (float)f3);
    }
    public void uniform(int location, double f1, double f2, double f3, double f4){
        uniform(location, (float)f1, (float)f2, (float)f3, (float)f4);
    }
    public void uniform(int location, Double f1){
        uniform(location, f1.floatValue());
    }
    public void uniform(int location, Double f1, Double f2){
        uniform(location, f1.floatValue(), f2.floatValue());
    }
    public void uniform(int location, Double f1, Double f2, Double f3){
        uniform(location, f1.floatValue(), f2.floatValue(), f3.floatValue());
    }
    public void uniform(int location, Double f1, Double f2, Double f3, Double f4){
        uniform(location, f1.floatValue(), f2.floatValue(), f3.floatValue(), f4.floatValue());
    }
}

///////////////////////////////////////////////////////////////////////////////
/// Shader context that uses OpenGL core features.
private static class GL20Context extends Context{
    public GL20Context(){
        this(GL.getCapabilities());
    }
    public GL20Context(GLCapabilities caps){
        super(caps.OpenGL32);
    }
    public int createProgram(){
        return GL20.glCreateProgram();
    }
    protected int createFragmentShader(){
        return GL20.glCreateShader(GL20.GL_FRAGMENT_SHADER);
    }
    protected int createVertexShader(){
        return GL20.glCreateShader(GL20.GL_VERTEX_SHADER);
    }
    protected int createGeometryShader(){
        return GL32.glCreateShader(GL32.GL_FRAGMENT_SHADER);
    }
    public void deleteShader(int shader){
        GL20.glDeleteShader(shader);
    }
    public void deleteProgram(int program){
        GL20.glDeleteProgram(program);
    }
    public void useProgram(int program){
        GL20.glUseProgram(program);
    }
    public void shaderSource(int shader, String src){
        GL20.glShaderSource(shader, src);
    }
    public void compileShader(int shader){
        GL20.glCompileShader(shader);
    }
    public void linkProgram(int program){
        GL20.glLinkProgram(program);
    }
    public void attachShader(int program, int shader){
        GL20.glAttachShader(program, shader);
    }
    public boolean shaderCompileStatus(int shader){
        final int status = GL20.glGetShaderi(shader, GL20.GL_COMPILE_STATUS);
        return status == GL20.GL_TRUE;
    }
    public boolean programLinkStatus(int program){
        final int status = GL20.glGetProgrami(program, GL20.GL_LINK_STATUS);
        return status == GL20.GL_TRUE;
    }
    public String shaderInfoLog(int shader){
        return GL20.glGetShaderInfoLog(shader);
    }
    public String programInfoLog(int program){
        return GL20.glGetProgramInfoLog(program);
    }
    public int getUniformLocation(int program, String name){
        return GL20.glGetUniformLocation(program, name);
    }
    public void uniform(int location, float f1){
        GL20.glUniform1f(location, f1);
    }
    public void uniform(int location, float f1, float f2){
        GL20.glUniform2f(location, f1, f2);
    }
    public void uniform(int location, float f1, float f2, float f3){
        GL20.glUniform3f(location, f1, f2, f3);
    }
    public void uniform(int location, float f1, float f2, float f3, float f4){
        GL20.glUniform4f(location, f1, f2, f3, f4);
    }
    public void uniformMat3x3(int location, float[] matrix){
        GL20.glUniformMatrix3fv(location, false, matrix);
    }
    public void uniformMat4x4(int location, float[] matrix){
        GL20.glUniformMatrix4fv(location, false, matrix);
    }
    public void bindAttribLocation(int program, int loc, String name){
        GL20.glBindAttribLocation(program, loc, name);
    }
    public int getAttribLocation(int program, String name){
        return GL20.glGetAttribLocation(program, name);
    }
    public void vertexAttribPointer(int attrib, int n, int type, int stride, long ptr){
        GL20.nglVertexAttribPointer(attrib, n, type, false, stride, ptr);
    }
}

///////////////////////////////////////////////////////////////////////////////
// A shader context that uses ARB extensions.
private static class ARBContext extends Context{
    public ARBContext(){
        super(false);
    }
    public int createProgram(){
        return ARBShaderObjects.glCreateProgramObjectARB();
    }
    protected int createFragmentShader(){
        return ARBShaderObjects.glCreateShaderObjectARB(
            ARBFragmentShader.GL_FRAGMENT_SHADER_ARB);
    }
    protected int createVertexShader(){
        return ARBShaderObjects.glCreateShaderObjectARB(
            ARBVertexShader.GL_VERTEX_SHADER_ARB);
    }
    public void deleteShader(int shader){
        ARBShaderObjects.glDeleteObjectARB(shader);
    }
    public void deleteProgram(int program){
        ARBShaderObjects.glDeleteObjectARB(program);
    }
    public void useProgram(int program){
        ARBShaderObjects.glUseProgramObjectARB(program);
    }
    public void shaderSource(int shader, String src){
        ARBShaderObjects.glShaderSourceARB(shader, src);
    }
    public void compileShader(int shader){
        ARBShaderObjects.glCompileShaderARB(shader);
    }
    public void linkProgram(int program){
        ARBShaderObjects.glLinkProgramARB(program);
    }
    public void attachShader(int program, int shader){
        ARBShaderObjects.glAttachObjectARB(program, shader);
    }
    public boolean shaderCompileStatus(int shader){
        final int status = ARBShaderObjects.glGetObjectParameteriARB(
            shader,
            ARBShaderObjects.GL_OBJECT_COMPILE_STATUS_ARB);
        return status == GL11.GL_TRUE;
    }
    public boolean programLinkStatus(int program){
        final int status = ARBShaderObjects.glGetObjectParameteriARB(
            program,
            ARBShaderObjects.GL_OBJECT_LINK_STATUS_ARB);
        return status == GL11.GL_TRUE;
    }
    public String shaderInfoLog(int shader){
        return ARBShaderObjects.glGetInfoLogARB(shader);
    }
    public String programInfoLog(int program){
        return ARBShaderObjects.glGetInfoLogARB(program);
    }
    public int getUniformLocation(int program, String name){
        return ARBShaderObjects.glGetUniformLocationARB(program, name);
    }
    public void uniform(int location, float f1){
        ARBShaderObjects.glUniform1fARB(location, f1);
    }
    public void uniform(int location, float f1, float f2){
        ARBShaderObjects.glUniform2fARB(location, f1, f2);
    }
    public void uniform(int location, float f1, float f2, float f3){
        ARBShaderObjects.glUniform3fARB(location, f1, f2, f3);
    }
    public void uniform(int location, float f1, float f2, float f3, float f4){
        ARBShaderObjects.glUniform4fARB(location, f1, f2, f3, f4);
    }
    public void uniformMat3x3(int location, float[] matrix){
        ARBShaderObjects.glUniformMatrix3fvARB(location, false, matrix);
    }
    public void uniformMat4x4(int location, float[] matrix){
        ARBShaderObjects.glUniformMatrix4fvARB(location, false, matrix);
    }
    public void bindAttribLocation(int program, int loc, String name){
        ARBVertexShader.glBindAttribLocationARB(program, loc, name);
    }
    public int getAttribLocation(int program, String name){
        return ARBVertexShader.glGetAttribLocationARB(program, name);
    }
    public void vertexAttribPointer(int attrib, int n, int type, int stride, long ptr){
        ARBVertexShader.nglVertexAttribPointerARB(attrib, n, type, false, stride, ptr);
    }
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Creates a shader context, using OpenGL capabilities to determine
///
/// if a GL2.0 or an ARB context is appropriate.
/// May return null if no context can be created.
public static Context CreateContext(){
    final GLCapabilities caps = GL.getCapabilities();
    if(caps.OpenGL20){
        return new GL20Context();
    }
    else if(caps.GL_ARB_shader_objects &&
        caps.GL_ARB_fragment_shader &&
        caps.GL_ARB_vertex_shader){
        
        return new ARBContext();
    }
    else{
        return null;
    }
}
    ").

% Define the native prototypes to handle shaders.
:- pragma foreign_decl("C", "
#include ""saffron.gl.shader.inc""
struct SaffronGL_ShaderCtx{
    SAFFRON_GL_SHADERCORE_CTX_FIELD(core);
#ifndef MR_USE_SINGLE_PREC_FLOAT
    MR_Bool have_fp64;
    SAFFRON_GL_SHADERFP64_CTX_FIELD(fp64); /* Optional */
#endif
    MR_Bool have_spirv;
    SAFFRON_GL_SPIRV_CTX_FIELD(spirv); /* Optional */
    MR_Bool have_program_binary;
    SAFFRON_GL_PROGRAMBINARY_CTX_FIELD(program_binary); /* Optional */
};
#define SAFFRON_GL_SHADER_RANGE_ERROR(WHAT, A, N) do{ \\
    if((A)->size != (N)) \\
        SaffronGL_ShaderRangeError((MR_String)(WHAT), (A)->size, (N)); \\
}while(0)

#define SAFFRON_GL_SHADER_HANDLE_ERROR_(CTX, ERR_TO, OK_TO, THAT, WHAT, STATUS_ENUM) do{ \\
    GLint SAFFRON_GL_SHADER_HANDLE_ERROR_i; \\
    SAFFRON_GL_SHADERCORE_FUNC(&((CTX)->core), Get ## WHAT ## iv)( \\
        (THAT), \\
        (STATUS_ENUM), \\
        &SAFFRON_GL_SHADER_HANDLE_ERROR_i); \\
    if(SAFFRON_GL_SHADER_HANDLE_ERROR_i != GL_TRUE){ \\
        SAFFRON_GL_SHADERCORE_FUNC(&((CTX)->core), Get ## WHAT ## iv)( \\
            (THAT), \\
            GL_INFO_LOG_LENGTH, \\
            &SAFFRON_GL_SHADER_HANDLE_ERROR_i); \\
        if(glGetError() != GL_NO_ERROR || SAFFRON_GL_SHADER_HANDLE_ERROR_i < 0){ \\
            MR_make_aligned_string_copy((ERR_TO), ""Invalid "" #WHAT "" log""); \\
        } \\
        else if(SAFFRON_GL_SHADER_HANDLE_ERROR_i == 0){ \\
            MR_make_aligned_string_copy((ERR_TO), ""Empty "" #WHAT "" log""); \\
        } \\
        else{ \\
            const GLint SAFFRON_GL_SHADER_HANDLE_ERROR_len = \\
                SAFFRON_GL_SHADER_HANDLE_ERROR_i; \\
            (ERR_TO) = MR_GC_malloc_atomic(SAFFRON_GL_SHADER_HANDLE_ERROR_len); \\
            SAFFRON_GL_SHADERCORE_FUNC(&((CTX)->core), Get ## WHAT ## InfoLog)( \\
                (THAT), \\
                SAFFRON_GL_SHADER_HANDLE_ERROR_len, \\
                 &SAFFRON_GL_SHADER_HANDLE_ERROR_i, \\
                (ERR_TO)); \\
            (ERR_TO)[SAFFRON_GL_SHADER_HANDLE_ERROR_len - 1] = '\\0'; \\
            if(glGetError() != GL_NO_ERROR){ \\
                MR_GC_free((ERR_TO)); \\
                MR_make_aligned_string_copy((ERR_TO), ""Could not read "" #WHAT "" log""); \\
            } \\
        } \\
        (OK_TO) = MR_FALSE; \\
    } \\
    else{ \\
        (ERR_TO) = (char*)"" ""; \\
        (OK_TO) = MR_TRUE; \\
    } \\
}while(0)

#define SAFFRON_GL_SHADER_HANDLE_SHADER_ERROR(CTX, ERR_TO, OK_TO, THAT) \\
    SAFFRON_GL_SHADER_HANDLE_ERROR_((CTX), (ERR_TO), (OK_TO), (THAT), Shader, GL_COMPILE_STATUS)

#define SAFFRON_GL_SHADER_HANDLE_PROGRAM_ERROR(CTX, ERR_TO, OK_TO, THAT) \\
    SAFFRON_GL_SHADER_HANDLE_ERROR_((CTX), (ERR_TO), (OK_TO), (THAT), Program, GL_LINK_STATUS)

").

:- pragma foreign_code("C", "
#define SAFFRON_GL_IMPL
#include ""saffron.gl.shader.inc""
#undef SAFFRON_GL_IMPL
").

%-----------------------------------------------------------------------------%

:- pragma foreign_enum("C", shader_type/0, [
    fragment    - "GL_FRAGMENT_SHADER",
    vertex      - "GL_VERTEX_SHADER",
    geometry    - "GL_GEOMETRY_SHADER"
]).

:- pragma foreign_export_enum("Java", shader_type/0).

%-----------------------------------------------------------------------------%

:- pragma foreign_type("C", shader_ctx, "struct SaffronGL_ShaderCtx*").
:- pragma foreign_type("Java", shader_ctx, "jmercury.saffron__gl__shader.Context").

%-----------------------------------------------------------------------------%

:- pragma foreign_type("C", shader_program, "GLuint").
:- pragma foreign_type("Java", shader_program, "int").

%-----------------------------------------------------------------------------%

:- pragma foreign_type("C", shader, "GLuint").
:- pragma foreign_type("Java", shader, "int").

%-----------------------------------------------------------------------------%

:- pragma foreign_type("C", uniform, "GLuint").
:- pragma foreign_type("Java", uniform, "int").

%-----------------------------------------------------------------------------%

:- pragma foreign_type("C", vertex_attrib, "GLuint").
:- pragma foreign_type("Java", vertex_attrib, "int").

%-----------------------------------------------------------------------------%

:- pred range_error(string::in, int::in, int::in) is erroneous.
range_error(What, I, N) :-
    exception.throw(exception.software_error(string.append(string.append(
        string.append("Range error in ", What), 
        string.append("Must be ", string.from_int(I))),
        string.append(" is ", string.from_int(N))))).

:- pragma foreign_export("C",
    range_error(in, in, in),
    "SaffronGL_ShaderRangeError").

%-----------------------------------------------------------------------------%

:- func create_shader_ctx_ok(shader_ctx) = maybe.maybe_error(shader_ctx).
create_shader_ctx_ok(Ctx) = maybe.ok(Ctx).
:- pragma foreign_export("C",
    create_shader_ctx_ok(in) = (out),
    "SaffronGL_CreateShaderCtxOK").

%-----------------------------------------------------------------------------%

:- func create_shader_ctx_error(string) = maybe.maybe_error(shader_ctx).
create_shader_ctx_error(E) = maybe.error(E).
:- pragma foreign_export("C",
    create_shader_ctx_error(in) = (out),
    "SaffronGL_CreateShaderCtxError").

%-----------------------------------------------------------------------------%

:- pragma foreign_code("Java", "
final public static Maybe_error_2<Context, String> create_context_error = 
    new Maybe_error_2.Error_1(""Shaders require OpenGL 2.0 or \
GL_ARB_shader_objects, GL_ARB_vertex_shader, and GL_ARB_fragment_shader"");
    ").

:- pred create_shader_ctx_internal(
    gl_load_func_ctx,
    maybe.maybe_error(shader_ctx),
    io.io, io.io).
:- mode create_shader_ctx_internal(
    in,
    out,
    di, uo) is det.

:- pragma foreign_proc("C",
    create_shader_ctx_internal(Ctx::in, MaybeShaderCtx::out, IOi::di, IOo::uo),
    [may_call_mercury, promise_pure, thread_safe, will_not_throw_exception],
    "
    struct SaffronGL_ShaderCtx *ctx;
    ctx = MR_GC_malloc(sizeof(struct SaffronGL_ShaderCtx));
    memset(ctx, 0, sizeof(*ctx));
    if(SAFFRON_GL_LOAD_SHADERCORE_CTX(Ctx, &(ctx->core)) == 0){
        /* Load extensions if possible. */
#ifndef MR_USE_SINGLE_PREC_FLOAT
        if(SAFFRON_GL_LOAD_SHADERFP64_CTX(Ctx, &(ctx->fp64)))
            ctx->have_fp64 = MR_YES;
#endif

        /* Only need to inspect spirv if we have program binaries. */
        if(SAFFRON_GL_LOAD_PROGRAMBINARY_CTX(Ctx, &(ctx->program_binary)) == 0){
            ctx->have_program_binary = MR_YES;
            if(SAFFRON_GL_LOAD_SPIRV_CTX(Ctx, &(ctx->spirv)) == 0){
                ctx->have_spirv = MR_YES;
            }
        }
        MaybeShaderCtx = SaffronGL_CreateShaderCtxOK(ctx);
    }
    else{
        MaybeShaderCtx = SaffronGL_CreateShaderCtxError(
            (MR_String)""Could not create context"");
    }
    IOo = IOi;
    ").


:- pragma foreign_proc("Java",
    create_shader_ctx_internal(Ctx::in, MaybeShaderCtx::out, IOi::di, IOo::uo),
    [may_call_mercury, promise_pure, thread_safe, will_not_throw_exception],
    "
    final jmercury.saffron__gl__shader.Context ctx =
        jmercury.saffron__gl__shader.CreateContext();
    MaybeShaderCtx = (ctx == null) ?
        jmercury.saffron__gl__shader.create_context_error :
        new jmercury.maybe.Maybe_error_2.Ok_1(ctx);
    IOo = IOi;
    ").

create_shader_ctx(Ctx, MaybeShaderCtx, !IO) :-
    create_shader_ctx_internal('new gl_load_func_ctx'(Ctx), MaybeShaderCtx, !IO).

%-----------------------------------------------------------------------------%

:- pragma foreign_proc("C",
    destroy_shader_program(Ctx::in, Program::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    tabled_for_io, may_duplicate, does_not_affect_liveness],
    "
    SAFFRON_GL_SHADERCORE_FUNC(&(Ctx->core), DeleteProgram)(Program);
    IOo = IOi;
    ").

:- pragma foreign_proc("Java",
    destroy_shader_program(Ctx::in, Program::in, IOi::di, IOo::uo),
    [promise_pure, thread_safe, will_not_throw_exception, may_duplicate],
    "
    Ctx.deleteProgram(Program);
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- pred create_program_inner(
    shader_ctx,
    shader,
    shader,
    shader_program,
    bool.bool,
    string,
    io.io, io.io).
:- mode create_program_inner(in, in, in, uo, uo, uo, di, uo) is det.

:- pragma foreign_proc("C",
    create_program_inner(Ctx::in, S2::in, S1::in, Prog::uo, OK::uo, Err::uo, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    tabled_for_io, may_duplicate, does_not_affect_liveness],
    "
    Prog = SAFFRON_GL_SHADERCORE_FUNC(&(Ctx->core), CreateProgram)();
    if(Prog == 0){
        MR_make_aligned_string_copy(Err, ""Error creating shader program"");
        OK = MR_NO;
    }
    else{
        GLint status;
        SAFFRON_GL_SHADERCORE_FUNC(&(Ctx->core), AttachShader)(Prog, S1);
        SAFFRON_GL_SHADERCORE_FUNC(&(Ctx->core), AttachShader)(Prog, S2);
        SAFFRON_GL_SHADERCORE_FUNC(&(Ctx->core), LinkProgram)(Prog);
        SAFFRON_GL_SHADER_HANDLE_PROGRAM_ERROR(Ctx, Err, OK, Prog);
    }
    IOo = IOi;
    ").

:- pragma foreign_proc("Java",
    create_program_inner(Ctx::in, S2::in, S1::in, Prog::uo, OK::uo, Err::uo, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception],
    "
    Prog = Ctx.createProgram();
    OK = jmercury.bool.NO;
    Err = """";
    if(Prog == 0){
        Err = ""Error creating shader program"";
    }
    else{
        Ctx.attachShader(Prog, S1);
        Ctx.attachShader(Prog, S2);
        Ctx.linkProgram(Prog);
        if(Ctx.programLinkStatus(Prog))
            OK = jmercury.bool.YES;
        else
            Err = Ctx.programInfoLog(Prog);
    }
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- pred create_program_inner(
    shader_ctx,
    list.list(shader),
    shader_program,
    bool.bool,
    string,
    io.io, io.io).
:- mode create_program_inner(in, in, uo, uo, uo, di, uo) is det.

:- pragma foreign_proc("C",
    create_program_inner(Ctx::in, Shaders::in, Prog::uo, OK::uo, Err::uo, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    tabled_for_io, may_duplicate, does_not_affect_liveness],
    "
    Prog = SAFFRON_GL_SHADERCORE_FUNC(&(Ctx->core), CreateProgram)();
    if(Prog == 0){
        MR_make_aligned_string_copy(Err, ""Error creating shader program"");
        OK = MR_NO;
    }
    else{
        while(!MR_list_is_empty(Shaders)){
            SAFFRON_GL_SHADERCORE_FUNC(&(Ctx->core), AttachShader)(Prog, MR_list_head(Shaders));
            Shaders = MR_list_tail(Shaders);
        }
        SAFFRON_GL_SHADERCORE_FUNC(&(Ctx->core), LinkProgram)(Prog);
        SAFFRON_GL_SHADER_HANDLE_PROGRAM_ERROR(Ctx, Err, OK, Prog);
    }
    IOo = IOi;
    ").

:- pragma foreign_proc("Java",
    create_program_inner(Ctx::in, Shaders::in, Prog::uo, OK::uo, Err::uo, IOi::di, IOo::uo),
    [may_call_mercury, promise_pure, thread_safe, will_not_throw_exception],
    "
    Prog = Ctx.createProgram();
    OK = jmercury.bool.NO;
    Err = """";
    if(Prog == 0){
        Err = ""Error creating shader program"";
        OK = jmercury.bool.NO;
    }
    else{
        while(!jmercury.list.is_empty(Shaders)){
            Ctx.attachShader(Prog, jmercury.list.det_head(Shaders));
            Shaders = jmercury.list.det_tail(Shaders);
        }
        Ctx.linkProgram(Prog);
        if(Ctx.programLinkStatus(Prog))
            OK = jmercury.bool.YES;
        else
            Err = Ctx.programInfoLog(Prog);
    }
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- pred create_program_inner_array(
    shader_ctx,
    array.array(shader),
    shader_program,
    bool.bool,
    string,
    io.io, io.io).
:- mode create_program_inner_array(in, in, uo, uo, uo, di, uo) is det.

create_program_inner_array(ShaderCtx, Shaders, Program, OK, Err, !IO) :-
    create_program_inner(ShaderCtx, array.to_list(Shaders), Program, OK, Err, !IO).

:- pragma foreign_proc("C",
    create_program_inner_array(Ctx::in, Shaders::in, Prog::uo, OK::uo, Err::uo, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    tabled_for_io, may_duplicate, does_not_affect_liveness],
    "
    Prog = SAFFRON_GL_SHADERCORE_FUNC(&(Ctx->core), CreateProgram)();
    if(Prog == 0){
        MR_make_aligned_string_copy(Err, ""Error creating shader program"");
        OK = MR_NO;
    }
    else{
        MR_Integer i;
        for(i = 0; i < Shaders->size; i++){
            SAFFRON_GL_SHADERCORE_FUNC(&(Ctx->core), AttachShader)(Prog, Shaders->elements[i]);
        }
        SAFFRON_GL_SHADERCORE_FUNC(&(Ctx->core), LinkProgram)(Prog);
        SAFFRON_GL_SHADER_HANDLE_PROGRAM_ERROR(Ctx, Err, OK, Prog);
    }
    IOo = IOi;
    ").

:- pragma foreign_proc("Java",
    create_program_inner_array(Ctx::in, Shaders::in, Prog::uo, OK::uo, Err::uo, IOi::di, IOo::uo),
    [may_call_mercury, promise_pure, thread_safe, will_not_throw_exception],
    "
    Prog = Ctx.createProgram();
    OK = jmercury.bool.NO;
    Err = """";
    if(Prog == 0){
        Err = ""Error creating shader program"";
    }
    else{
        for(int shader : Shaders)
            Ctx.attachShader(Prog, shader);
        Ctx.linkProgram(Prog);
        if(Ctx.programLinkStatus(Prog))
            OK = jmercury.bool.YES;
        else
            Err = Ctx.programInfoLog(Prog);
    }
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

create_program(ShaderCtx, S1, S2, MaybeProgram, !IO) :-
    create_program_inner(ShaderCtx, S1, S2, Program, OK, Err, !IO),
    (
        OK = bool.yes,
        MaybeProgram = maybe.ok(Program)
    ;
        OK = bool.no,
        MaybeProgram = maybe.error(Err)
    ).

:- pragma inline(create_program/6).

%-----------------------------------------------------------------------------%

create_program(ShaderCtx, Shaders, MaybeProgram, !IO) :-
    create_program_inner(ShaderCtx, Shaders, Program, OK, Err, !IO),
    (
        OK = bool.yes,
        MaybeProgram = maybe.ok(Program)
    ;
        OK = bool.no,
        MaybeProgram = maybe.error(Err)
    ).

:- pragma inline(create_program/5).

%-----------------------------------------------------------------------------%

create_program_array(ShaderCtx, Shaders, MaybeProgram, !IO) :-
    create_program_inner_array(ShaderCtx, Shaders, Program, OK, Err, !IO),
    (
        OK = bool.yes,
        MaybeProgram = maybe.ok(Program)
    ;
        OK = bool.no,
        MaybeProgram = maybe.error(Err)
    ).

:- pragma inline(create_program_array/5).

%-----------------------------------------------------------------------------%

:- pragma foreign_proc("C", no_shader_program = (Prog::out),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    may_duplicate, does_not_affect_liveness],
    " Prog = 0; ").

:- pragma foreign_proc("Java", no_shader_program = (Prog::out),
    [will_not_call_mercury, promise_pure, thread_safe, may_duplicate],
    " Prog = 0; ").
:- pragma inline(no_shader_program/0).

%-----------------------------------------------------------------------------%

:- pragma foreign_proc("C", use_program(Ctx::in, Prog::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    tabled_for_io, may_duplicate, does_not_affect_liveness],
    "
    SAFFRON_GL_SHADERCORE_FUNC(&(Ctx->core), UseProgram)(Prog);
    IOo = IOi;
    ").

:- pragma foreign_proc("Java", use_program(Ctx::in, Prog::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, may_duplicate],
    "
    Ctx.useProgram(Prog);
    IOo = IOi;
    ").
:- pragma inline(use_program/4).

%-----------------------------------------------------------------------------%

:- pragma foreign_proc("C", destroy_shader(Ctx::in, Shader::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    tabled_for_io, may_duplicate, does_not_affect_liveness],
    "
    SAFFRON_GL_SHADERCORE_FUNC(&(Ctx->core), DeleteShader)(Shader);
    IOo = IOi;
    ").

:- pragma foreign_proc("Java",
    destroy_shader(Ctx::in, Shader::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, may_duplicate],
    "
    Ctx.deleteShader(Shader);
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- pred create_shader(
    shader_ctx,
    shader_type,
    string, % Source
    shader,
    bool.bool,
    string,
    io.io, io.io).

:- mode create_shader(
    in,
    in,
    in,
    uo,
    uo,
    out,
    di, uo) is det.

:- pragma foreign_proc("C",
    create_shader(Ctx::in, Type::in, Src::in, Shader::uo, OK::uo, Err::out, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    tabled_for_io, may_duplicate, does_not_affect_liveness],
    "
    GLint i;
    GLsizei len;
    Shader = SAFFRON_GL_SHADERCORE_FUNC(&(Ctx->core), CreateShader)(Type);
    SAFFRON_GL_SHADERCORE_FUNC(&(Ctx->core), ShaderSource)(Shader, 1, (void*)&Src, NULL);
    SAFFRON_GL_SHADERCORE_FUNC(&(Ctx->core), CompileShader)(Shader);
    SAFFRON_GL_SHADER_HANDLE_SHADER_ERROR(Ctx, Err, OK, Shader);
    IOo = IOi;
    ").

:- pragma foreign_proc("Java",
    create_shader(Ctx::in, Type::in, Src::in, Shader::uo, OK::uo, Err::out, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, may_duplicate],
    "
    Shader = Ctx.createShader(Type);
    Ctx.shaderSource(Shader, Src);
    Ctx.compileShader(Shader);
    if(Ctx.shaderCompileStatus(Shader)){
        OK = jmercury.bool.NO;
        Err = """";
    }
    else{
        OK = jmercury.bool.YES;
        Err = Ctx.shaderInfoLog(Shader);
    }
    IOo = IOi;
    ").

create_shader(Ctx, Type, Src, MaybeShader, !IO) :-
    create_shader(Ctx, Type, Src, Shader, OK, Err, !IO),
    (
        OK = bool.yes,
        MaybeShader = maybe.ok(Shader)
    ;
        OK = bool.no,
        % Shader is guaranteed to be initialized in create_shader/8
        destroy_shader(Ctx, Shader, !IO),
        MaybeShader = maybe.error(Err)
    ).

:- pragma inline(create_shader/6).

%-----------------------------------------------------------------------------%

create_vertex_shader(Ctx, Src, MaybeShader, !IO) :-
    create_shader(Ctx, vertex, Src, MaybeShader, !IO).

%-----------------------------------------------------------------------------%

create_fragment_shader(Ctx, Src, MaybeShader, !IO) :-
    create_shader(Ctx, fragment, Src, MaybeShader, !IO).

%-----------------------------------------------------------------------------%

:- func maybe_uniform_no = (maybe.maybe(uniform)::uo) is det.
maybe_uniform_no = maybe.no.
:- pragma foreign_export("C",
    maybe_uniform_no = (uo),
    "SaffronGL_MaybeUniformNo").

:- func maybe_uniform_yes(uniform::di) = (maybe.maybe(uniform)::uo) is det.
maybe_uniform_yes(Uniform) = maybe.yes(Uniform).
:- pragma foreign_export("C",
    maybe_uniform_yes(di) = (uo),
    "SaffronGL_MaybeUniformYes").

:- pragma foreign_proc("C",
    get_uniform_location(Ctx::in, Program::in, Name::in, MaybeUniform::uo, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    may_duplicate, does_not_affect_liveness],
    "
    GLint uniform = SAFFRON_GL_SHADERCORE_FUNC(&(Ctx->core), GetUniformLocation)(
        Program,
        Name);
    MaybeUniform = (uniform < 0 || glGetError() != GL_NO_ERROR) ?
        SaffronGL_MaybeUniformNo() :
        SaffronGL_MaybeUniformYes((GLuint)uniform);
    IOo = IOi;
    ").

:- pragma foreign_proc("Java",
    get_uniform_location(Ctx::in, Prog::in, Name::in, MaybeUniform::uo, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, may_duplicate],
    "
    int i = Ctx.getUniformLocation(Prog, Name);
    MaybeUniform = jmercury.saffron__gl.MaybeInteger(i);
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- pragma foreign_proc("C",
    uniform(Ctx::in, U::in, F1::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    may_duplicate, does_not_affect_liveness],
    "
    SAFFRON_GL_SHADERCORE_FUNC(&(Ctx->core), Uniform1f)(U, F1);
    IOo = IOi;
    ").

:- pragma foreign_proc("Java",
    uniform(Ctx::in, U::in, F1::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, may_duplicate],
    "
    Ctx.uniform(U, F1);
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- pragma foreign_proc("C",
    uniform(Ctx::in, U::in, F1::in, F2::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    may_duplicate, does_not_affect_liveness],
    "
    SAFFRON_GL_SHADERCORE_FUNC(&(Ctx->core), Uniform2f)(U, F1, F2);
    IOo = IOi;
    ").

:- pragma foreign_proc("Java",
    uniform(Ctx::in, U::in, F1::in, F2::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, may_duplicate],
    "
    Ctx.uniform(U, F1, F2);
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- pragma foreign_proc("C",
    uniform(Ctx::in, U::in, F1::in, F2::in, F3::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    may_duplicate, does_not_affect_liveness],
    "
    SAFFRON_GL_SHADERCORE_FUNC(&(Ctx->core), Uniform3f)(U, F1, F2, F3);
    IOo = IOi;
    ").

:- pragma foreign_proc("Java",
    uniform(Ctx::in, U::in, F1::in, F2::in, F3::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, may_duplicate],
    "
    Ctx.uniform(U, F1, F2, F3);
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- pragma foreign_proc("C",
    uniform(Ctx::in, U::in, F1::in, F2::in, F3::in, F4::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    may_duplicate, does_not_affect_liveness],
    "
    SAFFRON_GL_SHADERCORE_FUNC(&(Ctx->core), Uniform4f)(U, F1, F2, F3, F4);
    IOo = IOi;
    ").

:- pragma foreign_proc("Java",
    uniform(Ctx::in, U::in, F1::in, F2::in, F3::in, F4::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, may_duplicate],
    "
    Ctx.uniform(U, F1, F2, F3, F4);
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- pragma foreign_proc("C",
    uniform1(Ctx::in, U::in, FV::in, IOi::di, IOo::uo),
    [may_call_mercury, promise_pure, thread_safe,
    may_duplicate, does_not_affect_liveness],
    "
    SAFFRON_GL_SHADER_RANGE_ERROR(""uniform1"", FV, 1);
#ifdef SAFFRON_GL_RAW_FLOATS
    SAFFRON_GL_SHADERCORE_FUNC(&(Ctx->core), Uniform1fv)(U, (void*)FV->elements);
#else
    SAFFRON_GL_SHADERCORE_FUNC(&(Ctx->core), Uniform1f)(
        U,
        (GLfloat)MR_word_to_float(FV->elements[0]));
#endif
    IOo = IOi;
    ").

:- pragma foreign_proc("Java",
    uniform1(Ctx::in, U::in, FV::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, may_duplicate],
    "
    Ctx.uniform(U, FV[0]);
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- pragma foreign_proc("C",
    uniform2(Ctx::in, U::in, FV::in, IOi::di, IOo::uo),
    [may_call_mercury, promise_pure, thread_safe,
    may_duplicate, does_not_affect_liveness],
    "
    SAFFRON_GL_SHADER_RANGE_ERROR(""uniform2"", FV, 2);
#ifdef SAFFRON_GL_RAW_FLOATS
    SAFFRON_GL_SHADERCORE_FUNC(&(Ctx->core), Uniform2fv)(U, (void*)FV->elements);
#else
    SAFFRON_GL_SHADERCORE_FUNC(&(Ctx->core), Uniform2f)(
        U,
        (GLfloat)MR_word_to_float(FV->elements[0]),
        (GLfloat)MR_word_to_float(FV->elements[1]));
#endif
    IOo = IOi;
    ").

:- pragma foreign_proc("Java",
    uniform2(Ctx::in, U::in, FV::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, may_duplicate],
    "
    Ctx.uniform(U, FV[0], FV[1]);
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- pragma foreign_proc("C",
    uniform3(Ctx::in, U::in, FV::in, IOi::di, IOo::uo),
    [may_call_mercury, promise_pure, thread_safe,
    may_duplicate, does_not_affect_liveness],
    "
    SAFFRON_GL_SHADER_RANGE_ERROR(""uniform3"", FV, 3);
#ifdef SAFFRON_GL_RAW_FLOATS
    SAFFRON_GL_SHADERCORE_FUNC(&(Ctx->core), Uniform3fv)(U, (void*)FV->elements);
#else
    SAFFRON_GL_SHADERCORE_FUNC(&(Ctx->core), Uniform3f)(
        U,
        (GLfloat)MR_word_to_float(FV->elements[0]),
        (GLfloat)MR_word_to_float(FV->elements[1]),
        (GLfloat)MR_word_to_float(FV->elements[2]));
#endif
    IOo = IOi;
    ").

:- pragma foreign_proc("Java",
    uniform3(Ctx::in, U::in, FV::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, may_duplicate],
    "
    Ctx.uniform(U, FV[0], FV[1], FV[2]);
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- pragma foreign_proc("C",
    uniform4(Ctx::in, U::in, FV::in, IOi::di, IOo::uo),
    [may_call_mercury, promise_pure, thread_safe,
    may_duplicate, does_not_affect_liveness],
    "
    SAFFRON_GL_SHADER_RANGE_ERROR(""uniform4"", FV, 4);
#ifdef SAFFRON_GL_RAW_FLOATS
    SAFFRON_GL_SHADERCORE_FUNC(&(Ctx->core), Uniform4fv)(U, (void*)FV->elements);
#else
    SAFFRON_GL_SHADERCORE_FUNC(&(Ctx->core), Uniform4f)(
        U,
        (GLfloat)MR_word_to_float(FV->elements[0]),
        (GLfloat)MR_word_to_float(FV->elements[1]),
        (GLfloat)MR_word_to_float(FV->elements[2]),
        (GLfloat)MR_word_to_float(FV->elements[3]));
#endif
    IOo = IOi;
    ").

:- pragma foreign_proc("Java",
    uniform4(Ctx::in, U::in, FV::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, may_duplicate],
    "
    Ctx.uniform(U, FV[0], FV[1], FV[2], FV[4]);
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- pragma foreign_proc("C",
    uniform(Ctx::in, U::in,
        A1::in, A2::in, A3::in,
        B1::in, B2::in, B3::in,
        C1::in, C2::in, C3::in,
        IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    may_duplicate, does_not_affect_liveness],
    "
    GLfloat matrix[9];
    matrix[0] = (GLfloat)A1;
    matrix[1] = (GLfloat)A2;
    matrix[2] = (GLfloat)A3;
    matrix[3] = (GLfloat)B1;
    matrix[4] = (GLfloat)B2;
    matrix[5] = (GLfloat)B3;
    matrix[6] = (GLfloat)C1;
    matrix[7] = (GLfloat)C2;
    matrix[8] = (GLfloat)C3;
    SAFFRON_GL_SHADERCORE_FUNC(&(Ctx->core), UniformMatrix3fv)(U, matrix);
    IOo = IOi;
    ").

:- pragma foreign_proc("Java",
    uniform(Ctx::in, U::in,
        A1::in, A2::in, A3::in,
        B1::in, B2::in, B3::in,
        C1::in, C2::in, C3::in,
        IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, may_duplicate],
    "
    float[] matrix = {
        (float)A1, (float)A2, (float)A3,
        (float)B1, (float)B2, (float)B3,
        (float)C1, (float)C2, (float)C3
    };
    Ctx.uniformMat3x3(U, matrix);
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- pragma foreign_proc("C",
    uniform3x3(Ctx::in, U::in, FV::in, IOi::di, IOo::uo),
    [may_call_mercury, promise_pure, thread_safe,
    may_duplicate, does_not_affect_liveness],
    "
    SAFFRON_GL_SHADER_RANGE_ERROR(""uniform3x3"", FV, 9);
#ifdef SAFFRON_GL_RAW_FLOATS
    SAFFRON_GL_SHADERCORE_FUNC(&(Ctx->core), UniformMatrix3fv)(U, (void*)FV->elements);
#else
    GLfloat matrix[9];
    MR_Integer i;
    for(i = 0; i < 9; i++)
        matrix[i] = (GLfloat)MR_word_to_float(FV->elements[i]);
    SAFFRON_GL_SHADERCORE_FUNC(&(Ctx->core), UniformMatrix3fv)(U, matrix);
#endif
    IOo = IOi;
    ").

:- pragma foreign_proc("Java",
    uniform3x3(Ctx::in, U::in, FV::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, may_duplicate],
    "
    float[] matrix = new float[9];
    for(int i = 0; i < 9; i++)
        matrix[i] = (float)FV[i];
    Ctx.uniformMat3x3(U, matrix);
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- pragma foreign_proc("C",
    uniform(Ctx::in, U::in,
        A1::in, A2::in, A3::in, A4::in,
        B1::in, B2::in, B3::in, B4::in,
        C1::in, C2::in, C3::in, C4::in,
        D1::in, D2::in, D3::in, D4::in,
        IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    may_duplicate, does_not_affect_liveness],
    "
    GLfloat matrix[16];
    matrix[0] = (GLfloat)A1;
    matrix[1] = (GLfloat)A2;
    matrix[2] = (GLfloat)A3;
    matrix[3] = (GLfloat)A4;
    matrix[4] = (GLfloat)B1;
    matrix[5] = (GLfloat)B2;
    matrix[6] = (GLfloat)B3;
    matrix[7] = (GLfloat)B4;
    matrix[8] = (GLfloat)C1;
    matrix[9] = (GLfloat)C2;
    matrix[10] = (GLfloat)C3;
    matrix[11] = (GLfloat)C4;
    matrix[12] = (GLfloat)D1;
    matrix[13] = (GLfloat)D2;
    matrix[14] = (GLfloat)D3;
    matrix[15] = (GLfloat)D4;
    SAFFRON_GL_SHADERCORE_FUNC(&(Ctx->core), UniformMatrix4fv)(U, matrix);
    IOo = IOi;
    ").

:- pragma foreign_proc("Java",
    uniform(Ctx::in, U::in,
        A1::in, A2::in, A3::in, A4::in,
        B1::in, B2::in, B3::in, B4::in,
        C1::in, C2::in, C3::in, C4::in,
        D1::in, D2::in, D3::in, D4::in,
        IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, may_duplicate],
    "
    float[] matrix = {
        (float)A1, (float)A2, (float)A3, (float)A4,
        (float)B1, (float)B2, (float)B3, (float)B4,
        (float)C1, (float)C2, (float)C3, (float)C4,
        (float)D1, (float)D2, (float)D3, (float)D4
    };
    Ctx.uniformMat4x4(U, matrix);
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- pragma foreign_proc("C",
    uniform4x4(Ctx::in, U::in, FV::in, IOi::di, IOo::uo),
    [may_call_mercury, promise_pure, thread_safe,
    may_duplicate, does_not_affect_liveness],
    "
    SAFFRON_GL_SHADER_RANGE_ERROR(""uniform4x4"", FV, 16);
#ifdef SAFFRON_GL_RAW_FLOATS
    SAFFRON_GL_SHADERCORE_FUNC(&(Ctx->core), UniformMatrix4fv)(U, (void*)FV->elements);
#else
    GLfloat matrix[16];
    MR_Integer i;
    for(i = 0; i < 16; i++)
        matrix[i] = (GLfloat)MR_word_to_float(FV->elements[i]);
    SAFFRON_GL_SHADERCORE_FUNC(&(Ctx->core), UniformMatrix4fv)(U, matrix);
#endif
    IOo = IOi;
    ").

:- pragma foreign_proc("Java",
    uniform4x4(Ctx::in, U::in, FV::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, may_duplicate],
    "
    float[] matrix = new float[16];
    for(int i = 0; i < 16; i++)
        matrix[i] = (float)FV[i];
    Ctx.uniformMat4x4(U, matrix);
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- pragma foreign_proc("C",
    bind_attrib_location(Ctx::in, Program::in, Name::in, I::in, Attrib::uo, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    may_duplicate, does_not_affect_liveness],
    "
    SAFFRON_GL_SHADERCORE_FUNC(&(Ctx->core), BindAttribLocation)(
        Program,
        I,
        Name);
    Attrib = I;
    IOo = IOi;
    ").

:- pragma foreign_proc("Java",
    bind_attrib_location(Ctx::in, Program::in, Name::in, I::in, Attrib::uo, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, may_duplicate],
    "
    Ctx.bindAttribLocation(Program, I, Name);
    Attrib = I;
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- func maybe_vertex_attrib_no = (maybe.maybe(vertex_attrib)::uo) is det.
maybe_vertex_attrib_no = maybe.no.
:- pragma foreign_export("C",
    maybe_vertex_attrib_no = (uo),
    "SaffronGL_MaybeVertexAttribNo").

:- func maybe_vertex_attrib_yes(vertex_attrib::di) = (maybe.maybe(vertex_attrib)::uo) is det.
maybe_vertex_attrib_yes(Attrib) = maybe.yes(Attrib).
:- pragma foreign_export("C",
    maybe_vertex_attrib_yes(di) = (uo),
    "SaffronGL_MaybeVertexAttribYes").

:- pragma foreign_proc("C",
    get_attrib_location(Ctx::in, Program::in, Name::in, MaybeAttrib::uo, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    may_duplicate, does_not_affect_liveness],
    "
    GLint attrib = SAFFRON_GL_SHADERCORE_FUNC(&(Ctx->core), GetAttribLocation)(
        Program,
        Name);
    MaybeAttrib = (attrib < 0 || glGetError() != GL_NO_ERROR) ?
        SaffronGL_MaybeVertexAttribNo() :
        SaffronGL_MaybeVertexAttribYes((GLuint)attrib);
    IOo = IOi;
    ").

:- pragma foreign_proc("Java",
    get_attrib_location(Ctx::in, Program::in, Name::in, MaybeAttrib::uo, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, may_duplicate],
    "
    int i = Ctx.getAttribLocation(Program, Name);
    MaybeAttrib = jmercury.saffron__gl.MaybeInteger(i);
    IOo = IOi;
    ").

%-----------------------------------------------------------------------------%

:- pragma foreign_proc("C",
    vertex_attrib_pointer(Ctx::in, Attrib::in, N::in, Type::in, Stride::in, Offset::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, will_not_throw_exception,
    may_duplicate, does_not_affect_liveness],
    "
    SAFFRON_GL_SHADERCORE_FUNC(&(Ctx->core), VertexAttribPointer)(
        Attrib,
        N,
        Type,
        GL_FALSE,
        Stride,
        (void*)(MR_Word)Offset);
    IOo = IOi;
    ").

:- pragma foreign_proc("Java",
    vertex_attrib_pointer(Ctx::in, Attrib::in, N::in, Type::in, Stride::in, Offset::in, IOi::di, IOo::uo),
    [will_not_call_mercury, promise_pure, thread_safe, may_duplicate],
    "
    Ctx.vertexAttribPointer(Attrib, N, Type, Stride, Offset);
    IOo = IOi;
    ").

