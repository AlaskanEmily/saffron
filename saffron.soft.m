% Copyright (c) 2021-2022 Transnat Games, AlaskanEmily
%
% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.

:- module saffron.soft.
%=============================================================================%
% Implementations of some Saffron typeclasses without any GPU resources.
% These are used in part by some backends, such as the GL2 backend.
:- interface.
%=============================================================================%

:- use_module array.
:- use_module list.

:- use_module saffron.geometry.

%-----------------------------------------------------------------------------%

:- type vertex(Vec) == saffron.geometry.vertex(Vec).

%-----------------------------------------------------------------------------%

:- type buffer(T) == array.array(T).

%-----------------------------------------------------------------------------%
% Since this will almost always be used as-is, wrap it up in its own type
% rather than an equivalence.
% This is needed because of limitations in typeclasses.
:- type color_buffer ---> color_buffer(buffer(color)).

%-----------------------------------------------------------------------------%

:- type shape(Vec, Tex).

%-----------------------------------------------------------------------------%

:- func texture(shape(_, Tex)) = Tex.

%-----------------------------------------------------------------------------%

:- func primitive_type(shape(_, _)) = saffron.geometry.primitive_type.

%-----------------------------------------------------------------------------%

:- pred shape_foldl(pred(vertex(Vec), T, T), shape(Vec, _), T, T).
:- mode shape_foldl(pred(in, in, out) is semidet, in, in, out) is semidet.
:- mode shape_foldl(pred(in, mdi, muo) is semidet, in, mdi, muo) is semidet.
:- mode shape_foldl(pred(in, in, out) is det, in, in, out) is det.
:- mode shape_foldl(pred(in, mdi, muo) is det, in, mdi, muo) is det.
:- mode shape_foldl(pred(in, di, uo) is det, in, di, uo) is det.

%-----------------------------------------------------------------------------%

:- func create_shape(
    saffron.geometry.primitive_type,
    buffer(vertex(Vec)),
    Tex) =
    shape(Vec, Tex).

%-----------------------------------------------------------------------------%

:- func create_shape_index_list(
    saffron.geometry.primitive_type,
    buffer(vertex(Vec)),
    list.list(int),
    Tex) =
    shape(Vec, Tex).

%-----------------------------------------------------------------------------%

:- func create_shape_index_array(
    saffron.geometry.primitive_type,
    buffer(vertex(Vec)),
    array.array(int),
    Tex) =
    shape(Vec, Tex).

%-----------------------------------------------------------------------------%

:- type group(Vec, Tex).

%-----------------------------------------------------------------------------%

:- func shapes(group(Vec, Tex)) = array.array(shape(Vec, Tex)).

%-----------------------------------------------------------------------------%

:- func create_group_list(list.list(shape(Vec, Tex))) = group(Vec, Tex).

%-----------------------------------------------------------------------------%

:- func create_group_array(array.array(shape(Vec, Tex))) = group(Vec, Tex).

%=============================================================================%
:- implementation.
%=============================================================================%

:- import_module int.

%-----------------------------------------------------------------------------%

:- type shape(Vec, Tex) --->
    shape(saffron.geometry.primitive_type, buffer(vertex(Vec)), Tex) ;
    indexed_shape(saffron.geometry.primitive_type, array.array(int), buffer(vertex(Vec)), Tex).

%-----------------------------------------------------------------------------%
% Used for iterating the indexed version.
% This is a separate pred instead of a lambda since we need multiple modes.
:- pred shape_foldl_inner(pred(vertex(Vec), T, T), buffer(vertex(Vec)), int, T, T).
:- mode shape_foldl_inner(pred(in, in, out) is semidet, in, in, in, out) is semidet.
:- mode shape_foldl_inner(pred(in, mdi, muo) is semidet, in, in, mdi, muo) is semidet.
:- mode shape_foldl_inner(pred(in, in, out) is det, in, in, in, out) is det.
:- mode shape_foldl_inner(pred(in, mdi, muo) is det, in, in, mdi, muo) is det.
:- mode shape_foldl_inner(pred(in, di, uo) is det, in, in, di, uo) is det.

shape_foldl_inner(Pred, Buffer, I, !T) :-
    array.lookup(Buffer, I, Vertex),
    Pred(Vertex, !T).

%-----------------------------------------------------------------------------%

shape_foldl(Pred, shape(_, Buffer, _), !T) :-
    array.foldl(Pred, Buffer, !T).

shape_foldl(Pred, indexed_shape(_, Indices, Buffer, _), !T) :-
    array.foldl(shape_foldl_inner(Pred, Buffer), Indices, !T).

%-----------------------------------------------------------------------------%

texture(shape(_, _, Tex)) = Tex.

texture(indexed_shape(_, _, _, Tex)) = Tex.

%-----------------------------------------------------------------------------%

primitive_type(shape(Type, _, _)) = Type.

primitive_type(indexed_shape(Type, _, _, _)) = Type.

%-----------------------------------------------------------------------------%

create_shape(Type, Buffer, Tex) = shape(Type, Buffer, Tex).

%-----------------------------------------------------------------------------%

create_shape_index_list(Type, Buffer, Indices, Tex) =
    indexed_shape(Type, array.from_list(Indices), Buffer, Tex).

%-----------------------------------------------------------------------------%

create_shape_index_array(Type, Buffer, Indices, Tex) =
    indexed_shape(Type, Indices, Buffer, Tex).

%-----------------------------------------------------------------------------%

:- type group(Vec, Tex) ---> group(array.array(shape(Vec, Tex))).

%-----------------------------------------------------------------------------%

shapes(group(Array)) = Array.

%-----------------------------------------------------------------------------%

create_group_list(List) = group(array.from_list(List)).

%-----------------------------------------------------------------------------%

create_group_array(Array) = group(Array).

%-----------------------------------------------------------------------------%

