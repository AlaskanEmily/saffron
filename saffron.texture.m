% Copyright (c) 2021-2022 Transnat Games, AlaskanEmily
%
% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.

:- module saffron.texture.
%=============================================================================%
% Texture types, typeclasses, and preds
:- interface.
%=============================================================================%

:- use_module bitmap.

%-----------------------------------------------------------------------------%

%:- typeclass texture(Texture) where [
%    func width(Texture) = int,
%    func height(Texture) = int
%].

%-----------------------------------------------------------------------------%

:- typeclass texture(Ctx, Texture)
    <= (destroy(Ctx, Texture), (Ctx -> Texture)) where [
    
    pred create_empty(Ctx, int, int, Texture, io.io, io.io),
    mode create_empty(in, in, in, out, di, uo) is det,
    
    % create_from_bitmap(Ctx, Bitmap, Width, Height, Texture, !IO)
    pred create_from_bitmap(Ctx, bitmap.bitmap, int, int, Texture, io.io, io.io),
    mode create_from_bitmap(in, in, in, in, out, di, uo) is det,

    % upload_at(Ctx, Texture, Bitmap, X, Y, Width, Height, Texture, !IO)
    % Uploads Bitmap onto Texture at X, Y.
    % The bitmap must be Width * Height * 4 bytes in size.
    % This can be used in combination with create_empty to make texture
    % atlases.
    pred upload_at(Ctx, Texture, bitmap.bitmap, int, int, int, int, io.io, io.io),
    mode upload_at(in, in, in, in, in, in, in, di, uo) is det
].

